<?php
namespace Etsoftware\Encrypt;

/**
 * 一般用于银行卡号最后一位验证码
 */
class Luhn
{
    
	function __construct()
    {
        
    }
    public function verify($number){
        $num = substr($number, 0, strlen($number)-1);
        $c = substr($number, strlen($number)-1, 1);
        return ($this->encrypt($num) == $c);
    }
    public function encrypt($number){
        $number = preg_replace("/\s+/im", "", $number);
        if(!preg_match("/^\d{2,}$/im", $number))return null;
        $reVal="";
        for($i=0; $i<strlen($number); $i+=2){
            $c = substr($number, $i, 1);
            $reVal.=$c*2;
        }
        $code=0;
        for($i=0; $i<strlen($reVal); $i++){
            $code += substr($reVal, $i, 1);
        }
        for($i=1; $i<strlen($number); $i+=2){
            $c = substr($number, $i, 1);
            $code+=$c;
        }
        return 10-substr($code, strlen($code)-1);
    }
    
}

