<?php
namespace Etsoftware\Encrypt;

use Etsoftware\Lib\File;

class Rsa 
{
    private $privkey = null;
    private $pubkey = null;
    private $expired = 0;
    private $_message = 0;
    private $expired_at = 10*60; // 过期时间（秒）
    private $cf = null; //
	/**
	 * initail 
	 * @param [type]  $host       host address
    **/
	function __construct($expired_at=null)
    {
        if($expired_at)$this->expired_at = $expired_at;
        $keys = $this->getKeyByCache();
        $this->loadKey($keys);
    }
    public function getKey(){
        return array(
            'privkey' => $this->privkey,
            'pubkey'  => $this->pubkey,
            'expired'  => $this->expired
        );
    }
    public function loadKey($keys=null){
        if($keys == null
            || time() - $keys['expired'] > $this->expired_at
        )$keys = $this->createKey();
        if($keys == null){
            echo($this->_message->getMessage());
            die;
        }
        $this->privkey = $keys['privkey'];
        $this->pubkey = $keys['pubkey'];
        $this->expired = $keys['expired'];
        return $this;
    }
    /**
     * get keys by rsa
     * @return [type] [description]
     */
    public function createKey(){
        return $this->try(function(){
            $res = openssl_pkey_new();
            openssl_pkey_export($res, $privkey);
            $d= openssl_pkey_get_details($res);
            $pubkey = $d['key'];
            $keys = array(
                'privkey' => $privkey,
                'pubkey'  => $pubkey,
                'expired'  => time()
            );  
            $this->setKeyByCache($keys);      
            return $keys;
        });
    }
    /**
     * encrypt by private key
     * @param  [type] $data     [description]
     * @param  [type] $key      private key
     * @param  string $encoding [description]
     * @return [type]           [description]
     */
    public function encryptByPrivate($data, $key=null, $encoding='base64'){
        return $this->try(function()use($data, $key, $encoding){
            $reval = null;
            if($key == null)$key = $this->privkey;
            $res = openssl_pkey_get_private($key);
            openssl_private_encrypt($data, $reval, $res);
            return $this->encode($reval, $encoding);
        });
    }
    /**
     * decrypt by private key
     * @param  [type] $data     [description]
     * @param  [type] $key      private key
     * @param  string $encoding [description]
     * @return [type]           [description]
     */
    public function decryptByPrivate($data, $key=null, $encoding='base64'){
        return $this->try(function()use($data, $key, $encoding){
            $reval = null;
            if($key == null)$key = $this->privkey;
            $data = $this->decode($data, $encoding);
            $res = openssl_pkey_get_private($key);
            openssl_private_decrypt($data, $reval, $res);
            return $reval;
        });
    }
    /**
     * encrypt by public key
     * @param  [type] $data     [description]
     * @param  [type] $key      private key
     * @param  string $encoding [description]
     * @return [type]           [description]
     */
    public function encryptByPublic($data, $key=null, $encoding='base64'){
        return $this->try(function()use($data, $key, $encoding){
            $reval = null;
            if($key == null)$key = $this->pubkey;
            $res = openssl_pkey_get_public($key);
            openssl_public_encrypt($data, $reval, $res);
            return $this->encode($reval, $encoding);
        });
    }
    /**
     * decrypt by public key
     * @param  [type] $data     [description]
     * @param  [type] $key      private key
     * @param  string $encoding [description]
     * @return [type]           [description]
     */
    public function decryptByPublic($data, $key=null, $encoding='base64'){
        return $this->try(function()use($data, $key, $encoding){
            $reval = null;
            if($key == null)$key = $this->pubkey;
            $data = $this->decode($data, $encoding);
            $res = openssl_pkey_get_public($key);
            openssl_public_decrypt($data, $reval, $res);
            return $reval;
        });
    }
    public function getErrors(){
        return $this->_message;
    }
    private function getKeyByCache(){
        $this->cf = $f = new File("et.rsa");
        $ret = $f->read();
        if($ret['errcode'] != 0){return null; }
        return json_decode($ret['data'], true);
    }
    private function setKeyByCache($data){
        $data = json_encode($data);
        $ret = $this->cf->write($data);
        return $this;
    }    
    private function try($callback){
        $this->_message = null;
        set_error_handler(function($errno, $errstr, $errfile, $errline, $errcontext) {
            if (0 === error_reporting())return false;
            // throw new \Exception($errstr, 0, $errno, $errfile, $errline);
            throw new \Exception($errstr);
        });
        try {
            $ret = $callback->call($this);
        } catch (\Exception $e) {
            $ret = null;
            $this->_message = $e;
        }
        restore_error_handler();
        return $ret;
    }

    //=?charset?B?xxxxxxxx?=
    /**
     * encoding string
     * @param  [type] $str [description]
     * @return [type]      [description]
     */
    private function encode($str, $encoding='base64'){
        $encoding = strtolower($encoding);
        if( $encoding == "base64" ){
            return base64_encode($str);
        }
        if( $encoding == "quoted-printable" ){
            return quoted_printable_encode($str);
        }
        if( $encoding == "7bit" ){//是缺省的编码方式
            // return quoted_printable_decode($str);
        }
        if( $encoding == "8bit" ){
            // return quoted_printable_decode($str);
        }
        if( $encoding == "Binary" ){
            // return quoted_printable_decode($str);
        }
        return $str;
    }   
    /**
     * decode string
     * @param  [type] $str [description]
     * @return [type]      [description]
     */
    private function decode($str, $encoding='base64'){
        if( $encoding == "base64" ){
            return base64_decode($str);
        }
        if( $encoding == "quoted-printable" ){
            return quoted_printable_decode($str);
        }
        if( $encoding == "7bit" ){//是缺省的编码方式
            // return quoted_printable_decode($str);
        }
        if( $encoding == "8bit" ){
            // return quoted_printable_decode($str);
        }
        if( $encoding == "Binary" ){
            // return quoted_printable_decode($str);
        }
        return $str;
    }
    
}

