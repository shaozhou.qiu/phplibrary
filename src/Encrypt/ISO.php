<?php
namespace Etsoftware\Encrypt;

use Etsoftware\Lib\File;

class ISO
{
  
	function __construct($expired_at=null)
    {

    }
    static public function VC_7064_983_MOD_11_2($data){
        if(!preg_match_all("/\d/im", $data, $mc, PREG_SET_ORDER)) return null;
        if(count($mc)<17)return null;

        $factor = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2];
        $code = [1, 0, 'X' , 9, 8, 7, 6, 5, 4, 3, 2];
        $ret = 0;
        foreach ($factor as $k=>$v) {
            $ret += $v*$mc[$k][0];
            
        }
        return $code[ $ret%11];
    }
}