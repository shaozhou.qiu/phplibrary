<?php
namespace Etsoftware\Iot;
use Etsoftware\Lib\StringUtil;

class Server
{
	function __construct()
    {
    }
    public function forward($ip, $port, $data){
        $ret = StringUtil::hexstr2Char($data);
        $hdata = StringUtil::char2Hexstr($ret['data'], $ret['length']);
        $portHex = $this->int2b($port, 2);
        $d = $this->strip2b($ip)." $portHex";
        $d .= " ". $hdata['data'];
        return $this->command("0x02", $d);
    }
    private function command($cmd, $data){return "0x2a $cmd 0x3a $data 0x23"; }

    /**
     * 将int 转化为4个16进制的字符串
     * @param  [type] $v 整形
     * @param  [type] $size 储存位数
     * @return [type]    [description]
     */
    private function int2b($v, $size=4){
        if(!is_numeric($v))return null;
        $ret='';
        for($i=($size-1); $i>-1; $i--){
            $p = pow(256, $i);
            if($p>$v){$ret = "0x00 $ret"; continue; }
            $d = round($v/$p);
            $v = $v%$p;
            $h = dechex($d);
            if(strlen($h)<2) $h= "0$h";
            $ret = "0x$h $ret";
        }
        return trim($ret);
    }
    /**
     * 将"127.0.0.1"转换成以4个16进制的ip表达式
     * @param  [type] $v 字符串ip地址
     * @return [type]    [description]
     */
    private function strip2b($v){
        if(!$v)$v="127.0.0.1";
        if(!preg_match("/^(\d+\.){3}\d+$/im", $v))return null;
        preg_match_all("/\d+/im", $v, $mc, PREG_SET_ORDER);
        $ret="";
        foreach($mc as $m){
            $h = dechex($m[0]);
            if(strlen($h)<2)$h="0$h";
            $ret.=" 0x$h";
        }
        return trim($ret);
    }
        /**
     * 将字符串转换成16进制表达式
     * @param  [type] $v 字符串ip地址
     * @return [type]    [description]
     */
    protected function str2b($v){
        if(strlen($v)<1)return null;
        preg_match_all("/./im", $v, $mc, PREG_SET_ORDER);
        $ret="";
        foreach($mc as $m){
            $d = ord($m[0]);
            $h = dechex($d);
            if(strlen($h)<2)$h="0$h";
            $ret.=" 0x$h";
        }
        return $ret;
    }

}