<?php
namespace Etsoftware\Mcu;
use Etsoftware\Lib\StringUtil;

class Funcation
{
    /**
     * 将int 转化为4个16进制的字符串
     * @param  [type] $v 整形
     * @return [type]    [description]
     */
    static public function int2b($v, $size=4){
        if(!is_numeric($v)){throw new \Exception("Error Parameter 1, not an integer", 1);
         return null;}
        $ret='';
        for($i=3; $i>-1; $i--){
            $p = pow(256, $i);
            if($p>$v){$ret = "0x00 $ret"; continue; }
            $d = round($v/$p);
            $v = $v%$p;
            $h = dechex($d);
            if(strlen($h)<2) $h= "0$h";
            $ret = "0x$h $ret";
        }
        if($size<4)
            $ret = preg_replace("/^((\w+\s*){".$size."}).*/", "$1", $ret);
        $ret = trim($ret);
        return $ret;
    }
    /**
     * 将"127.0.0.1"转换成以4个16进制的ip表达式
     * @param  [type] $v 字符串地址
     * @return [type]    [description]
     */
    static public function strip2b($v){
        if(!preg_match("/^(\d+\.){3}\d+$/im", $v))return null;
        preg_match_all("/\d+/im", $v, $mc, PREG_SET_ORDER);
        $ret="";
        foreach($mc as $m){
            $h = dechex($m[0]);
            if(strlen($h)<2)$h="0$h";
            $ret.=" 0x$h";
        }
        return trim($ret);
    }
    /**
     * 将字符串转换成16进制表达式
     * @param  [type] $v 字符串地址
     * @return [type]    [description]
     */
    static public function str2b($v){
        if(strlen($v)<1)return null;
        return StringUtil::char2Hexstr($v, strlen($v));
    }

}