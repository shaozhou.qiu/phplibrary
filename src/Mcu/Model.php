<?php
namespace Etsoftware\Mcu;

class Model
{
    private $mcu=[
        'Esp8266'
    ];
	function __construct($data=null)
    {
        foreach($this->mcu as $k=>$v){
            $muc = "Etsoftware\\Mcu\\$v";
            unset($this->mcu[$k]);
            $this->mcu[strtoupper($v)] = new $muc($data); 
        }
    }
    public function getMcu()
    {
        return $this->mcu;
    }
}