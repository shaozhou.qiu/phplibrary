<?php
namespace Etsoftware\Mcu;
use Etsoftware\Lib\StringUtil;

class Mcu
{
    function __construct()
    {
    }

    /**
     * 智能解析数据包
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function analysis($data){
        if(is_string($data)){
            if( preg_match("/^(\s*([0-9a-f]{1,3})\s*)+$/im", $data)){
                if(preg_match("/[a-f]/im", $data)){
                    $d= preg_replace("/([0-9a-f]{1,2})/", "0x$1", $data);
                    return $this->analysis($d);
                }else if(preg_match("/[0-9]{3}/im", $data)){
                    preg_match_all("/[0-9]{1,3}/im", $data, $mc, PREG_SET_ORDER);
                    $d='';
                    foreach($mc as $m)$d.=' 0x'.dechex($m[0]);
                    return $this->analysis($d);
                }
                return -1;
            }
            if( preg_match("/^(\s*(\\\\?0x[0-9a-f]{1,2})\s*)+$/im", $data)){
                $d=[];
                preg_match_all("/\\\\?(0x[0-9a-f]+)/im", $data, $mc, PREG_SET_ORDER);
                foreach($mc as $v){ array_push($d, $v[1]);}
                return $this->analysis($d);
            }
            return -2;
        }

        if(!is_array($data))return -3;
        $this->analysisHex($data);
    }
    /**
     * 将int 转化为4个16进制的字符串
     * @param  [type] $v 整形
     * @return [type]    [description]
     */
    protected function int2b($v, $size=4){
        return Funcation::int2b($v, $size);
    }
    /**
     * 将"127.0.0.1"转换成以4个16进制的ip表达式
     * @param  [type] $v 字符串ip地址
     * @return [type]    [description]
     */
    protected function strip2b($v){
        return Funcation::strip2b($v);
    }
    /**
     * 将字符串转换成16进制表达式
     * @param  [type] $v 字符串ip地址
     * @return [type]    [description]
     */
    protected function str2b($v){
        return Funcation::str2b($v);
    }

}