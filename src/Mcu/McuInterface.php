<?php
namespace Etsoftware\Mcu;

interface McuInterface
{

    /**
     * 解析数据包
     * @param  [type] $hex [description]
     * @return [type]       [description]
     */
    public function analysisHex($hex=null);
    /**
     * 获取有效引角
     * @param  [type] $hex [description]
     * @return [type]       [description]
     */
    public function getPins();
    /**
     * 获取有效引角值
     * @param  [type] $hex [description]
     * @return [type]       [description]
     */
    public function getPinValues();
    /**
     * 获取MCU唯一标识号
     * @return [type] [description]
     */
    public function getId();
    /**
     * 获取自动获取对应Gpio口
     * @return [type] [description]
     */
    public function getGpio($pin);
}