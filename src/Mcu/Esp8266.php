<?php
namespace Etsoftware\Mcu;

use Etsoftware\Mcu\Mcu;
use Etsoftware\Mcu\McuInterface;


class Esp8266 extends Mcu implements McuInterface
{
    private $id=null;
    private $gpio=[];
    private $d=[];
    private $a0=0;
    private $gpioMap=[
        'GPIO1'=>"TX"
        ,'GPIO3'=>"RX"
        ,'GPIO15'=>"D8"
        ,'GPIO13'=>"D7"
        ,'GPIO12'=>"D6"
        ,'GPIO14'=>"D5"
        ,'GPIO2'=>"D4"
        ,'GPIO0'=>"D3"
        ,'GPIO4'=>"D2"
        ,'GPIO5'=>"D1"
        ,'GPIO16'=>"D0"
        ,'GPIO10'=>"SD3"
        ,'GPIO9'=>"SD2"
        ,'GPIO8'=>"SD1"
        ,'GPIO11'=>"CMD"
        ,'GPIO7'=>"SD0"
        ,'GPIO6'=>"CLK"
    ];
	function __construct($hex=null)
    {
        for($i=0; $i<9; $i++){ $this->d[$i]=0; }
        for($i=0; $i<17; $i++){ $this->gpio[$i]=0; }
        $this->analysis($hex);
    }
    function __set($property, $args){
        $fun = "set$property";
        return $this->$fun($args);
    }
    function __get($property){
        $fun = "get$property";
        return $this->$fun();
    }
    function __call($fun, $args){
        $lowfun = strtolower($fun);
        if(preg_match("/^getd(\d+)$/im", $fun, $m)){
            $n = $m[1]*1;
            return ($n<count($this->d))?$this->d[$n]:0;
        }else if(preg_match("/^setd(\d+)$/im", $fun, $m)){
            $n = $m[1]*1;
            return $this->setDPinValue($n, intval("0".$args[0]));
        }else if(preg_match("/^geta(\d+)$/im", $fun, $m)){
            return $this->a0;
        }else if(preg_match("/^seta(\d+)$/im", $fun, $m)){
            $this->a0 = intval("0".$args[0]);
            return $this->a0;
        }else if(preg_match("/^getgpio(\d+)$/im", $fun, $m)){
            $n = $m[1]*1;
            return ($n<count($this->gpio))?$this->gpio[$n]:0;
        }else if(preg_match("/^setgpio(\d+)$/im", $fun, $m)){
            $n = $m[1]*1;
            return $this->setGpioValue($n, intval("0".$args[0]));
        }
    }
    /**
     * 获取自动获取对应Gpio口
     * @return [type] [description]
     */
    public function getGpio($pin){
        if(is_numeric($pin))return $pin;
        if(preg_match("/gpio(\d+)/im", $pin, $m)){
            return $m[1]*1;
        }
        if(preg_match("/d(\d+)/im", $pin, $m)){
            return $this->getGpio($this->getGpioByD($m[1]));
        }
        return 0;
    }
    /**
     * 获取D口对应的Gpio口
     * @param  [type] $pin [description]
     * @return [type]      [description]
     */
    public function getGpioByD($pin){
        if(is_numeric($pin) || preg_match("/^\d+$/im", $pin)){
            $pin = "D$pin";
        }
        foreach($this->gpioMap as $k=>$v){
            if(strtoupper($v) == strtoupper($pin)){
                return $k;
                break;
            }
        }
        return null;
    }
    /**
     * 获取有效引角值
     * @param  [type] $hex [description]
     * @return [type]       [description]
     */
    public function getPinValues(){
        $ret=[];
        $pins = $this->getPins();
        foreach($pins as $pn){
            $ret[$pn] = $this->$pn;
        }
        return $ret;
    }
    /**
     * 获取有效引角
     * @param  [type] $hex [description]
     * @return [type]       [description]
     */
    public function getPins(){
        $ret=[];
        for($i=0; $i<count($this->d); $i++)array_push($ret, "D$i");
        for($i=0; $i<count($this->gpio); $i++)array_push($ret, "GPIO$i");
        array_push($ret, "A0");
        return $ret;
    }
    /**
     * 解析数据包
     * @param  [type] $hex  十六进制数组
     * @return [type]       [description]
     */
    public function analysisHex($hex=[]){
        if(!is_array($hex))return -1;
        if(count($hex)<15)return -2;
        $this->id = $this->analysisId($hex);
        $this->gpio = $this->analysisGpio($hex);
        $this->a0 = $this->analysisA0($hex);

    }
    public function getD(){return $this->d; }
    public function getA0(){return $this->a0; }
    public function getId(){return $this->id; }
    /**
     * 上传Gpio引角映射
     * @param [type] $mcugpiomap  传入一个McuGpioMap对象
     * @param [type] $fun 回调函数
     */
    public function uploadGpioMap($mcugpiomap, $fun=null){
        $hex = "";
        if(is_array($mcugpiomap)){
            for($i=0; $i<count($mcugpiomap); $i++){
                $ugm = $this->uploadGpioMap($mcugpiomap[$i]);
                $tpl = $this->cc("0x0c (.*)");
                $ugm = trim(preg_replace("/^$tpl$/im", "$1", $ugm));
                $hex .= " ".$ugm;
            }
        }else if(!($mcugpiomap instanceof McuGpioMap)){
            throw new \Exception("The first parameter type is wrong!", 1);
            return null;
        }else{
            $hex = $mcugpiomap->toHex();
        }
        return $this->cc("0x0c $hex");
    }
    /**
     * 设置wifi ssid
     * @param [type] $v   wifi名称
     * @param [type] $fun 回调函数
     */
    public function setWifiSsid($v, $fun=null){
        return $this->s2b("0x02", $v, $fun);
    }
    /**
     * 设置上报服务时间间隔
     * @param [type] $v   一个整形的值
     * @param [type] $fun 回调函数
     */
    public function setWifiPassword($v, $fun=null){
        return $this->s2b("0x03", $v, $fun);
    }
    /**
     * 设置上报服务时间间隔
     * @param [type] $v   一个整形的值
     * @param [type] $fun 回调函数
     */
    public function setReportTime($v, $fun=null){
        return $this->i2b("0x04", $v, $fun);
    }
    /**
     * 设置Gpio值
     * @param [type] $gpio   0到17
     * @param [type] $v   0-1
     * @param [type] $fun 回调函数
     */
    public function setGpio($gpio, $v, $fun=null){
        if(!is_numeric($gpio) || $gpio<0 || $gpio>16)return null;
        $hex = dechex($gpio);
        $hex = (strlen($hex)<2)?"0x0$hex":"0x$hex";
        return $this->i2b("0x0b $hex", $v, $fun);
    }
    /**
     * 设置多个Gpio值
     * @param [type] $gpios   一个整形的Gpio或值
     * @param [type] $fun 回调函数
     */
    public function setGpios($gpios, $fun=null){
        $gpios = $this->getParams($gpios, "gpio");
        return $this->i2b("0x05", $gpios, $fun);
    }
    /**
     * 设置D pin值
     * @param [type] $v   0-8
     * @param [type] $fun 回调函数
     */
    public function setDpin($idx, $v=0, $fun=null){
        if(!is_numeric($idx) || $idx<0 || $idx>8)return null;
        $hex = dechex($idx);
        $hex = (strlen($hex)<2)?"0x0$hex":"0x$hex";
        return $this->i2b("0x0a $hex", $v, $fun);
    }
    /**
     * 设置多个 D pin 值
     * @param [type] $v   一个整形的D pin或值
     * @param [type] $fun 回调函数
     */
    public function setDpins($v, $fun=null){
        $v = $this->getParams($v, "d");
        return $this->i2b("0x06", $v, $fun);
    }
    /**
     * 设备服务器IP地址
     * @param [type] $ip  服务器ip地址
     * @param [type] $fun 回调函数
     */
    public function setServerIpaddress($v, $fun=null){
        $h = $this->strip2b($v);
        $v=($h==null)?null:"0x07$h";
        $v=$this->cc($v);
        if(is_callable($fun)) $v = $fun($v);
        return $v;
    }
    /**
     * 设备服务器端口
     * @param [type] $ip  服务器port端口
     * @param [type] $fun 回调函数
     */
    public function setServerPort($v, $fun=null){
        return $this->i2b("0x08", $v, $fun);
    }
    /**
     * 设备服务器IP和端口
     * @param [type] $ip    服务器port端口
     * @param [type] $port  服务器port端口
     * @param [type] $fun   回调函数
     */
    public function setServerInfo($ip, $port, $fun=null){
        $h = $this->strip2b($ip);
        if(null==$h)return null;
        $v="0x09 $h";
        $h = $this->int2b($port);
        if(null==$h)return null;
        $v.=" $h";
        $v=$this->cc($v);
        if(is_callable($fun)) $v = $fun($v);
        return $v;
    }
    private function analysisA0($hex=null){
        if(count($hex)<14)return -1;
        return hexdec($hex[14]);
    }
    private function analysisGpio($hex=null){
        if(count($hex)<14)return -1;
        $len = 4; $v = 0; $pow = 1;
        for($i=0; $i<$len; $i++){
            $h = $hex[10+$i];
            $dec = hexdec($h);
            $v+=$dec*$pow;
            $pow*=256;
        }
        for($i=0; $i< count($this->gpio); $i++){
            $dv = pow(2, $i);
            $this->gpio[$i] = ( ($v&$dv)==$dv )?1:0;
        }
        $this->analysisDs($this->gpio);
        return $this->gpio;
    }
    private function analysisDs($gpio){
        $map=[16,5,4,0,2,14,12,13,15,3,1];
        for($i=0; $i<count($this->d); $i++){
            $this->d[$i] = $gpio[$map[$i]];
        }
    }
    private function analysisId($hex=null){
        if(count($hex)<10)return -1;
        $len = 10; $id="";
        for($i=0; $i<$len; $i++){
            $b = $hex[$i];
            if(preg_match("/^0x0+$/im", $b))break;
            $id.= chr(hexdec($b));
        }
        return $id;
    }
    private function cc($cmd){return "0x2a $cmd 0x23"; }
    private function s2b($cmd, $v, $fun){
        $h = $this->str2b($v);
        if(null==$h)return null;
        $v="$cmd $h";
        if(is_callable($fun)) $v = $fun($v);
        return $this->cc($v);
    }
    private function i2b($cmd, $v, $fun){
        $h = $this->int2b($v);
        if(null==$h)return null;
        $v="$cmd $h";
        if(is_callable($fun)) $v = $fun($v);
        return $this->cc($v);
    }
    private function getParams($esp8266, $params){
        if(!($esp8266 instanceof Esp8266))return $esp8266;
        $ret = 0;
        foreach($esp8266->$params as $n=>$g){
            if($g) $ret |= pow(2, $n);
        }
        return $ret;
    }
    private function setDPinValue($idx, $v){
        if($idx<count($this->d)){
            $this->d[$idx] = intval("0$v");
            foreach($this->gpioMap as $k1=>$v1){
                $dn = strtolower($v1);
                if( $dn == "d$idx"){
                    if(preg_match("/gpio(\d+)/im", $k1, $m)){
                        $n=$m[1]*1;
                        $this->gpio[$n] = $this->d[$idx];
                    }
                }
            }
            return $this->gpio[$idx];
        }
        return -1;        
    }
    private function setGpioValue($idx, $v){
        if($idx<count($this->gpio)){
            $this->gpio[$idx] = intval("0$v");
            if(isset($this->gpioMap['GPIO$idx'])){
                $nickName = $this->gpioMap['GPIO$idx'];
                if(preg_match("/^D(\d)$/im", $nickName, $m)){
                    $this->d[$m[1]] = $this->gpio[$idx]; 
                }
            }
            return $this->gpio[$idx];
        }
        return -1;        
    }
}