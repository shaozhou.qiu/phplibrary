<?php
namespace Etsoftware\Mcu;

class McuGpioMap
{
    // 触发gpio引角
    public $srcGPIO;
    // 触发gpio引角 类型 0 表示GPIO， 1表示D引角， 2表示A引角
    public $stype;
    // 触发时引角最小值
    public $min;
    // 触发时引角最大值
    public $max;
    // 触发gpio引角 默认电平
    public $sdv;
    // 触发次数计数器，系统会自动记数
    public $counter;
    // 触发类型，点动，互锁，
    public $type;
    // 响应gpio引角, pow(2, gpio)
    public $desGPIO;    
    // 触发gpio引角 值
    public $ddv;
    function __construct()
    {
        $this->srcGPIO = 0;
        $this->stype = 0;
        $this->min = 0;
        $this->max = 0;
        $this->sdv = 0;
        $this->counter = 0;
        $this->type = 0;
        $this->desGPIO  = 0;
        $this->ddv = 0;
    }
    /**
     * 设置目标gpio引角，可传入多个参数
     */
    public function setDesGPIO(){
        $args = func_get_args();
        foreach($args as $v){
            $this->desGPIO  |= pow(2, $v*1);
        }
    }
    private function amg($data, $fun){
        if(is_array($data)){
            $reVal=null;
            foreach($data as $v)
                $reVal = $this->amg($v, $fun);
            return $reVal;
        }
        if(!($data instanceof McuGpioMap))
            return $fun?$fun(null):null;
        return $fun?$fun($data):null;
    }
    private function checkData($data){
        foreach($data as $k=>$v){
            if(is_numeric($v))continue;
            if(is_string($v)){
                $data[$k]=$v*1;
                continue;
            }
            throw new \Exception("Error $k is not numeric", 1);
        }
    }
    /**
     * 转换成二进制文件，
     */
    public function toBin($data=null){
        $hex = $this->toHex($data);
        if(null == $hex)
            return ["errcode"=>-1 , "size"=>0 , "data"=>null ];
        $reVal = ["errcode"=>0 , "size"=>0 , "data"=>null ];
        if(preg_match_all("/0x([a-f\d]+)/im", $hex, $mc, PREG_SET_ORDER)){
            foreach($mc as $m){
                $reVal['size'] += 1;
                $reVal['data'] .= chr($m[1]);
            }
        }
        return $reVal;
    }
    /**
     * 转换成二进制文件，
     */
    public function toHex($data=null){
        if(null == $data)return $this->toHex($this);
        $hex = null;
        return $this->amg($data, function($v)use(&$hex){
            if(!$v) return null;
            $hex .= Funcation::int2b($v->srcGPIO*1, 1);
            $hex .= " ".Funcation::int2b($v->stype*1, 1);
            $hex .= " ".Funcation::int2b($v->min*1, 1);
            $hex .= " ".Funcation::int2b($v->max*1, 1);
            $hex .= " ".Funcation::int2b($v->sdv*1, 1);
            $hex .= " ".Funcation::int2b($v->counter*1, 1);
            $hex .= " ".Funcation::int2b($v->type*1, 1);
            $hex .= " ".Funcation::int2b($v->desGPIO*1);
            $hex .= " ".Funcation::int2b($v->ddv*1, 1);
            return $hex;
        });
    }
 
}