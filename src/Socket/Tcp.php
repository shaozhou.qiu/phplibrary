<?php
namespace Etsoftware\Socket;

use Etsoftware\Socket\Socket;

class Tcp extends Socket
{
    private $address = '0.0.0.0';
    private $port = 7425;
    /**
     * construct
     * @param  [type] $address remote address
     * @param  [type] $port remote port
     * @return Array           
     */
    public function __construct($address, $port, $ipType=AF_INET){
        $this->address = $address;
        $this->port = $port;
    }
    /**
     * test server port
     * @param  [type] $ip  remote ip address
     * @param  [type] $port  remote port
     * @return Array           
     */
    static function test($ip, $port=80){
        $ip = gethostbyname($ip);
        $st = time(); $sock = false;
        if( filter_var( $ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6) ){        //IPv6
            $sock = socket_create(AF_INET6, SOCK_STREAM, SOL_TCP);
        }elseif( filter_var( $ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) ){    //IPv4
            $sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        }else{
            $ipv4 = gethostbyname($ip);
            if($ipv4 != $ip){
                return self::test($ipv4, $port);
            }
        }        
        if(!$sock){
            return ['status'=>0, 'code'=>-1, 'msg'=>"$ip:$port", 'timespan'=>0, 
            'errno'=> socket_last_error(), 'message'=>socket_strerror(socket_last_error())];         
        }
        socket_set_nonblock($sock);
        socket_connect($sock, $ip, $port);
        socket_set_block($sock);
        $r = $w = $f = array($sock);
        $code = socket_select($r, $w, $f, 5);
        socket_close($sock);
        $timespan = time() - $st;
        switch( $code )
        {
            case 2:
                $status = 4;
                $ret = "Closed";
                break;
            case 1:
                $status = 2;
                $ret = "Openning";
                break;
            case 0:
                $status = 1;
                $ret = "Timeout";
                break;
        }  
        $speed = 0;
        return ['status'=>$status, 'code'=>$code, 'msg'=>$ret, 'timespan'=>$timespan, 'speed'=>$speed];         
    }    
    public function connect($callback){
        return $this->bind($this->address, $this->port, 'tcp', $callback);
    }
    public function send($msg, $len, $ip, $port, $callback=null){
        $ip = $ip?$ip:$this->address;
        $port = $port?$port:$this->port;
        
        if(!$this->hdlSocket){
            $ret = $this->connect(null);
            if($ret['errno'] == 0){
                $sock = $ret['data'];
                socket_set_option($sock, SOL_SOCKET, SO_REUSEADDR, 1);
                $this->write($msg, $len, $ip, $port, $sock, $callback);
            }
            $this->close();
        }else{
            $this->write($msg, $len, $ip, $port, $this->hdlSocket, null);
        }
        
        // $client = socket_create(AF_INET, SOCK_STREAM, SOL_TCP); //创建socket/参数1:代表IPV4/参数2：流传输/参数3：TCP/
        // $result = socket_connect($client, $ip, $port); //向指定地址/端口发出连接请求，连接结果返回到resule
        // if($result == false){  //这里我们简单的对连接结果进行响应/为了Debug方便
        //     echo "ERROR CONNECT\n"; 
        // die();
        // } else {
        //     echo "CONNECTED\n";
        // }
        // socket_write($client, $msg); //将消息发送出去
        // socket_close($client); //关闭socket
        
    }
}
