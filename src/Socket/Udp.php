<?php
namespace Etsoftware\Socket;

use Etsoftware\Socket\Socket;

class Udp extends Socket
{
    private $address = '0.0.0.0';
    private $port = 7425;
    public function __construct($address, $port, $ipType=AF_INET){
        $this->address = $address;
        $this->port = $port;
    }
    public function connect($callback){
        return $this->bind($this->address, $this->port, 'udp', $callback);
    }
    public function send($msg, $len, $ip, $port, $callback=null, $timeout=2){
        $ip = $ip?$ip:$this->address;
        $port = $port?$port:$this->port;
        if($timeout==null )$timeout=2;
        if(!$this->hdlSocket){
            $ret = $this->connect(null);
            if($ret['errno'] == 0){
                $sock = $ret['data'];
                $this->sendTo($msg, $len, $ip, $port, $callback, $timeout, $sock);
            }
            $this->close();
        }else{
            $this->sendTo($msg, $len, $ip, $port, $callback, $timeout, $this->hdlSocket);
        }
    }
    
}
