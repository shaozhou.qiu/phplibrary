<?php
namespace Etsoftware\Socket;

use Etsoftware\Socket\HttpCookie;
use Etsoftware\Socket\HttpHeader;

class Http
{
    private $options = [];
    private $isPost = false;
    private $ch = null;
    private $cookie = null;
    public $header = null;
    public function __construct($ch=null){
        $this->ch = $ch;
        error_reporting(0);
        $this->setUseragent($_SERVER['HTTP_USER_AGENT']);
        $this->header = new HttpHeader();
        register_shutdown_function(array($this, "__destroy"));
    }
    public function __destroy(){
        if($this->cookie){
            if(file_exists($this->cookie->getFileName()) && !is_dir($this->cookie->getFileName())){
                unlink($this->cookie->getFileName());
            }
        }
    }
    private function init(){
        $this->setVersion(CURL_HTTP_VERSION_1_1);
        // $this->setVersion(CURL_HTTP_VERSION_1_0); // 强制使用 HTTP/1.0
        $this->setopt(CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4); // 强制使用IPV4
        $this->setTimeout(30);
        $this->setopt(CURLOPT_RETURNTRANSFER, true);
        if(null == $this->ch) $this->ch = curl_init();
        foreach ($this->options as $k => $v) curl_setopt($this->ch, $k, $v);
    }
    public function getUrl($url, $data=[], $callback=null){ 
        $ret = $this->get($url, $data, $callback);
        return $ret;
     }
    public function postUrl($url, $data=[], $callback=null){ 
        $ret = $http->post($url, $data, $callback);
        return $ret;
     }
    public function get($url, $data=[], $callback=null){ return $this->request($url, $data, "GET", "", "", $callback); }
    public function put($url, $data=[], $callback=null){ return $this->request($url, $data, "PUT", "", "", $callback); }
    public function delete($url, $data=[], $callback=null){ return $this->request($url, $data, "DELETE", "", "", $callback); }
    public function post($url, $data=[], $callback=null){ return $this->request($url, $data, "POST", "", "", $callback); }
    public function request($url, $data=[], $method="get", $contenttype="", $referer=null, $callback=null){
        if(!$this->cookie) $this->cookie = new HttpCookie(preg_replace("/.*?\.([^\/]+).*/im", "$1", $url));
        if(!$referer){$referer = $this->header->getReferer(); }
        if(!$referer){$referer = $url; }
        $method = preg_replace("/\s*/im", "", $method);
        $method = strtoupper($method);
        if($method == "POST"){
            $this->setopt(CURLOPT_POST, true);
        }else if($method == "PUT"){ // PUT
            $this->setopt(CURLOPT_CUSTOMREQUEST, "PUT");
        }else if($method == "DELETE"){ // DELETE
            $this->setopt(CURLOPT_CUSTOMREQUEST, "DELETE");
        }else{
            $this->setopt(CURLOPT_HTTPGET, true);
            $url = $url.(strstr($url, "?")?"&":"?");
            if( is_array($data) ){
                $url .= http_build_query($data);
            }else{
                if(null != $data)$url .= $data;
            }
            $data = [];
        }
        $postData = $data;
        if(count($data)){
            $contenttype = $this->header->getContentType();
            if($contenttype){
                //ÉèÖÃpostÊý¾Ý
                if(preg_match("/(.*\/json\s*$)/im", $contenttype)){
                    $postData = json_encode($postData);
                }else if(preg_match("/(.*\/x-www-form-urlencoded\s*$)/im", $contenttype)){
                    $postData = http_build_query($postData);
                }
            }
            $this->setopt(CURLOPT_POSTFIELDS, $postData);
        }

        $header=$this->header->toArray();
        $this->setopt(CURLOPT_HTTPHEADER, $header);
        if( $this->header->getAcceptEncoding()){
            $this->setopt(CURLOPT_ENCODING, $this->header->getAcceptEncoding());
        }
        

        $this->setopt(CURLOPT_URL, $url);
            

        if(preg_match("/^https:\/\//im", $url)){
            $this->setopt(CURLOPT_SSL_VERIFYPEER, FALSE); // ¶ÔÈÏÖ¤Ö¤ÊéÀ´Ô´µÄ¼ì²é
            $this->setopt(CURLOPT_SSL_VERIFYHOST, FALSE); // ´ÓÖ¤ÊéÖÐ¼ì²éSSL¼ÓÃÜËã·¨ÊÇ·ñ´æÔÚ ²»ÑéÖ¤Ö÷»ú
        }

        // $this->setopt(CURLOPT_COOKIE, $this->cookie->toString()); // ÉèÖÃcookie
        $this->setopt(CURLOPT_COOKIEFILE, $this->cookie->getFileName()); // ÉèÖÃcookie
        $this->setopt(CURLOPT_COOKIEJAR, $this->cookie->getFileName()); // ÉèÖÃcookie

        $this->setopt(CURLOPT_FOLLOWLOCATION, true); // ¸úËæÖØ¶¨Ïò
        $this->setopt(CURLOPT_MAXREDIRS, 20); // ÖØ¶¨Ïò´ÎÊý

        $this->setopt(CURLOPT_REFERER, $referer); // À´Ô´Ò³Ãæ

        $this->init();

        $response = curl_exec($this->ch);
        $httpInfo =curl_getinfo($this->ch);
        $httpInfo['errcode'] = $httpInfo['errno'] = curl_errno( $this->ch );
        // $httpCode = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
        

        if ($response === FALSE) {
            $errmsg = curl_error($this->ch);
            $errmsg = $errmsg?$errmsg:"Error: curl errno[".curl_errno( $this->ch )."]!";
            $httpInfo['data'] = $errmsg;
            return $httpInfo;
        }
        if(preg_match("/.*\s+charset\s*=\s*([^\; ]+)/im", $httpInfo['content_type'], $m)){
            if(!preg_match("/UTF/im", $m[1])){
                // GBK,UTF-8,ASCII  to UTF-8
                $response = mb_convert_encoding($response, "UTF-8", "GBK,UTF-8,ASCII");
            }
        }
        $ret  = array_merge($httpInfo, [
             'method' => $method
             ,'request_data' => $postData
             ,'request_header' => $header
             ,'data' => $response
        ]);
        $this->close();

        if(null!=$callback){
            $ret = call_user_func_array($callback, [$this, $info]);
            return $ret;
        }
        return $ret;
    }
    /**
     * ¹Ø±ÕÁ¬½Ó
     * @return [type] [description]
     */
    public function close(){
        if(!$this->ch) curl_close($this->ch);
        $this->ch = null;
    }
    /**
     * ÉèÖÃ User agent
     * @param [type] $curl_http_version [description]
     */
    public function setUseragent($useragent){
        $this->setopt(CURLOPT_USERAGENT, $useragent);
    }
    /**
     * ÉèÖÃ°æ±¾
     * @param [type] $curl_http_version [description]
     */
    public function setVersion($curl_http_version){
        $this->setopt(CURLOPT_HTTP_VERSION, $curl_http_version);
    }
    /**
     * ³¬Ê±ÉèÖÃ
     * @param  [type] $n [description]
     * @return [type]    [description]
     */
    public function setTimeout($timeout){
        // ÔÚ³É¹¦Á¬½Ó·þÎñÆ÷Ç°µÈ´ý¶à¾Ã
        $this->setopt(CURLOPT_CONNECTTIMEOUT, $timeout);
        // ´Ó·þÎñÆ÷½ÓÊÕ»º³åÍê³ÉÇ°ÐèÒªµÈ´ý¶à³¤Ê±¼ä
        $this->setopt(CURLOPT_TIMEOUT, $timeout);
        return $this;
    }
    public function setopt($key, $value){
        if(isset( $this->options[$key]  )){
            $this->options[$key] = $value;
            return $this;
        }
        $this->options[$key] = $value;
        return $this;
    }
}