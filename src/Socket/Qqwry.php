<?php
namespace Etsoftware\Socket;

use Etsoftware\Socket\Http;
use Etsoftware\Lib\File;
use Etsoftware\Lib\Memory;
use Etsoftware\Socket\IpInformation;

/**
* 构造函数，打开 QQWry.Dat 文件并初始化类中的信息
* http://www.cz88.net/ip/
* http://www.cz88.net/fox/ipdat.shtml
* http://ipaddr.cz88.net/data.php?ip=113.102.166.20
*/
class Qqwry {
    //数据文件指针
    private $database_path = "/db";
    private $fp;
    private $firstip;
    private $lastip;
    private $totalip;
    private $encoding;
    /**
    * 构造函数，打开 QQWry.Dat 文件并初始化类中的信息
    */
    public function __construct($filename = null, $encoding = 'utf8'){
        if( !$filename ){
            $ret = preg_match("/(.*)(\/[^\/]*){3}$/im", __FILE__, $m);
            if($ret){
                $filename = ( $m[1].$this->database_path );
            }
        }
        $this->encoding = $encoding;
        $this->fp = 0;
        
        $filename = $this->findbase($filename);
        if($filename && file_exists($filename)){
            if (($this->fp = @fopen($filename, 'rb')) !== false) {
                $this->firstip = $this->getlong();
                $this->lastip = $this->getlong();
                $this->totalip = ($this->lastip - $this->firstip) / 7;
                //注册析构函数，使其在程序执行结束时执行
                register_shutdown_function(array(&$this, 'destroy'));
            }
        }
    }
    private function findbase($path) {
        if(!is_dir($path)){ $path = "/"; }
        $fn = null;
        $data = scandir($path);
        foreach ($data as $value){
            if($value != '.' && $value != '..'){
                if(preg_match("/qqwry.dat/", $value)){
                    $fn = "$path/$value";
                    break;
                }
            }
        }
        // if($fn==null){
        //     $m = new Memory(ftok(__FILE__, "t"));
        //     $fn = $m->read();
        //     if(!$fn || !file_exists($fn) ){
        //         $pingresult = exec("find / -name \"[Qq][Qq][Ww][Rr][Yy]*.[dD][Aa][Tt]\"", $outline, $status);
        //         if(count($outline)){
        //             $fn = $outline[0];
        //         }
        //         $m->write($fn);
        //     }
        // }
        return $fn;
    }
    private function getlong() {
        //unpack从二进制字符串对数据进行解包
        //将读取的little-endian编码的4个字节转化为长整型数,fread安全读取二进制文件
        $result = unpack('Vlong', fread($this->fp, 4));
        return $result['long'];
    }


    private function getlong3() {
        //将读取的little-endian编码的3个字节转化为长整型数
        $result = unpack('Vlong', fread($this->fp, 3).chr(0));
        return $result['long'];
    }


    private function packip($ip) {
        //pack把数据装入一个二进制字符串
        //ip2long将IP地址转成无符号的长整型，也可以用来验证IP地址
        return pack('N', intval(ip2long($ip)));
    }


    private function getstring($data = "") {
        $char = fread($this->fp, 1);
        while (ord($char) > 0) {    //ord返回字符的ASCII值，字符串按照C格式保存，以\0结束
            $data .= $char;
            $char = fread($this->fp, 1);
        }
        return mb_convert_encoding($data, $this->encoding, "gbk");
    }


    private function getarea() {
        $byte = fread($this->fp, 1);  // 标志字节
        switch (ord($byte)) {
            case 0:           // 没有区域信息
                $area = "";
                break;
            case 1:
            case 2:           // 标志字节为1或2，表示区域信息被重定向
                fseek($this->fp, $this->getlong3());
                $area = $this->getstring();
                break;
            default:          // 否则，表示区域信息没有被重定向
                $area = $this->getstring($byte);
                break;
        }
        return $area;
    }
    static public function ipv4infomation($ipv4) {
        $ret = (new self())->getlocation($ipv4);
        if($ret==null) $ret = (new self)->getlocationByOnline($ipv4);
        return $ret;
    }
    // 在线获取IP信息
    public function getlocationByOnline($ip) {
        if( filter_var( $ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) ){    //IPv4
            $sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        }else{
            $ipv4 = gethostbyname($ip);
            if($ipv4 != $ip){
                return $this->getlocationByOnline($ipv4);
            }
        }
        $reVal = null;
        $ret = IpInformation::get($ip);
        if(!$ret){return null;}
        $area = $ret['area'];
        $country = "";
        if(preg_match("/^\s*(([^\s]+国)|([^\s]+岛)|([^\s]+利亚)|([^\s]+维亚)|([^\s]+斯坦)|([^\s]+尼亚)|(蒙古)|(朝鲜)|(日本)|(菲律宾)|(越南)|(老挝)|(柬埔寨)|(缅甸)|(马来西亚)|(文莱)|(新加坡)|(印度尼西亚)|(东帝汶)|(尼泊尔)|(不丹)|(印度)|(斯里兰卡)|(马尔代夫)|(阿富汗)|(伊拉克)|(伊朗)|(约旦)|(黎巴嫩)|(以色列)|(沙特阿拉伯)|(巴林)|(卡塔尔)|(科威特)|(阿曼)|(也门)|(格鲁吉亚)|(阿塞拜疆)|(土耳其)|(塞浦路斯)|(芬兰)|(瑞典)|(挪威)|(丹麦)|(立陶宛)|(白俄罗斯)|(俄罗斯)|(乌克兰)|(摩尔多瓦)|(波兰)|(捷克)|(斯洛伐克)|(匈牙利)|(奥地利)|(瑞士)|(列支敦士登)|(爱尔兰)|(荷兰)|(比利时)|(卢森堡)|(摩纳哥)|(马其顿)|(希腊)|(克罗地亚)|(波斯尼亚和墨塞哥维那)|(意大利)|(梵蒂冈)|(圣马力诺)|(马耳他)|(西班牙)|(葡萄牙)|(安道尔)|(埃及)|(利比亚)|(苏丹)|(突尼斯)|(摩洛哥)|(埃塞俄比亚)|(厄立特里亚)|(索马里)|(吉布提)|(乌干达)|(卢旺达)|(布隆迪)|(塞舌尔)|(刚果）)|(圣多美及普林西比)|(）)|(塞内加尔)|(冈比亚)|(马里)|(布基纳法索)|(几内亚)|(几内亚比绍)|(佛得角)|(塞拉利昂)|(利比里亚)|(科特迪瓦)|(加纳)|(多哥)|(贝宁)|(尼日尔)|(赞比亚)|(安哥拉)|(津巴布韦)|(马拉维)|(莫桑比克)|(博茨瓦纳)|(纳米比亚)|(南非)|(斯威士兰)|(莱索托)|(马达加斯加)|(科摩罗)|(毛里求斯)|(留尼旺)|(圣赫勒拿)|(新西兰)|(巴布亚新几内亚)|(瓦努阿图)|(密克罗尼西亚)|(帕劳)|(瑙鲁)|(基里巴斯)|(图瓦卢)|(萨摩亚)|(汤加)|(法属波利尼西亚)|(瓦利斯与富图纳)|(纽埃)|(托克劳)|(美属萨摩亚)|(北马里亚纳)|(加拿大)|(墨西哥)|(格陵兰)|(危地马拉)|(伯利兹)|(萨尔瓦多)|(洪都拉斯)|(尼加拉瓜)|(哥斯达黎加)|(巴拿马)|(巴哈马)|(古巴)|(牙买加)|(海地)|(安提瓜和巴布达)|(圣基茨和尼维斯)|(多米尼克)|(圣卢西亚)|(圣文森特和格林纳丁斯)|(格林纳达)|(巴巴多斯)|(特立尼达和多巴哥)|(波多黎各)|(安圭拉)|(蒙特塞拉特)|(瓜德罗普)|(马提尼克)|(荷属安的列斯)|(阿鲁巴)|(百慕大)|(哥伦比亚)|(委内瑞拉)|(圭亚那)|(法属圭亚那)|(苏里南)|(厄瓜多尔)|(秘鲁)|(巴西)|(智利)|(阿根廷)|(乌拉圭)|(巴拉圭))/im", $area, $m)){
            $country = $m[1];
        }
        return [
            'ip' => $ret['ipv4']
            ,'platform' => ""
            ,'beginip' => ''
            ,'endip' => ''
            ,'country' => $country
            ,'area' => $ret['area']
        ];

        // $ret = (new Http)->getUrl("http://ipaddr.cz88.net/data.php?ip=".urlencode($ip) );
        // if($ret['errcode'] != 0){ return null; }
        // if($ret['http_code'] == 200){
        //     $content = $ret['data'];
        //     $content = mb_convert_encoding($content, 'UTF-8', 'UTF-8,GBK,GB2312,BIG5');
        //     if(preg_match("/'([^']*)','([^']*)','([^']*)'/im", $content, $m)){
        //         $country = preg_replace("/(.*国).*/im", "$1", $m[2]);
        //         $reVal = [
        //             'ip' => $m[1]
        //             ,'platform' => $m[3]
        //             ,'beginip' => ''
        //             ,'endip' => ''
        //             ,'country' => $country
        //             ,'area' => $m[2]
        //         ];
        //     }
        // }
        // return $reVal;
    }
    public function getlocation($ip) {
        if (!$this->fp) return null;      // 如果数据文件没有被正确打开，则直接返回空
        $location['ip'] = gethostbyname($ip);  // 域名转化为IP地址
        $ip = $this->packip($location['ip']);  // 将输入的IP地址转化为可比较的IP地址
        // 不合法的IP地址会被转化为255
        // 对分搜索
        $l = 0;             // 搜索的下边界
        $u = $this->totalip;      // 搜索的上边界
        $findip = $this->lastip;    // 如果没有找到就返回最后一条IP记录（QQWry.Dat的版本信息）
        while ($l <= $u) {       // 当上边界小于下边界时，查找失败
            $i = floor(($l + $u) / 2); // 计算近似中间记录
            fseek($this->fp, $this->firstip + $i * 7);
            $beginip = strrev(fread($this->fp, 4));   // 获取中间记录的开始IP地址,strrev反转字符串
            // strrev函数在这里的作用是将little-endian的压缩IP地址转化为big-endian的格式，便于比较
            //关于little-endian与big-endian 参考：http://baike.baidu.com/view/2368412.htm
            if ($ip < $beginip) {    // 用户的IP小于中间记录的开始IP地址时
            $u = $i - 1;      // 将搜索的上边界修改为中间记录减一
            }
            else {
                fseek($this->fp, $this->getlong3());
                $endip = strrev(fread($this->fp, 4));  // 获取中间记录的结束IP地址
                if ($ip > $endip) {   // 用户的IP大于中间记录的结束IP地址时
                    $l = $i + 1;    // 将搜索的下边界修改为中间记录加一
                } else {         // 用户的IP在中间记录的IP范围内时
                    $findip = $this->firstip + $i * 7;
                    break;       // 则表示找到结果，退出循环
                }
            }
        }


        fseek($this->fp, $findip);
        $location['beginip'] = long2ip($this->getlong());  // 用户IP所在范围的开始地址
        $offset = $this->getlong3();
        fseek($this->fp, $offset);
        $location['endip'] = long2ip($this->getlong());   // 用户IP所在范围的结束地址
        $byte = fread($this->fp, 1);  // 标志字节
        switch (ord($byte)) {
            case 1:           // 标志字节为1，表示国家和区域信息都被同时重定向
                $countryOffset = $this->getlong3();     // 重定向地址
                fseek($this->fp, $countryOffset);
                $byte = fread($this->fp, 1);  // 标志字节
                switch (ord($byte)) {
                    case 2:       // 标志字节为2，表示国家信息又被重定向
                        fseek($this->fp, $this->getlong3());
                        $location['country'] = $this->getstring();
                        fseek($this->fp, $countryOffset + 4);
                        $location['area'] = $this->getarea();
                        break;
                    default:      // 否则，表示国家信息没有被重定向
                        $location['country'] = $this->getstring($byte);
                        $location['area'] = $this->getarea();
                        break;
                }
                break;
            case 2:           // 标志字节为2，表示国家信息被重定向
                fseek($this->fp, $this->getlong3());
                $location['country'] = $this->getstring();
                fseek($this->fp, $offset + 8);
                $location['area'] = $this->getarea();
                break;
            default:          // 否则，表示国家信息没有被重定向
                $location['country'] = $this->getstring($byte);
                $location['area'] = $this->getarea();
                break;
        }
        if ($location['country'] == " CZNET") { // CZNET表示没有有效信息
            $location['country'] = "未知";
        }
        if ($location['area'] == " CZNET") {
            $location['area'] = "";
        }
        return $location;
    }
    
    /**
    * 析构函数，用于在页面执行结束后自动关闭打开的文件
    */
    public function destroy() {
        if ($this->fp) { fclose($this->fp); }
        $this->fp = 0;
    }
}