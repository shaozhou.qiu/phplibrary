<?php
namespace Etsoftware\Socket;



class HttpHeader
{
    private $data = [];
    public function __construct(){        
        $this->set("Connection", "Keep-Alive");
        $this->set("Keep-Alive", 300);
        //告诉浏览器，可以把所属本站的所有 http 连接升级为 https 连接
        $this->set("Upgrade-Insecure-Requests", 1);
        $this->set("Accept-Encoding", "*");
        $this->set("Accept-Language", "*");
    }
    public function set($key, $val){
        $this->data[$key] = $val;
        return $this;
    }
    public function load($data){
        $this->data = $data;
    }
    public function get($key){
        return $this->data[$key]??null;
    }
    public function toArray(){
        $arys=[];
        foreach ($this->data as $k => $v) {
            array_push($arys, "$k:$v");
        }
        return $arys;
    }
    private function resetkey($key){
        $key = preg_replace("/\_/im", "-", $key);
        if(preg_match_all("/([^\-^])([A-Z])/m", $key, $mc, PREG_SET_ORDER)){
            foreach ($mc as $m) {
                $key = preg_replace("/".$m[0]."/m", $m[1]."-".$m[2], $key);
            }
        }
        return $key;
    }
    function __call($fun, $args){
        if(preg_match("/^set(.+)$/im", $fun, $m)){
            array_unshift($args, $this->resetkey($m[1]));
            call_user_func_array([$this,'set'], $args);
            return $this;
        }else if(preg_match("/^get(.+)$/im", $fun, $m)){
            array_unshift($args, $this->resetkey($m[1]));
            return call_user_func_array([$this,'get'], $args);
        }
    }
}