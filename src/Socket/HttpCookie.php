<?php
namespace Etsoftware\Socket;

use Etsoftware\Lib\File;

class HttpCookie
{
    private $fileName = "";
    private $data = [];
    public function __construct($file=null){
        if($file){
            if(preg_match("/^[^\\".preg_quote(DIRECTORY_SEPARATOR)."]+$/im", $file)){
                $file = preg_replace("/[^\w]+/im", "_", $file);
                $this->fileName = $this->createPath().DIRECTORY_SEPARATOR.$file.".tmp";
            }else{
                $this->fileName = $file;
            }
        }else{
            $this->fileName = $this->createPath().DIRECTORY_SEPARATOR.mt_rand().".tmp";
        }
        if(file_exists($this->fileName)){
            $f = new File($this->fileName);
            $ret = $f->read();
            if($ret['errcode']==0) $this->load($ret['data']);
        }
    }
    /**
     * @param  [String] $key delete data with key
     * @return [type]
     */
    public function delete($key){unset($this->data[$key]); }
    /**
     * @param [String] $key key name
     * @param [String] $value content
     */
    public function add($key, $value){$this->data[$key] = $value; }
    /**
     * @return empty data
     */
    public function empty(){
        $this->data=[];
        (new File($this->fileName))->write("");
    }
    public function getFileName(){return $this->fileName;}
    public function save(){
        (new File($this->fileName))->save($this->toString());
    }
    public function load($content){
        $content = preg_replace("/^\s*#.*$/im", "", $content);
        if(preg_match_all("/([^;\=]+)=([^;\=]+)/", $content, $mc, PREG_SET_ORDER)){
            foreach($mc as $m){
                $this->add($m[1], $m[2]);
            }
        }
    }
    public function toArray(){return $this->data;}
    /**
     * @return string
     */
    public function toString(){
        $reVal = "";
        foreach($this->data as $k=>$v){
            $reVal .= ";$k=$v";
        }
        return substr($reVal,1);
    }
    private function createPath(){
        $path = $_SERVER['DOCUMENT_ROOT'];
        if(file_exists("$path/downloadfiles")){
            $path .= "/downloadfiles";
        }
        $path = "$path/cookie";
        if(!file_exists($path))mkdir($path);
        return $path;
    }
    
}