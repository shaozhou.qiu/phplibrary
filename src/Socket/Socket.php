<?php
namespace Etsoftware\Socket;

use Etsoftware\Lib\File;
use Etsoftware\Socket\Qqwry;

class Socket
{
    protected $hdlSocket = null;
    protected $hdlASockets = [];
    protected $length = 1024; //The number of bytes to receive from the socket.
    protected $type = SOCK_STREAM;
    protected $ipType = AF_INET;
    protected $backlog = 4; // 呼入连接数量限制，

    public function __construct($ipType = AF_INET){
        if($ipType == AF_INET6){
            $this->ipType = AF_INET6;
        }else if($ipType == AF_UNIX){
            $this->ipType = AF_UNIX;
        }
    }
    /**
     * whitelist
     * @param  [type] $ipv4 
     * @param  [type] $data  default data
     * @return [type]        
     */
    static function whitelist($data, $ipv4=null){
        if(!$data) return false;
        $ipv4 = $ipv4?$ipv4: self::getUserIP();
        return (array_search($ipv4, $data) === FALSE)?false:true;
    }
    /**
     * get ip infomation
     * @param  [type] $ip ipv4
     * @return Array           
     */    
    static function ping($ipv4){
        $status = -1;
        if (strcasecmp(PHP_OS, 'WINNT') === 0) {
            // Windows 服务器下
            $pingresult = exec("ping -n 1 {$ipv4}", $outcome, $status);
        } elseif (strcasecmp(PHP_OS, 'Linux') === 0) {
            // Linux 服务器下
            $pingresult = exec("ping -c 1 {$ipv4}", $outcome, $status);
        }
        if (0 == $status) {
            $status = true;
        } else {
            $status = false;
        }
        return $status;        
    }
    /**
     * get user ip
     * @param  [type] $ip ipv4
     * @return Array           
     */    
    static function getUserIP(){
        $svr = $_SERVER;
        if (isset($svr["HTTP_CLIENT_IP"]) && strcasecmp($svr["HTTP_CLIENT_IP"], "unknown")<1) {
            $ip = $svr["HTTP_CLIENT_IP"];
        } else {
            if (isset($svr["HTTP_X_FORWARDED_FOR"]) && strcasecmp($svr["HTTP_X_FORWARDED_FOR"], "unknown")<1) {
                $ip = $svr["HTTP_X_FORWARDED_FOR"];
            } else {
                if (isset($svr["REMOTE_ADDR"]) && strcasecmp($svr["REMOTE_ADDR"], "unknown")<1) {
                    $ip = $svr["REMOTE_ADDR"];
                }else{
                    $ip = "unknown";
                }
            }
        }
        return ($ip);
    }
    /**
     * get ip infomation
     * @param  [type] $ip ipv4
     * @return Array           
     */    
    static function ipv4Info($ipv4){
        $qqwry = new Qqwry();
        $info = $qqwry->getlocation($ipv4);
        if(!$info){
            $info = $qqwry->getlocationByOnline($ipv4);
        }
        return $info;

    }
    /**
     * construct
     * @param  [type] $address remote address
     * @param  [type] $port remote port
     * @param  [type] $port remote type
     * @return Array           
     */    
    protected function bind($address, $port, $type, $callback=null){
        if($this->hdlSocket)return $this;
        $protocol = SOL_TCP;
        if($type == 'udp'){
            $protocol = SOL_UDP;
            $this->type = SOCK_DGRAM;
        }else if($type == 'seqpacket'){
            $this->type = SOCK_SEQPACKET;
        }else if($type == 'icmp'){
            $protocol = SOL_ICMP;
            $this->type = SOCK_RAW;
        }else if($type == 'rdm'){
            $this->type = SOCK_RDM;
        }
        $this->hdlSocket = socket_create($this->ipType, $this->type, $protocol);
        if(!$this->hdlSocket){
            return $this->getLastError();
        }
        if(!socket_set_block($this->hdlSocket)){
            $this->close();
            return $this->getLastError();
        }
        if(!$callback){
            return ['errno'=> 0, 'message'=>'ok' , 'data'=>$this->hdlSocket ];
        }
        $ret = socket_bind($this->hdlSocket, $address, $port);
        if(!$ret){
            $this->close();
            return $this->getLastError();
        }
        if($this->type == SOCK_DGRAM){
            return $this->receive($this->hdlSocket, function(...$args)use($callback){
                $ret = call_user_func_array($callback, $args);
                return true;
            });
        }else{
            return $this->listen($this->hdlSocket, function(...$args)use($callback){
                $ret = call_user_func_array($callback, $args);
                return true;
            });
        }
    }
    private function getLastError(){
        return ['errno'=> socket_last_error(), 'message'=>socket_strerror(socket_last_error())];

    }
    /**
     * 接收消息（无连接）
     * @param  [type] $callback [description]
     * @return [type]           [description]
     */
    protected function receive($hdlSocket, $callback, $timeout=0){
        $loop = true;
        do {
            if($timeout>0){
                // 超时 5 秒
                socket_set_option($hdlSocket,SOL_SOCKET,SO_RCVTIMEO,array("sec"=>$timeout, "usec"=>0 ) );
            }
            $len = socket_recvfrom($hdlSocket, $buf, $this->length, 0, $address, $port);
            $ret = [
                'length' => $len
                , 'data' => $buf
                , 'address' => $address
                , 'port' => $port
            ];
            if($callback){
                $loop = call_user_func_array($callback, $ret);
            }
        } while ( $loop);
    }
    /**
     * 发送数据（无连接）
     * @param  [type] $msg      [description]
     * @param  [type] $len      [description]
     * @param  [type] $ip       [description]
     * @param  [type] $port     [description]
     * @param  [type] $callback [description]
     * @param  [type] $sock     [description]
     * @return [type]           [description]
     */
    protected function sendTo($msg, $len, $ip, $port, $callback=null, $timeout=5, $sock=null){
        $sock = $sock?$sock:$this->hdlSocket;
        $len = $len?$len:strlen($msg);
        socket_sendto($sock, $msg, $len, 0, $ip, $port);
        if($callback){
            $this->receive($sock, function(...$args)use($callback){
                call_user_func_array($callback, $args);
                return false;
            }, $timeout);
        }
    }

    /**
     * 监听消息（有连接）
     * @param  [type] $callback [description]
     * @return [type]           [description]
     */
    private function listen($hdlSocket, $callback){
        $ret = socket_listen($hdlSocket, $this->backlog);
        if(!$ret){
            $this->close();
            return $this->getLastError();
        }
        $loop = true;
        do {
            $ret = ['errno'=> 0, 'message'=>'ok'];
            $hdl = socket_accept($hdlSocket);
            if(!$hdl){
                return $this->getLastError();
            }else{
                array_push($this->hdlASockets, $hdl);
                $buf = socket_read($hdl, $this->length);
                $ret = [
                    'length' => 0
                    , 'data' => $buf
                    , 'address' => ""
                    , 'port' => ""
                    , 'sock' => $hdl
                ];
            }
            if($callback){
                $loop = call_user_func_array($callback, $ret);
            }
            // socket_close($hdl); 
        } while ( $loop );
    }
    /**
     * 发送消息（有连接）
     * @param  [type] $msg  [description]
     * @param  [type] $size [description]
     * @param  [type] $sock  [description]
     * @return [type]       [description]
     */
    protected function write($msg, $size, $ip, $port, $sock=null, $callback = null){
        $sock = $sock?$sock:$this->hdlSocket;
        $size = $size?$size:strlen($msg);
        $ret = socket_connect($sock, $ip, $port);
        if(!$ret){
            return $this->getLastError();
        }
        $ret = socket_write($sock, $msg, $size);
        if(!$ret){
            return $this->getLastError();
        }
        if($callback){
            $this->listen($sock, function(...$args)use($callback){
                call_user_func_array($callback, $args);
                return false;
            });    
        }
        return ['errno'=> 0, 'message'=>'ok'];
    }

    public function close(){
        if( $this->hdlSocket ){
            socket_close($this->hdlSocket);
            $this->hdlSocket = null;
        }
        for(;count($this->hdlASockets);){
            $hdl = array_shift($this->hdlASockets);
            socket_close( $hdl );
        }
        return $this;
    }
    public function __destory()
    {
        $this->close();
    }
}