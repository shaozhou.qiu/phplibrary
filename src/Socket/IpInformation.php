<?php
namespace Etsoftware\Socket;

use Etsoftware\Socket\Http;
use Etsoftware\Template\Template;


class IpInformation {
    /**
     * 根据IPV4获取信息
     * @param  [type] $ipv4 [description]
     * @return [type]       [description]
     */
    static public function get($ipv4) {
        return (new self)->getInfomation($ipv4);
    }
    private $apis = [
        ['url'=>'https://ip.tool.chinaz.com/{{ipv4}}', 'data'=>null, "regexp"=>"/<em>\s*(.+?)\s*<\/em>\s*<.*ip138提供/im"]
        ,['url'=>'https://www.ip138.com/iplookup.asp?ip={{ipv4}}&action=2', 'data'=>null, "regexp"=>"/\{\"ASN归属地\"\s*:\s*\"\s*(.+?)\s*\"/im"]
        ,['url'=>'https://ip.900cha.com/{{ipv4}}.html', 'data'=>null, "regexp"=>"/参考归属地.*\->\s*(.+?)\s*\n/im"]
        ,['url'=>'http://ip.yqie.com/ip.aspx?ip={{ipv4}}', 'data'=>null, "regexp"=>"/name=\"AddressInfo\".*?value=\"\s*(.+?)\s*\"/im"]
        ,['url'=>'http://ip.t086.com/?ip={{ipv4}}', 'data'=>null, "regexp"=>"/所在区域.*?class=\s*\"f1\"\s*>\s*(.*+)\s*</im"]
    ];
    /**
    * 构造函数，打开 QQWry.Dat 文件并初始化类中的信息
    */
    public function __construct(){

    }
    private function isPrivate($ipv4){
        $ary=[
            "A"=>["10.0.0.0", "10.255.255.255"]
            ,"B"=>["172.16.0.0", "172.31.255.255"]
            ,"C"=>["192.168.0.0", "192.168.255.255"]
            ,"LOCLHOAT"=>["127.0.0.1", "127.0.0.1"]
        ];
        $ipv4l = ip2long($ipv4);
        foreach( $ary as $k => $v ){
            $is = ip2long($v[0]);
            $ie = ip2long($v[1]);
            if($is <= $ipv4l && $ipv4l <= $ie){
                return true;
            }
        }
        return false;
    }
    private function getType($ipv4){
        $ary=[
            "A"=>["0.0.0.0", "127.255.255.255"]
            ,"B"=>["128.0.0.0", "191.255.255.255"]
            ,"C"=>["192.0.0.0", "223.255.255.255"]
            ,"D"=>["224.0.0.0", "239.255.255.255"]
            ,"E"=>["240.0.0.0", "247.255.255.255"]
        ];
        $ipv4l = ip2long($ipv4);
        foreach( $ary as $k => $v ){
            $is = ip2long($v[0]);
            $ie = ip2long($v[1]);
            if($is <= $ipv4l && $ipv4l <= $ie){
                return $k;
            }
        }
        return "";
    }
    public function getInfomation($ipv4){
        $reval = [
            "ipv4"=>$ipv4
            ,"type"=>$this->getType($ipv4)
            ,"private"=>$this->isPrivate($ipv4)
        ];
        if ($reval["private"]) {
            return array_merge($reval, [
                'api'=>""
                ,'area'=>"内网"
            ]);
        }
        $tpl = new Template;
        foreach ($this->apis as $v) {
            
            $regexp = $v['regexp'];

            $url = $tpl->render($v['url'], $reval);
            $data = $v['data'];
            $method = $data?"POST":"GET";
            $contenttype = "";
            $referer = $url;

            $ret = (new Http)->request($url, $data, $method, $contenttype, $referer);
            if($ret['http_code'] != 200){continue ;}

            $content = $ret['data'];
            if(!preg_match($regexp, $content, $m)){
                continue;
            }
            $reval["api"] = $url;
            $reval["area"] = $m[1];
            break;
        }
        return isset($reval["api"])?$reval:null;
    }

}