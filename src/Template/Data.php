<?php
namespace Etsoftware\Template;
use Etsoftware\Lib\File;

class Data
{
    
    public function __construct($data){
        if(!$data)return ;
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
    }
    public function fill($tpl, $funk=null){
        if(!$tpl) return $tpl;
        foreach ($this as $k => $v) {
            $pattern = $funk?$funk("\\$?".preg_quote("$k"), $v):preg_quote("$$k");
            if(is_array($v) || is_object($v)){
                $tpl = (new self($v))->fill($tpl, function($f)use($funk, $k){
                    $f1 = $f;
                    if( preg_match("/^\(\(([^\.]+)\\\.([^\)]+)\)\|.*\)\)$/im", $f, $m) ){
                        $f1 = $m[1]."\]\[".$m[2]."";
                        // echo("$f<br>");
                    }
                    $key = "(";
                    $key .= "(\\\$$k\.$f1)";
                    $key .= "|(\\\$$k\[\s*$f1\s*\])";
                    $key .= "|(\\\$$k\[\s*\"\s*$f1\s*\"\s*\])";
                    $key .= "|(\\\$$k\[\s*'\s*$f1\s*'\s*\])";
                    $key .= ")";
                    return $funk($key);
                });
                if(is_object($v)){
                    if(is_callable($v)){
                        $v = $v($k);
                    }else{
                        $v = "object{}";
                    }
                }else{
                    $v = "Array()";
                }
            }
            $tpl = preg_replace("/$pattern/im", $v, $tpl);
        }
        return $this->analysisFunctionLbl($tpl, $funk);
    }
    private function analysisFunctionLbl($tpl, $funk=null){
        return $tpl;
    }
    public function __set($key, $val){
        $this->$key=$val;
    }
    public function __get($key){
        return $this->$key;
    }
}