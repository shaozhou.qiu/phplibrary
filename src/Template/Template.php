<?php
namespace Etsoftware\Template;
use Etsoftware\Template\Data;
use Etsoftware\Template\Func;
use Etsoftware\Template\Grammar;
use Etsoftware\Template\Label;
use Etsoftware\Lib\File;

class Template
{
    private $data = null;
    private $esc = '@';
    private $qualifier = ['{{', '}}'];
    public function __construct(){
        
    }
    public function render($tpl, $data=null){
    	if(!$tpl) return $tpl;
        if(is_array($tpl)){
            foreach ($tpl as $k => $v) {
                $tpl[$k] = $this->render($v, $data);
            }
            return $tpl;
        }

    	$this->setData($data);
    	$data = $this->data;
    	$funk = function($k){
            return $this->getKey($k);
        };
    	return $this->filterEscLbl($tpl, function($tpl)use($data, $funk){
            $tpl = $this->data->fill($tpl, $funk);
	    	$tpl = (new Func($data))->render($tpl, $funk);
	    	$tpl = (new Grammar($data))->render($tpl, $funk);

            $label = new Label();
            $tpl = $label->fill($tpl, $funk);
            // dump($tpl); die;

            $tpl = preg_replace("/".$funk(".*?")."/im", "", $tpl);
	    	return $tpl;
    	});
    }
    /**
     * filter esc label
     * @param  [type] $tpl      [description]
     * @param  [type] $callback [description]
     * @return [type]           [description]
     */
    private function filterEscLbl($tpl, $callback){
    	if(!$tpl) return $tpl;
    	$esclbls = [];
    	if($this->esc){
	    	$pattern = "/".preg_quote($this->esc)."(".$this->getKey(".*").")/im";
	    	preg_match_all($pattern, $tpl, $mc, PREG_SET_ORDER);
	    	foreach ($mc as $i => $v) {
	    		$lbl = "tpl_esc_label_".mt_rand(10000, 99999);
	    		$tpl = preg_replace("/".preg_quote($v[0])."/im", $lbl, $tpl);
	    		$esclbls[ $v[1] ] = $lbl;
	    	}
    	}
    	if(is_callable($callback)){ $tpl = $callback($tpl); }
    	foreach ($esclbls as $k => $v) {
    		$tpl = preg_replace("/".preg_quote($v)."/im", $k, $tpl);
    	}
    	return $tpl;
    }
    /**
     * get label with key
     * @param  [type] $key [description]
     * @return [type]      [description]
     */
    private function getKey($key){
    	$pattern = preg_quote($this->qualifier[0])."\s*";
    	$pattern .= $key??'';
    	$pattern .= "\s*".preg_quote($this->qualifier[1]);
    	return $pattern;
    }
    public function setEsc($data="@"){
    	if(!$data)return $this;
    	$this->esc = $data;
    }
    public function getEsc(){
    	return $this->esc;
    }
    public function setQualifier($data){
    	if(!$data || !is_array($data))return $this;
    	if( count($data) < 2)return $this;
    	$this->qualifier = $data;
    }
    public function getQualifier(){
    	return $this->qualifier;
    }
    public function setLQualifier($data){
    	if(!$data)return $this;
    	$this->qualifier[0] = $data;
    }
    public function getLQualifier(){
    	return $this->qualifier[0];
    }
    public function setRQualifier($data){
    	if(!$data)return $this;
    	$this->qualifier[1] = $data;
    }
    public function getRQualifier(){
    	return $this->qualifier[1];
    }
    public function setData($data){
    	// if(!$data)return $this;
    	$this->data = new Data($data);
    }
    public function getData(){
    	return $this->data;
    }
}