<?php
namespace Etsoftware\Template;

class Label
{
    public function __construct(){
        
    }
    public function fill($tpl, $funk=null){
        $tpl = $this->or($tpl, $funk);
        $tpl = $this->rnd09($tpl, $funk);
        $tpl = $this->rndlAZ($tpl, $funk);
        $tpl = $this->rndUAZ($tpl, $funk);
        $tpl = $this->rndHZ($tpl, $funk);
        return $tpl;
    }
    private function getLabel($tpl, $pattern, $cb){
        if(preg_match_all("/$pattern/m", $tpl, $mc, PREG_SET_ORDER)){
            foreach ($mc as $m) {$tpl = $cb($tpl, $m); }
        }
        return $tpl;
    }
    /**
     * 随机汉字 {0xb00xd0}
     * @param  [type] $tpl  [description]
     * @param  [type] $funk [description]
     * @return [type]       [description]
     */
    private function rndHZ($tpl, $funk=null){
        $pattern = $funk("0xb00xd0");
        return $this->getLabel($tpl, $pattern, function($tpl, $m){
            mt_srand();
            // 使用chr()函数拼接双字节汉字，前一个chr()为高位字节，后一个为低位字节
            $a = chr(mt_rand(0xB0,0xD0)).chr(mt_rand(0xA1, 0xF0));
            // 转码
            return preg_replace("/".preg_quote($m[0])."/m", iconv('GB2312', 'UTF-8', $a), $tpl);
        });
    }
    /**
     * {A-Z}
     * @param  [type] $tpl  [description]
     * @param  [type] $funk [description]
     * @return [type]       [description]
     */
    private function rndUAZ($tpl, $funk=null){
        $pattern = $funk("([A-Z])\s*\-\s*([A-Z])");
        return $this->getLabel($tpl, $pattern, function($tpl, $m){
            $min = ord($m[1]); $max = ord($m[2]);
            mt_srand();
            $ascii = mt_rand($min, $max);
            return preg_replace("/".preg_quote($m[0])."/m", chr($ascii), $tpl);
        });
    }
    /**
     * {a-z}
     * @param  [type] $tpl  [description]
     * @param  [type] $funk [description]
     * @return [type]       [description]
     */
    private function rndlAZ($tpl, $funk=null){
        $pattern = $funk("([a-z])\s*\-\s*([a-z])");
        return $this->getLabel($tpl, $pattern, function($tpl, $m){
            $min = ord($m[1]); $max = ord($m[2]);
            mt_srand();
            $ascii = mt_rand($min, $max);
            return preg_replace("/".preg_quote($m[0])."/m", chr($ascii), $tpl);
        });
    }
    /**
     * {0-9}
     * @param  [type] $tpl  [description]
     * @param  [type] $funk [description]
     * @return [type]       [description]
     */
    private function rnd09($tpl, $funk=null){
        $int = "(\d+)|(\d+\.\d+)";
        $pattern = $funk("($int)\s*\-\s*($int)");
        return $this->getLabel($tpl, $pattern, function($tpl, $m){
            $min = $m[1]*1; $max = $m[4]*1;
            mt_srand();
            $n = mt_rand($min, $max);
            return preg_replace("/".preg_quote($m[0])."/im", $n, $tpl);
        });
    }
    /**
     * {a|b|c|d}
     * @param  [type] $tpl  [description]
     * @param  [type] $funk [description]
     * @return [type]       [description]
     */
    private function or($tpl, $funk=null){
        $pattern = $funk("([^\|\{\}]+\|([^\|\{\}]+\|?)+)");
        return $this->getLabel($tpl, $pattern, function($tpl, $m){
            $dat=explode("|", $m[1]);
            mt_srand();
            $n = mt_rand(0, count($dat)-1);
            return preg_replace("/".preg_quote($m[0])."/im", $dat[$n], $tpl);
        });
    }

}