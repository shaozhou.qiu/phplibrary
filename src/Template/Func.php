<?php
namespace Etsoftware\Template;

class Func
{
    private $data = [];
    public function __construct($data){
        if($data){ $this->data = $data; }
    }
    public function render($tpl, $funk=null){
        return $this->builtInFunction($tpl, $funk);
    }
    private function builtInFunction($tpl, $funk=null){
        $pattern = "((\w+)\s*\((.*?)\))\s*;?";
        if($funk){
            $pattern = $funk( $pattern );
        }
        $pattern = "/$pattern/im";
        if( preg_match_all($pattern, $tpl, $mc, PREG_SET_ORDER) == 0)return $tpl;

        foreach ($mc as $k => $v) {
            // dump($pattern); die;
            // dump($v); die;
            $val = "";
            if(function_exists($v[2])){
                $val = $this->eval("return ".$v[1].";", $this->data);
            }
            $tpl = str_replace($v[0], $val, $tpl);
        }
        return $tpl;
        // dump($this->data); 
        // dump($mc); 
        die;

    }
    private function eval($phpcode, $data=null){
        $cmd = '';
        if($data){
            $param = "tpl_data_parameter_".mt_rand(1000, 9999);
            $$param = $this->data;
            $cmd = "if( isset(\$$param) ){\n";
            $cmd .= "   foreach(\$$param as \$k =>\$v){ \$\$k = \$v; }\n";
            $cmd .= "}\n";            
        }
        $cmd .= "try {";
        $cmd .= $phpcode;
        $cmd .= "} catch (\Exception \$e) {";
        $cmd .= "   return \$e->getMessage();";
        $cmd .= "}";

        return eval($cmd);
    }
}