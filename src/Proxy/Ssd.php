<?php
namespace Etsoftware\Proxy;
use Etsoftware\Proxy\Proxy;

class Ssd extends Proxy
{
    public function __construct(){
        $this->protocol = "ssd";
    }
    /**
     * parsing ss link to config
     * @param  [type] $server
     * @return [type]
     */
    public function encode($conf, $remarks=null){
        $msg = $this->protocol.'://'.$this->base64_encode(json_encode($conf, true));
        if($msg){
            $ss .='#'.urlencode($remarks);
        }
        return $ss;
    }
    /**
     * parsing ss link to config
     * @param  [type] $content file, ss, rss
     * @return [type]
     */
    public function decode($msg){
        $ret = preg_match("/^[\s\r\n]*(".$this->protocol."):\/\/([\w\=]+)[\s\r\n]*/im", $ss, $m);
        if(!$ret)return null;
        $txt = $this->base64_decode($m[2]);
        $data = json_decode($txt, true);
        return $data;
    }
}