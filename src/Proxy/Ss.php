<?php
namespace Etsoftware\Proxy;
use Etsoftware\Proxy\Proxy;

class Ss extends Proxy
{
    public function __construct(){
        $this->protocol = "ss";
    }
    /**
     * parsing ss link to config
     * @param  [type] $server
     * @return [type]
     */
    public function encode($server, $port=8080, $method=null, $password="", $remarks=null, $group='39doo'){
        if(is_array($server)){
            return $this->encode(
                $server["server"]??"",
                $server["port"]??0,
                $server["method"]??'',
                $server["password"]??'',
                $server["remarks"]??'39doo'
            );
        }
        $ss = "";
        if($method) $ss = "$method:";
        $ss .= urlencode($password)."@$server:$port";
        // $ss = $this->protocol.'://'.$ss;
        $ss = $this->protocol.'://'.$this->base64_encode($ss);
        $info = "";
        if($group){$info = "[$group] "; }
        if($remarks){$info .= "$remarks"; }else{ $info .= "$server"; }
        if($info){
            $ss .='#'.urlencode($info);
        }
        // echo($ss); die;
        return $ss;
    }
    /**
     * parsing ss link to config
     * @param  [type] $content file, ss, rss
     * @return [type]
     */
    public function decode($ss){
        $ret = preg_match("/^[\s\r\n]*(".$this->protocol."):\/\/((.*):)?(\w+)@([^:]+):(\d+)(#(.*))?/im", $ss, $m);
        if($ret){
            return [
                'method' => $m[3],
                'password' => $m[4],
                'server' => $m[5],
                'port' => $m[6],
                'remarks' => urldecode($m[8]??'')
            ];
        }
        $ret = preg_match("/^[\s\r\n]*(".$this->protocol."):\/\/([\w\=]+)(#(.*))?[\s\r\n]*/im", $ss, $m);
        if(!$ret)return null;
        $remarks = $m[4]??"";
        $txt = $this->base64_decode($m[2]);
        $re = "/^([^:]+):(.*)\@([^:]+):(.+)/im";
        $ret = preg_match($re, $txt, $m);
        if( !$ret ) return null;
        $data = [
            'method' => $m[1],
            'password' => urldecode($m[2]),
            'server' => $m[3],
            'port' => $m[4],
            'remarks' => urldecode($remarks)
        ];
        return $data;
    }
}