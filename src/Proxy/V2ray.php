<?php
namespace Etsoftware\Proxy;
use Etsoftware\Proxy\Vmess;

class V2ray
{
    private $conf = null;
    public $ps = null;
    public function __construct(){
    }
    /**
     * 检查是否为v2ray配置文件
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    static public function chkconf($data){
        if(is_string($data)){
            $data = json_decode($data, true);
        }
        if(!is_array($data)){
            return false;
        }
        if(!isset($data['inbounds']) && isset($data['outbounds'])){
            return false;
        }
        return $data;
    }
    /**
     * 加载配置文件
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function loadConf($data){
        $conf = self::chkconf($data);
        if(!$conf){
            return false;
        }
        $this->conf = $conf;
        return true;
    }
    /**
     * 转为vmess
     * @return [type] [description]
     */
    public function toVmess(){
        if($this->conf == null){return null;}
        if(!isset($this->conf['outbounds'])){return null;}
        $outbounds = $this->conf['outbounds'];
        if(!is_array($outbounds) || count($outbounds)<1){ return null; }
        if( !isset($outbounds['protocol']) ){
            $val = '';
            foreach ($outbounds as $k => $v) {
                $val .= $this->outboundsToVmess($v)."\n";
            }
            return $val;
        }else{
            return $this->outboundsToVmess($outbounds);
        }
    }
    private function outboundsToVmess($data){
        if( !isset($data['protocol']) ){ return null; }
        $protocol = strtoupper($data['protocol']);
        $protocol = trim($protocol);
        if($data['protocol'] != "vmess"){ return null;}
        if(!isset($data['settings'])){ return null;}
        $settings = $data['settings'];
        if(!isset($settings['vnext'])){ return null;}
        $vnext = $settings['vnext'];
        if(!is_array($vnext) || count($vnext)<1){return null;}
        $v = $vnext[0];
        $users = $v['users'];
        $ps = $this->ps??'39doo.com';
        $server = $v['address'];
        $port = $v['port'];
        $tls = "";
        $type = "";
        $host = "";
        $net = "";
        $path = "";
        $v = "";
        $security = "auto";
        $val = '';
        $vmess = new Vmess;
        foreach ($users as $k => $usr) {
            $id = $usr['id'];
            $aid = $usr['alterId']??64;
            $val .= $vmess->encode($ps, $server, $port, $id, $aid, $tls, $type, $host, $net, $path, $v, $security)."\n";
        }
        // dump($val);
        return $val;
    }
}