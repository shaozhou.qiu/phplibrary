<?php
namespace Etsoftware\Proxy;
use Etsoftware\Proxy\Proxy;

class Trojan extends Proxy
{
    public function __construct(){
        $this->protocol = "trojan";
    }
    /**
     * parsing ssr link to config
     * @param  [type] $server
     * @return [type]
     */
    public function encode($server, $port=8080, $password="", $remarks="", $group="", $exdata=[]){
        if(is_array($server)){
            return $this->encode(
                $server["server"]??"",
                $server["port"]??0,
                $server["password"]??'',
                $server["remarks"]??'',
                $server["group"]??'39doo'
            );
        }
        // trojan://vwawb4SoNNosTJCy@www.zhanan15.online:443#%E9%A6%99%E6%B8%AF(%E6%AC%A2%E8%BF%8E%E8%AE%A2%E9%98%85Youtube%E7%A0%B4%E8%A7%A3%E8%B5%84%E6%BA%90%E5%90%9B)
        $group = urlencode($group);
        $remarks = urlencode($remarks);
        $ret = $this->protocol."://".urlencode($password)."@$server:$port";
        // if($exdata && count($exdata)>0){
        //     $ret .= "?";
        //     foreach ($exdata as $k => $v) {
        //         $ret .= "&$k=".urlencode($v);
        //     }
        //     $ret .= "&$remarks=".urlencode($remarks);
        //     $ret .= "&$group=".urlencode($group);
        // }else{
        //     $ret .= "#$remarks($group)";
        // }
        $ret .= "#$remarks($group)";
        return $ret;
    }
    /**
     * parsing Trojan link to config
     * @param  [type] $url
     * @return [type]
     */
    public function decode($url){
        $isMatch = false;
        $ret = preg_match("/^[\s\r\n]*(".$this->protocol."):\/\/([^@]+)@([^:]+):(\d+)(.*)$/im", $url, $m);
        $password = '';
        $server = '127.0.0.1';
        $port = 8080;
        $remarks = "";
        if($ret){
            $password = $m[2];
            $server = $m[3];
            $port = $m[4];
            $data = [];
            if(preg_match_all("/(\?|\&)(\w+)\s*=\s*([^\&]*)/", $m[5], $mc, PREG_SET_ORDER)){
                foreach ($mc as $m) {
                    $data[$m[2]]=$m[3];
                }
            }else{
                $remarks = urldecode($m[5]);
            }
        }else{
            return null;
        }
        return array_merge(array(
            "server" => $server
            , "port" => $port
            , "password" => urldecode($password)
            , "remarks" => $remarks
        ), $data);
    }
}