<?php
namespace Etsoftware\Proxy;
use Etsoftware\Proxy\Proxy;

class Ssr extends Proxy
{
    public function __construct(){
        $this->protocol = "ssr";
    }
    /**
     * parsing ssr link to config
     * @param  [type] $server
     * @return [type]
     */
    public function encode($server, $port=9050, $protocol="origin", $method="none", $garble="plain", $password="", $obfsparam="", $protoparam="", $remarks="", $group="39doo"){
        if(is_array($server)){
            return $this->encode(
                $server["server"]??"",
                $server["port"]??0,
                $server["protocol"]??'',
                $server["method"]??'',
                $server["garble"]??'',
                $server["password"]??'',
                $server["obfsparam"]??'',
                $server["protoparam"]??'',
                $server["remarks"]??'',
                $server["group"]??'39doo'
            );
        }
        $port = $port?$port:9050;
        $protocol = $protocol?$protocol:'origin';
        $garble = $garble?$garble:'plain';
        $method = $method?$method:'aes-256-cfb';
        $remarks = $remarks?$remarks:'';
        $group = $group?$group:'EtSoftware';
        $ssr = "$server:$port:$protocol:$method:$garble:".$this->base64_encode(urlencode($password))
            ."/?obfsparam=".$this->base64_encode($obfsparam)
            ."&protoparam=".$this->base64_encode($protoparam)
            ."&remarks=".$this->base64_encode($remarks)
            ."&group=".$this->base64_encode($group);
// dump($ssr); die;
        return $this->protocol.'://'.$this->base64_encode($ssr);
    }
    /**
     * parsing ssr link to config
     * @param  [type] $content file, ssr, rss
     * @return [type]
     */
    public function decode($ssr){
        $ret = preg_match("/^[\s\r\n]*(".$this->protocol."):\/\/([^:]+):([^:]+):([^:]+):([^:]+):([^:]+):([^:]+)(\/\?(.*))/im", $ssr, $m);
        if($ret){
            $data = [
                'server' => $m[2],
                'port' => $m[3],
                'protocol' => $m[4],
                'method' => $m[5],
                'garble' => $m[6],
                'password' => $m[7],
                'group' => '',
                'remarks' => ''
            ];
            if(isset($m[9])){
                if(preg_match_all("/(\w+)=([^&]*)/im", $m[9], $mc, PREG_SET_ORDER)){
                    foreach ($mc as $m) $data[$m[1]] = base64_decode($m[2]);
                }
            }
            return $data;
        }
        $ret = preg_match("/^[\s\r\n]*(".$this->protocol."):\/\/([\w\=]+)[\s\r\n]*/im", $ssr, $m);
        if(!$ret)return null;
        $txt = $this->base64_decode($m[2]);
        $re = "/"
        ."([^:]+)"
        .":(\d+)"
        .":([^:]+)"
        .":([^:]+)"
        .":([^:]+)"
        .":([^:\/\?]+)"
        ."("
            ."\/\?(.*)"
            // ."\/\?obfsparam=([^&]*)"
            // ."&remarks=([^&]*)"
            // ."&group=([^&]*)"
        .")?"
        ."/im";
        $ret = preg_match($re, $txt, $m);
        if( !$ret ) return null;
        $data = [
            'server' => $m[1],
            'port' => $m[2],
            'protocol' => $m[3],
            'method' => $m[4],
            'garble' => $m[5],
            'password' => $m[6],
            'group' => '',
            'remarks' => '',
        ];
        $data['password'] = urldecode($this->base64_decode($data['password']));
        if(isset($m[7])){
            if( preg_match_all("/(\w+)=([^&]*)/im", $m[8], $mc, PREG_SET_ORDER) ){
                foreach ($mc as $k => $v) {
                    $data[$v[1]] = $this->base64_decode($v[2]);
                }
            }
        }
        return $data;
    }
}