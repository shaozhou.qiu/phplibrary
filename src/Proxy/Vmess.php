<?php
namespace Etsoftware\Proxy;
use Etsoftware\Proxy\Proxy;

class Vmess extends Proxy
{
    public function __construct(){
        $this->protocol = "vmess";
    }
    /**
     * parsing vmess link to config
     * @param  [type] $ps       server title or remark
     * @param  [type] $server   address with ipv4
     * @param  [type] $port     open port
     * @param  [type] $id       id
     * @param  [type] $aid       aid
     * @param  [type] $tls       tls
     * @param  [type] $type       type
     * @return [type]
     */
    public function encode($ps, $server="127.0.0.1", $port=8080, $id="", $aid="", $tls="none", $type="none", $host="", $net="", $path="", $v="", $security='auto'){
        if(is_array($ps)){
            return $this->encode(
                $ps["ps"]??"",
                $ps["add"]??"",
                $ps["port"]??0,
                $ps["id"]??'',
                $ps["aid"]??'',
                $ps["tls"]??'',
                $ps["type"]??'',
                $ps["host"]??'',
                $ps["net"]??'',
                $ps["path"]??'',
                $ps["v"]??'',
                $ps["security"]??'auto'
            );
        }
        if ( preg_match("/websocket/im", $net) ) {
            return $this->encodeUrl($ps, $server, $port, $id, $tls, $host, $net, $path, $security);
        }
        return $this->encodeJson($ps, $server, $port, $id, $aid, $tls, $type, $host, $net, $path, $v, $security);
    }
    public function encodeJson($ps, $server, $port, $id, $aid, $tls="none", $type="none", $host="", $net="", $path="", $v="", $security='auto'){
        // $ps = urlencode($ps);
        // $ps = $this->base64_encode($ps);
        $data = [
            "v" => $v,
            "ps" => $ps,
            "add" => $server,
            "port" => $port,
            "id" => $id,
            "aid" => $aid,
            "net" => $net,
            "type" => $type,
            "host" => $host,
            "path" => $path,
            "tls" => $tls
        ];
        $data = json_encode($data, true);
        // dump($data); die;
        $vmess = $this->base64_encode($data);
        return $this->protocol."://".$vmess;
    }
    public function encodeUrl($ps, $server, $port, $id, $tls="none", $obfsParam="", $obfs="websocket", $path="", $security='auto'){
        // $ps = urlencode($ps);
        $obfsParam = $obfsParam?$obfsParam:$server;
        $vmess = $this->base64_encode( "$security:$id@$server:$port" );
        $vmess .= "?remarks=$ps&obfsParam=$obfsParam&path=$path&obfs=$obfs&tls=";
        if($tls=='none' || $tls=='1' || $tls=='tls' || $tls ===true){
            $vmess .= 1;
        }else{
            $vmess .= 0;
        }
        return $this->protocol."://".$vmess;
    }
    /**
     * parsing vmess link to config
     * @param  [type] $content file, vmess, rss
     * @return [type]
     */
    public function decode($vmess){
        $ret = preg_match("/^[\s\r\n]*(".$this->protocol."):\/\/([\w\+\/\=]+)[\s\r\n]*(.*)/im", $vmess, $m);
        if(!$ret)return null;
        $data = [];
        $m2 = $this->base64_decode($m[2]);
        $m2 = preg_replace("/[\r\n]/im", "", $m2);
        if(preg_match("/[\s\n\r]*\{[\w\W]*\}[\s\n\r]*/im", $m2)){
            if( preg_match("/.*?(\{.*\}).*/", $m2, $conf) ){
                $data = array_merge($data, json_decode($conf[1], true));
            }
        }else if(preg_match("/^([^:]*):(.*)@([^:]*):(\d+)$/im", $m2, $m2m)){
            $data = array_merge($data, [
                'method' => $m2m[1],
                'id' => $m2m[2],
                'add' => $m2m[3],
                'port' => $m2m[4],
            ]);
        }
        if($m[3] !=''){
            if(preg_match_all("/(\w+)=([^&]*)/", $m[3], $mc, PREG_SET_ORDER)){
                foreach ($mc as $k => $v) {
                    $data[$v[1]] = urldecode($v[2]);
                }
            }
        }
        return $this->configMap($data);
    }
    private function configMap($data){
        return $this->dataMap($data, [
            "ps" => "ps",
            "remark" => "ps",
            "address" => "add",
            "add" => "add",
            "port" => "port",
            "id" => "id",
            "uuid" => "id",
            "aid" => "aid",
            "alterId" => "aid",
            "security" => "security", // chacha20-poly1305, aes-128-gcm, auto, none
            "net" => "net", // tcp, kcp, ws, h2, quic
            "network" => "net", // tcp, kcp, ws, h2, quic
            "type" => "type", // none, http, srtp, utp, wechat-video, dtls, wireguard
            "h2host" => "host",
            "wshost" => "host",
            "host" => "host",
            "h2path" => "path",
            "wspath" => "path",
            "path" => "path",
            "v" => "v",
            "tls" => "tls",
            "skip-cert-verify" => "skip-cert-verify"
        ], function($data, $src, $exp){
            // $data['tls'] = ($data['tls']=="tls" || $data['tls']=="1")?true:false;
            if($data['security']=="") $data['security'] = 'auto';
            if($data['skip-cert-verify']=="") $data['skip-cert-verify'] = true;
            // return $data;
            return array_merge($data, $exp);
        });
    }
}