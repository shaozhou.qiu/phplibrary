<?php
namespace Etsoftware\Proxy;
use Etsoftware\Proxy\Proxy;
use Etsoftware\Proxy\Mtproto;
use Etsoftware\Proxy\Socks5;
// Telegram proxy
// class Tg extends Proxy
class Tg
{
    public function __construct(){
        $this->protocol = "MTProto";
    }
    private function getParamByUrl($url){
        $reVal = [];
        if(preg_match_all("/([\w\-]+)\=([^&]+)/im", $url, $mc, PREG_SET_ORDER)){
            foreach ($mc as $k => $v) {
                $reVal[$v[1]] = $v[2];
            }
        }
        return $reVal;
    }
    /**
     * 将url 转换成为 tg:// 格式
     * @param  [type] $content [description]
     * @return [type]          [description]
     */
    static public function toMTPOSCK($content){
        $content = preg_replace("/\&amp\;/im", "&", $content);
        if(preg_match_all("/((https:\\/\\/t\.me\/proxy)|(tg:\\/\\/proxy))([\#\@\:\w\.\/\?\=\&\-\%]+)/im", $content, $mc, PREG_SET_ORDER)){
            foreach ($mc as $k => $v) {
                $tg = $v[0];
                $data = (new self)->getParamByUrl($tg);
                $server = $data['server']??'';
                $port = $data['port']??0;
                $secret = $data['secret']??'';
                $mtp = (new Mtproto)->encode($server, $port, $secret, null, null);
                $content = str_replace($tg, $mtp, $content);
            }
        }
        if(preg_match_all("/https:\/\/t\.me\/socks\?(.*)/im", $content, $mc, PREG_SET_ORDER)){
            foreach ($mc as $k => $v) {
                $tg = $v[0];
                $data = (new self)->getParamByUrl($tg);
                $server = $data['server']??'';
                $port = $data['port']??0;
                $user = $data['user']??'';
                $pass = $data['pass']??'';
                $mtp = (new Socks5)->encode($server, $port, $user, $pass, null, null);
                $content = str_replace($tg, $mtp, $content);
            }
        }
        return $content;
    }
    static public function formMTPOSCK($content){
        $mtproto = new Mtproto();
        $mtproto->load($content);
        $data = $mtproto->getData();
        $reVal = "";
        foreach ($data as $k => $v) {
            if(!$v)continue;
            $reVal .= "\nhttps://t.me/proxy?server=".$v['server']."&port=".$v['port']."&secret=".urlencode($v['secret']);
        }
        $socks5 = new Socks5();
        $socks5->load($content);
        $data = $socks5->getData();
        foreach ($data as $k => $v) {
            if(!$v)continue;
            $reVal .= "\nhttps://t.me/socks?server=".$v['server']."&port=".$v['port']."&user=".urlencode($v['username'])."&pass=".urlencode($v['password']);
        }
        return $reVal;
    }
}