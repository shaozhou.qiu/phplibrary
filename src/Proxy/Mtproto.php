<?php
namespace Etsoftware\Proxy;

use Etsoftware\Proxy\Proxy;

class Mtproto extends Proxy
{
    public function __construct(){
        $this->protocol = "MTProto";
    }
    /**
     * parsing ssr link to config
     * @param  [type] $server  
     * @return [type]           
     */
    public function encode($server, $port, $secret,  $remarks, $group){
        return $this->protocol."://".$this->base64_encode(urlencode($secret)."@$server:$port")."#".urlencode("$remarks($group)");
    }
    /**
     * parsing Trojan link to config
     * @param  [type] $url
     * @return [type]           
     */
    public function decode($url){
        $ret = preg_match("/^[\s\r\n]*".$this->protocol.":\/\/([\w\=]+)(#([^\r\n]+))?[\s\r\n]*/im", $url, $m);
        if(!$ret)return null;
        $txt = $this->base64_decode($m[1]);
        $remarks = isset($m[3])?urldecode($m[3]):'';
        $re = "/^(([^:]+):)?([^\@]+)\@([^:]+):(.+)/im";
        $ret = preg_match($re, $txt, $m);
        if( !$ret ) return null;
        $data = [
            'secret' => urldecode($m[3]),
            'server' => $m[4],
            'port' => $m[5],
            'remarks' => $remarks
        ];
        return $data;
    }

}
