<?php
namespace Etsoftware\Proxy;
use Etsoftware\Lib\File;

class Proxy
{
    protected $data = [];
    protected $protocol = 'etmsg';
    private $content = '';
    public function __construct(){
    }
    /**
     * Load ssr link , rss or file to instance
     * @param  [type] $content file, ssr, rss
     * @return [type]           
     */
    public function load($content, $start=0, $end=0){
        $content = preg_replace("/^[\s\b\n\r]*(.*?)[\s\n\r\b]*$/im", "$1", $content);
        $data = $this->getNode($content, $start, $end);
        $this->data = array_merge($this->data, $data);
        return $this;
    }
    /**
     * get node config form ssr link, rss or file
     * @param  [type] $content file, ssr, rss
     * @return [type]           
     */        
    public function getData(){
        return $this->data;
    }


    /**
     * get node message list
     * @param  [type] $content ssr link
     * @return Array           
     */
    public function getMessages($content, $callback=null, $start=0, $end=0){
        $data = null;
        if( preg_match("/(^|^\w)".$this->protocol.":\/\/.*/im", $content) ){
            $content = preg_replace("/".$this->protocol.":/im", "\n$0", $content);
            // if(preg_match_all("/".$this->protocol.":\/\/[^\n\r]*/im", $content, $mc, PREG_SET_ORDER)){
            if(preg_match_all("/".$this->protocol.":\/\/[\@\:\w\.\/\?\=\&\-\%\+]+(#(.*))?/im", $content, $mc, PREG_SET_ORDER)){
                $data = [];
                foreach ($mc as $k => $v) {
                    if($end >0 && $start < $end){
                        if($k<$start || $k>$end)continue;
                    }
                    // if(!$v)continue;
                    array_push($data, $v[0]);
                }
            }
        }else if(preg_match("/^[\w\/]+[=]*$/im", $content) ){ 
            $data = $this->rss2conf( $content );
            $data = $this->getMessages($data, null, $start,$end);
        }else if(preg_match("/^https?:\/\//im", $content) ){
            $data = $this->url2conf( $content );
            $data = $this->getMessages($data, null, $start,$end);
        }else if(preg_match("/^((\\".DIRECTORY_SEPARATOR."\w+)|(\w+:\\\\))/im", $content) ){
            $data = $this->file2conf( $content );
            $data = $this->getMessages($data?$data:'', null, $start,$end);
        }else{
            // dump( $this->base64_decode($content) );
        }

        if($callback) $data = $callback->call($this, $data);
        if($data==null)$data = [];
        return is_array($data)?$data:[$data];
    }
    public function getContent(){
        return $this->content;
    }
    /**
     * get node config form ssr link, rss or file
     * @param  [type] $content file, ssr, rss
     * @return [type]           
     */    
    protected function getNode($content, $start, $end){
        return $this->getMessages($content, function($data){
            $msg = "";
            if(is_array($data)){
                foreach ($data as $k => $v) {$msg .= "\n$v"; }
                $msg = substr($msg, 1);
            }else{
                $msg = $data;
            }
            $conf = $this->msg2conf( $msg );
            return $conf;
        }, $start, $end);
    }
    private function cache($f, $callback){
        $fn = hash("sha1", $f).".pxy";
        $fn = File::buildTreeDirectory("downloadfiles\{date:Y/m/d}") . $fn;
        $content = null;
        if(file_exists($fn)){
            $content = $this->file2conf($fn);
        }else{
            $content = is_callable($callback)?call_user_func_array($callback, [$f]):null;
            if($content){
                $f = new File($fn);
                $f->write($content);
            }
        }
        return $content;
    }
    /**
     * get node config form file
     * @param  [type] $content ssr link
     * @return [type]           
     */
    protected function file2conf($filename){
        // return $this->cache($filename, function($filename){
            if(!file_exists($filename)) return null;
            $f = fopen($filename, "rb");
            if($f){
                $data = fread($f, filesize($filename));
                fclose($f);
            }
            return $data;
        // }
    }
    /**
     * get node config form url
     * @param  [type] $content ssr link
     * @return [type]           
     */
    protected function url2conf($url){
        return $this->cache($url, function($url){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);  
            curl_setopt($ch, CURLOPT_HEADER, 0);
            $data = curl_exec($ch);
            curl_close($ch);
            return $data;

        });
    }
    /**
     * get node config form rss
     * @param  [type] $content ssr link
     * @return [type]           
     */
    protected function rss2conf($rss){
        return  $this->base64_decode($rss);
    }
    /**
     * get node config form ssr link
     * @param  [type] $content ssr link
     * @return [type]           
     */
    protected function msg2conf($content){
        $re = preg_match_all("/".$this->protocol.":\/\/[^\s\r\n]+/im", $content, $mc, PREG_SET_ORDER);
        if( !$re )return [];
        $data = [];
        foreach ($mc as $k => $v) {
            $ret = static::decode( $v[0] );
            array_push($data, $ret);
        }
        return $data;
    }

    protected function base64_encode($data){
        // $data = urlencode( $data );
        $data = base64_encode($data);
        $data = preg_replace("/\//im", "_", $data);
        $data = preg_replace("/\+/im", "-", $data);
        return $data;
    }
    protected function base64_decode($data){
        // $data = urldecode($data);
        $data = str_replace("\0", "", $data);
        $data = preg_replace("/_/im", "/", $data);
        $data = preg_replace("/[ \-]/im", "+", $data);
        $data = base64_decode($data);
        // $data = decodeURIComponent(escape(atob( $data )));
        return $data;
    }
    /**
     * data map
     * @param  [type] $data source
     * @return [type] $map  map ruler
     */
    protected function dataMap($data, $map, $callback){
        $reVal = [];
        $exp = $data;
        foreach ($map as $k => $v) {
            if(!isset($reVal[$v])) $reVal[$v] = '';
            if(!isset( $exp[$k] )) continue; 
            $reVal[$v] .= $exp[$k];
            unset($exp[$k]);
        }
        if($callback){
            $reVal = $callback->call($this, $reVal, $data, $exp);
        }
        return $reVal;
    }    
}
