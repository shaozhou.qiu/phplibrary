<?php
namespace Etsoftware\Email;

use Etsoftware\Lib\File;

class Imap extends Mail
{
	/**
	 * initail 
	 * @param [type]  $host       host address
	 * @param [type]  $port       host port
	 * @param [type]  $user       user name
	 * @param [type]  $password   user passwrord
	 * @param string  $email      user email
	 * @param string  $servertype server type , pop 
	 * @param boolean $ssl        whenther ssl
	 */
	function __construct($host, $port, $user, $password, $email='', $servertype='pop', $ssl=false)
    {
        parent::__construct($host, $port, $user, $password, false);
        $this->email = $email;
        $this->servertype = $servertype;
        $this->ssl = $ssl;
    }
    
    
    /**
     * get maill count
     * @return [type] return numeric if successful, otherwise returns null
     */
    public function count(){
        // 读取邮件ID
        $ret = $this->getList();
        // dump($ret);
        $data = null;
        if($ret['errcode'] == 0){
            $ret['data'] = count($ret['data']);
        }
        return $ret;
    }
    /**
     * get mailbox list
     * @param  [type] $num  top n for list, default 10, 0 is all list
     * @return [type] [description]
     */
    public function getList($num=0){
        return $this->login(function($ret)use($num){
            ini_set("max_execution_time", "600");// 下载时间限制为10分钟
            $ret = $this->sendMessage("LIST\r\n");
            if($ret['errcode'] != 0){return $ret; }
            $data = array(); $content =$ret['message']; $count =0;
            // 读取邮件ID
            while (true) {
                $msg = fgets($this->_handle);
                $this->log($msg);
                if (preg_match('/(^\.)|(^\s*-ERR\s)/im', $msg)) { break; }
                if($num!=0 && $count >= $num)continue;
                $content .= $msg;
                if (! preg_match('/^\+OK/', $msg) && !preg_match('/^\./', $msg)) {
                    preg_match("/\s*(\d+)\s*(\d+)\s*/im", $msg, $m);
                    if($m){
                        array_push($data, ['id' => $m[1] , 'size' => $m[2]*1]);
                    }
                }
                $count++;
            }
            foreach ($data as $k => $v) {
                $ret = $this->getUIDL($v['id']);
                if($ret['errcode'] == 0){
                    $data[$k]['UIDL'] = $ret['data']['UIDL'];
                }
            }
            ini_restore("max_execution_time");// 还原下载时间限制
            return ['errcode'=>0, 'data'=>$data, 'message'=>$content];
        });
    }   
 
    public function readMail($id){
        ini_set("max_execution_time", "600");// 下载时间限制为10分钟 
        $ret = $this->getMime($id);
        if($ret['errcode'] != 0){return $ret;}
        $mime = $ret['data'];
		$header = $ret['message'];
        $body = '';        
        $html = '';        
        $eml = '';
        $ret = $this->getEML($id, $mime);
        if($ret['errcode'] == 0){
            $eml = $ret['data'];
        }
        ini_restore("max_execution_time");// 还原下载时间限制
        return ['errcode'=>0, 'data'=>[
            'MIME' => $mime
            ,'header' => $header
            ,'body' => $body
            ,'html' => $html
            ,'eml' => $eml
        ]];
    }
    private function login($fun){
        $ret = $this->connect();
        if($ret['errcode'] != 0){
            return $ret;
        }
        $ret = $this->auth();
        if($ret['errcode'] != 0){
            return $ret;
        }
        $ret = $fun($ret);
        $this->sendMessage("QUIT\r\n");
        $this->close();
        return $ret;
    }
    /**
     * Auth user and password
     * @return [type] [description]
     */
    private function auth(){
        $data = [
            [0, "USER $this->user"]
            ,[0, "PASS $this->password"]
            ,[0, "stat"]
        ];
        foreach ($data as $v) {
            $ret = $this->sendMessage($v[1]."\r\n");
            if($ret['errcode'] != $v[0] ){
                // dump(__FILE__.":".__LINE__);
                // dump($ret); die();
                return ['errcode'=>$ret['errcode'], 'data'=>'Login failed'];
            }
        }
        return ['errcode'=>0, 'data'=>$this->_handle];
    }

    /**
     * 获取邮件正文
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    private function getEML($id, $mime){
        $filename = "downloadfiles#mailbox";
        // $filename .= "#".preg_replace("/.*?([\w\.]+)@([\w\.]+).*/im", "$2#$1", $mime->to);
        if( preg_match("/.*?([\w\.]+)@([\w\.]+).*/im", $mime->to, $m) )
        {
            $filename .= "#".$m[2]. preg_replace("/(\w)/im", "#$1", $m[1]);
        }
        if( preg_match("/.*?([\w\.]+)@([\w\.]+).*/im", $mime->from, $m) )
        {
            $filename .= "#".$m[2]. preg_replace("/(\w)/im", "#$1", $m[1]);
        }
        // if(preg_match_all("/\w/im", $mime->from, $mc, PREG_SET_ORDER))
        // {
        //     foreach ($mc as $v) {$filename .= "#".$v[0]; }
        // }
        $filename = preg_replace("/#/im", DIRECTORY_SEPARATOR, $filename);
        $fn = preg_replace("/\W+/im", "", $mime->messageId);
        $filename .= DIRECTORY_SEPARATOR.$fn.".eml";
        $filename = strtolower($filename);
        return $this->login(function($ret)use($id, $filename){
            $file = new File($filename);
            if( $file->exists() ){
                return ['errcode'=>0, 'data'=>$file->getFileName()];
            }
            $msg = "RETR $id 1\r\n";
            fwrite($this->_handle, $msg);
            $this->log($msg);
            while (true) {
                $msg = fgets($this->_handle);
                $this->log($msg);
                if (preg_match('/(^\.)|(^\s*-ERR\s)/im', $msg)) { break; }
                $file->append($msg);
            }
            return ['errcode'=>0, 'data'=>$file->getFileName()];
        });
    }
    /**
     * 取出某个信件（不能是标记为删除的信件）的信头和指定的行数
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    private function getMime($id){
        return $this->login(function($ret)use($id){
            // top n,m 处理 返回n号邮件的前m行内容，m必须是自然数,0表示查看整个邮件头
            $msg = "TOP $id 0\r\n";
            fwrite($this->_handle, $msg);
            $this->log($msg);
            $mime = new Mime;
            $content = "";
            while (true) {
                $msg = fgets($this->_handle);
                $this->log($msg);
                if (preg_match('/(^\.)|(^\s*-ERR\s)/im', $msg)) { break; }
                $content .= $msg;
            }
            // dump($content);
            // dump([strlen($content), $content]);
            $mime->load($content);
            $ret = $this->getUIDL($id);
            if($ret['errcode'] == 0){
                $mime->uidl = $ret['data']['UIDL'];
            }
            return ['errcode'=>0, 'data'=>$mime, 'message'=>$content];
        });
    }
    /**
     * [getUIDL description]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    private function getUIDL($id){
        $ret = $this->sendMessage("uidl $id\r\n");
        if($ret['errcode'] == 0){
            // +OK 1 xS2BjgclvVc65z02owAAsz
            if( preg_match("/^\s*(\+OK)\s*(\d+)\s*(.*)/im", $ret['message'], $m) ){
                return ['errcode'=>0, 'data'=>['id'=>$m[2] ,'UIDL'=>$m[3] ], 'message'=>$ret['message']];
            }
        }
        return $ret;
    }    



}