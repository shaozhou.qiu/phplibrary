<?php
namespace Etsoftware\Email;

use Etsoftware\Lib\File;

class Mail
{
    private $debug = true; // sock handle wite log to the root path
    protected $_handle = null; // sock handle
    protected $config = [
        "host" => '127.0.0.1' // host ip address
        , "port" => 25 // host port
        , "user" => 'rimke' // user account
        , "password" => '****' // password with account
        , "auth" => false // whether auth 
        , "timeout" => 10 // time out
        , "email" => '' // email address
        , "servertype" => '' // email server type
        , "ssl" => false // email server type
    ];
    private $exitCodes = [
        0 => 'OK',
        211 => "系统状态或系统帮助响应", //
        214 => "帮助信息", //
        220 => " 服务就绪", //
        221 => " 服务关闭传输信道", //
        235 => "用户验证成功", //
        250 => "要求的邮件操作完成", //
        251 => "用户非本地，将转发向", //
        334 => "等待用户输入验证信息", //   
        354 => "开始邮件输入，以.结束", //
        421 =>" 服务未就绪，关闭传输信道（当必须关闭时，此应答可以作为对任何命令的响应）\r\n".
                "HL:REP 该IP发送行为异常，存在接收者大量不存在情况，被临时禁止连接。请检查是否有用户发送病毒或者垃圾邮件，并核对发送列表有效性；\r\n".
                "HL:ICC 该IP同时并发连接数过大，超过了网易的限制，被临时禁止连接。请检查是否有用户发送病毒或者垃圾邮件，并降低IP并发连接数量；\r\n".
                "HL:IFC 该IP短期内发送了大量信件，超过了网易的限制，被临时禁止连接。请检查是否有用户发送病毒或者垃圾邮件，并降低发送频率；\r\n".
                "HL:MEP 该IP发送行为异常，存在大量伪造发送域域名行为，被临时禁止连接。请检查是否有用户发送病毒或者垃圾邮件，并使用真实有效的域名发送；", 
        450 => "要求的邮件操作未完成，邮箱不可用（例如，邮箱忙）\r\n".
                "MI:CEL 发送方出现过多的错误指令。请检查发信程序；\r\n".
                "MI:DMC 当前连接发送的邮件数量超出限制。请减少每次连接中投递的邮件数量；\r\n".
                "MI:CCL 发送方发送超出正常的指令数量。请检查发信程序；\r\n".
                "RP:DRC 当前连接发送的收件人数量超出限制。请控制每次连接投递的邮件数量；\r\n".
                "RP:CCL 发送方发送超出正常的指令数量。请检查发信程序；\r\n".
                "DT:RBL 发信IP位于一个或多个RBL里。请参考http://www.rbls.org/关于RBL的相关信息；\r\n".
                "WM:BLI 该IP不在网易允许的发送地址列表里；\r\n".
                "WM:BLU 此用户不在网易允许的发信用户列表里；", //
        451 => "放弃要求的操作；处理过程中出错\r\n".
                "DT:SPM ,please try again 邮件正文带有垃圾邮件特征或发送环境缺乏规范性，被临时拒收。请保持邮件队列，两分钟后重投邮件。需调整邮件内容或优化发送环境；\r\n".
                "Requested mail action not taken: too much fail authentication 登录失败次数过多，被临时禁止登录。请检查密码与帐号验证设置；\r\n".
                "RP:CEL 发送方出现过多的错误指令。请检查发信程序；\r\n".
                "MI:DMC 当前连接发送的邮件数量超出限制。请控制每次连接中投递的邮件数量；\r\n".
                "MI:SFQ 发信人在15分钟内的发信数量超过限制，请控制发信频率；\r\n".
                "RP:QRC 发信方短期内累计的收件人数量超过限制，该发件人被临时禁止发信。请降低该用户发信频率；\r\n".
                "Requested action aborted: local error in processing 系统暂时出现故障，请稍后再次尝试发送；", //
        452 => "系统存储不足，要求的操作未执行", //
        500 => "Error: bad syntaxU 发送的smtp命令语法有误；格式错误，命令不可识别（此错误也包括命令行过长）", //
        501 => "参数格式错误", //
        502 => "命令不可实现", //
        503 => "错误的命令序列", //
        504 => "命令参数不可实现", //
        535 => "用户验证失败", //
        550 => "要求的邮件操作未完成，邮箱不可用（例如，邮箱未找到，或不可访问）\r\n".
                "MI:NHD HELO命令不允许为空；\r\n".
                "MI:IMF 发信人电子邮件地址不合规范。请参考http://www.rfc-editor.org/关于电子邮件规范的定义；\r\n".
                "MI:SPF 发信IP未被发送域的SPF许可。请参考http://www.openspf.org/关于SPF规范的定义；\r\n".
                "MI:DMA 该邮件未被发信域的DMARC许可。请参考http://dmarc.org/关于DMARC规范的定义；\r\n".
                "MI:STC 发件人当天的连接数量超出了限定数量，当天不再接受该发件人的邮件。请控制连接次数；\r\n".
                "RP:FRL 网易邮箱不开放匿名转发（Open relay）；\r\n".
                "RP:RCL 群发收件人数量超过了限额，请减少每封邮件的收件人数量；\r\n".
                "RP:TRC 发件人当天内累计的收件人数量超过限制，当天不再接受该发件人的邮件。请降低该用户发信频率；\r\n".
                "DT:SPM 邮件正文带有很多垃圾邮件特征或发送环境缺乏规范性。需调整邮件内容或优化发送环境；\r\n".
                "Invalid User 请求的用户不存在；\r\n".
                "User in blacklist 该用户不被允许给网易用户发信；\r\n".
                "User suspended 请求的用户处于禁用或者冻结状态；\r\n".
                "Requested mail action not taken: too much recipient  群发数量超过了限额；", //
        551 => "用户非本地，请尝试", //
        552 => "过量的存储分配，要求的操作未执行\r\n".
                "Illegal Attachment 不允许发送该类型的附件，包括以.uu .pif .scr .mim .hqx .bhx .cmd .vbs .bat .com .vbe .vb .js .wsh等结尾的附件；\r\n".
                "Requested mail action aborted: exceeded mailsize limit 发送的信件大小超过了网易邮箱允许接收的最大限制；", //
        553 => "邮箱名不可用，要求的操作未执行（例如邮箱格式错误）\r\n".
                "Requested action not taken: NULL sender is not allowed 不允许发件人为空，请使用真实发件人发送；\r\n".
                "Requested action not taken: Local user only  SMTP类型的机器只允许发信人是本站用户；\r\n".
                "Requested action not taken: no smtp MX only  MX类型的机器不允许发信人是本站用户；\r\n".
                "authentication is required  SMTP需要身份验证，请检查客户端设置；", //
        554 => "操作失败\r\n".
                "DT:SPM 发送的邮件内容包含了未被许可的信息，或被系统识别为垃圾邮件。请检查是否有用户发送病毒或者垃圾邮件；\r\n".
                "DT:SUM 信封发件人和信头发件人不匹配；\r\n".
                "IP is rejected, smtp auth error limit exceed 该IP验证失败次数过多，被临时禁止连接。请检查验证信息设置；\r\n".
                "HL:IHU 发信IP因发送垃圾邮件或存在异常的连接行为，被暂时挂起。请检测发信IP在历史上的发信情况和发信程序是否存在异常；\r\n".
                "HL:IPB 该IP不在网易允许的发送地址列表里；\r\n".
                "MI:STC 发件人当天内累计邮件数量超过限制，当天不再接受该发件人的投信。请降低发信频率；\r\n".
                "MI:SPB 此用户不在网易允许的发信用户列表里；\r\n".
                "IP in blacklist 该IP不在网易允许的发送地址列表里。", //        
    ];
    /**
     * construct instance
     * @param [type]  $host     smtp ip address
     * @param [type]  $port     smtp port
     * @param [type]  $user     user's account
     * @param [type]  $password user's password for smtp
     * @param boolean $auth     whether auth with login
     */
    public function __construct($host, $port, $user, $password, $auth=true)
    {
        $this->config = array_merge($this->config, [
            "host" => $host , "port" => $port
            , "user" => $user, "password" => $password
            , "auth" => $auth
        ]);
    }
    /**
     * connect smtp server 
     * @param  [type] $conf [description]
     * @return [type]       [description]
     */
    protected function connect(){
        $this->log("connect to the [$this->host : $this->port] server :");
        $this->_handle = @fsockopen($this->host, $this->port, $errno, $errstr, $this->timeout);
        if(!$this->_handle) 
        { 
            $this->log("Unable to connect to [$this->host : $this->port] server.");
            return ['errcode'=>$errno, 'data'=>"Error: Connecting to mail server"]; 
        }
        $ret = $this->response($this->_handle);
        if($ret['errcode'] != 0){
            fclose($this->_handle);
            return $ret;
        }        
        return ['errcode'=>0, 'data'=>$this->_handle];
    }
    /**
     * send message to server
     * @param  [type] $msg [description]
     * @return [type]      [description]
     */
    protected function sendMessage($msg){
        if($this->_handle == null){
            return ['errcode'=>221, 'data'=>'-Err Service off transport channel'];
        }
        fwrite($this->_handle, $msg);
        $this->log($msg);
        return $this->response();
    }    
    protected function response(){
        return $this->str2msg($this->fgets($this->_handle)['data']);
    }    
    /**
     * string translate to message
     * @param  [type] $str [description]
     * @return [type]      [description]
     */
    protected function str2msg($str){
        if(!is_string($str)){
            return ['errcode'=>1003, 'data'=>'Err: variable type error,when calling str2msg function', 'message'=>''];
        }
        $ret = ['errcode'=>1002, 'data'=>$str, 'message'=>$str];
        preg_match("/^\s*(\d+)\s*(.*)/im", $str, $m);
        if(!$m){
            preg_match("/^\s*([\+]?ok)\s+(.*)/im", $str, $m);
            if($m){ $m[1] = 0; $m[2] = $m[0]; }
        }
        if($m){
            $ret = ['errcode'=>$m[1]*1, 'data'=>$m[2], 'message'=>$str];
        }
        if( isset($this->exitCodes[$ret['errcode']]) ){
            $ret['data'] .= "\r\n".$this->exitCodes[$ret['errcode']];
        }
        if($ret['errcode'] == 220){ $ret['errcode'] = 0; }
        return $ret;
    }
    /**
     * string translate to message
     * @param  [type] $str [description]
     * @return [type]      [description]
     */
    protected function fgets($hdl=null, $size=512){
        $hdl = $hdl?$hdl:$this->_handle;
        $ret = ['errcode'=>554, 'data'=>'operation failed'];
        if($hdl){
            $data = fgets($hdl, $size);
            $ret = ['errcode'=>0, 'data'=>$data ];
        }
        $this->log($ret['data']);
        // $this->log( "UTF-8---".iconv($gbk, 'UTF-8', $ret['data']) );
        return $ret;
    }
    /**
     * get maill address
     * @param  [type] $str [description]
     * @return [type]      return array if success, otherwise reutrn  errcode in errno.
     */
    protected function getMailAddress($str){
        $ret = [];
        if( is_array($str) ){
            foreach ($str as $v) {
                $email = $this->getMailAddress($v);
                $ret = array_merge($ret, $email);
            }
            return $ret;
        }
        preg_match_all("/\w[\w\-\.]+@\w[\w\-\.]+/im", $str, $mc, PREG_SET_ORDER);
        if(count($mc) > 0){
            foreach ($mc as $v) { array_push($ret, $v[0]); }
        }
        return $ret;
    }
    /**
     * get value with config 
     * @param  [type] $attr [description]
     * @return [type]       [description]
     */
    public function __get($attr){
        return $this->config[$attr]??null;
    }
    /**
     * set value with config
     * @param [type] $attr [description]
     * @param [type] $val  [description]
     */
    public function __set($attr, $val){
        isset($this->config[$attr]) && ($this->config[$attr] = $val);
    }
    /**
     * close socket connect
     * @return [type] [description]
     */
    public function close()
    {
        if($this->_handle){
            fclose($this->_handle);
            $this->_handle = null;
            $this->log("disconnect server.");
        }
    }
    public function __destory()
    {
        $this->close();
    }
    protected function log($str){
        if($this->debug){
            $str = preg_replace("/\r/im", "\\r", $str);
            $str = preg_replace("/\n/im", "\\n", $str);
            $file = new File();
            $file->append(date("Y-m-d H:i:s")."\t".$str."\r\n");
        }
    }

}