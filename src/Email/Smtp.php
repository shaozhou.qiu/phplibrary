<?php
namespace Etsoftware\Email;

//use Etsoftware\Support\Str;

class Smtp extends Mail
{
    /**
     * construct instance
     * @param [type]  $host     smtp ip address
     * @param [type]  $port     smtp port
     * @param [type]  $user     user's account
     * @param [type]  $password user's password for smtp
     * @param boolean $auth     whether auth with login
     */
    function __construct($host, $port, $user, $password, $auth=false)
    {
        parent::__construct($host, $port, $user, $password, $auth);
    }
    /**
     * send email
     * @param  [type]  $to      receve email address
     * @param  [type]  $from    form email address eg: rimke@163.com or Rimke<rimke@163.com>
     * @param  [type]  $subject email's title
     * @param  [type]  $body    content with email
     * @param  boolean $type    whether html or text
     * @param  array   $cc      抄送地址 邮件的创建者
     * @param  array   $bcc     暗送地址 邮件的创建者
     * @param  string  $headers [description]
     * @return [type]           [description]
     */
    public function sendMail($to, $from, $subject, $body, $type=false, $cc=[], $bcc=[], $headers="")
    {
        $toMail = $this->getMailAddress($to);
        if(!$from || count($toMail) == 0){
            return ['errcode'=>553, 'data'=>$this->exitCodes[553]];
        }
        if(count($toMail) > 1){
            $ret = ['errcode'=>0, 'data'=>[]];
            foreach ($toMail as $v) {
                $retv = $this->sendMail($v, $from, $subject, $body, $type, $cc, $bcc, $headers);
                array_push($ret['data'], $retv);
            }
            return $ret;
        }

        $mime = new Mime();
        $mime->to = $toMail[0];
        $mime->from = $from;
        $mime->subject = $subject;
        $mime->cc = $cc;
        $mime->bcc = $bcc;

        $ret = $this->connect($this->config);
        if($ret['errcode'] != 0) return $ret;
        $this->_handle = $ret['data'];
        // 网易邮箱必须 $to,$from都为发送者邮箱，否则会报 554 DT:SPM 错误
        $ret = $this->sendMail($from, $from, $mime->toString(), $body, $mime);
        $this->close();
        return $ret;
    }
    /**
     * send command to smtp server
     * @param  [type] $to     [description]
     * @param  [type] $from   [description]
     * @param  [type] $header [description]
     * @param  [type] $body   [description]
     * @param  [type] $mime   [description]
     * @return [type]         [description]
     */
    private function sendMail($to, $from, $header, $body, $mime){
        $toMail = $this->getMailAddress($to)[0];
        $fromMail = $this->getMailAddress($from)[0];
        $msgs = [ [250, "HELO $this->host"] ];
        if($this->auth){
            array_push($msgs, [334, "AUTH LOGIN " . $mime->encode($this->user)]);
            array_push($msgs, [235, $mime->encode($this->password)]);
        }
        array_push($msgs, [250, "MAIL FROM:<$fromMail>"]);
        array_push($msgs, [250, "RCPT TO:<$toMail>"]);
        array_push($msgs, [354, "DATA"]);

        foreach ($msgs as $v) {
            $ret = $this->sendMessage($v[1] . "\r\n");
            if($ret['errcode'] != $v[0]){
                return $ret;
            }
        }
        $ret = $this->sendMessage( $header . "\r\n" . $mime->encode($body) );
        $ret = $this->sendMessage("\r\n.\r\n");
        if($ret['errcode'] != 250 ){ return $ret; }
        $ret = $this->sendMessage("QUIT\r\n");
        return ['errcode'=>0, 'data'=>'ok'];
    }


}