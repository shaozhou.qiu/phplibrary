<?php
namespace Etsoftware\Email;

//use Etsoftware\Support\Str;

class Mime
{
    public $uidl = '';
    public $content;
    private $config = [
        "MIME-Version" => "1.0", //MIME版本 表示使用的MIME的版本号，一般是1.0;
        "Received" => "", //传输路径 各级邮件服务器
        "Return-Path" => "", //回复地址 目标邮件服务器
        "Delivered-To" => "", //发送地址 目标邮件服务器
        "Reply-To" => "", //回复地址 
        "From" => "", //发件人地址 
        "To" => "", //收件人地址 
        "Cc" => "", //抄送地址 
        "Bcc" => "", //暗送地址 
        "Date" => "", //日期和时间 
        "Subject" => "", //主题 
        "Message-ID" => "", //消息ID 
        /**
         * 内容的类型 
         * “主类型/子类型”的形式
         * 主类型有text, image, audio, video, application, multipart, message等，
         * 分别表示文本、  图片、   音频、   视频、   应用、         分段、      消息等。
         * 每个主类型都可能有多个子类型，如
         * text类型就包含plain, html, xml, css等子类型
         * 以X-开头的主类型和子类型，同样表示自定义的类型，未向IANA正式注册，但大多已经约定成俗了。如application/x-zip-compressed是ZIP文件类型。
         * 在Windows中，注册表的“HKEY_CLASSES_ROOT\MIME\Database\Content Type”内列举了除multipart之外大部分已知的Content-Type。
         * 关于参数的形式，RFC里有很多补充规定，有的允许带几个参数，较为常见的有:
         *  主类型         参数名     含义
         *  text        charset     字符集
         *  image       name        名称
         *  application name        名称
         *  multipart   boundary    边界 邮件中常用到的复合类型
         *  multipart/alternative ：表示正文由两个部分组成，可以选择其中的任意一个。
         *                          主要作用是在征文同时有text格式和html格式时，可以在两个正文中选择一个来显示，
         *                          支持 html 格式的邮件客户端软件一般会显示其 HTML 正文，而不支持的则会显示其Text正文；
         *  multipart/mixed       ：表示文档的多个部分是混合的，指正文与附件的关系。如果邮件的MIME类型是multipart/mixed，即表示邮件带有附件。
         *  multipart/related     ：表示文档的多个部分是相关的，一般用来描述 Html 正文与其相关的图片
         *  
         */
        "Content-Type" => "text/plain; charset=utf-8", 
        "Content-Transfer-Encoding" => "base64", //内容的传输编码方式 
        "Content-Disposition" => "", //段体的安排方式
        "Content-ID" => "", //段体的ID
        "Content-Location" => "", //段体的位置(路径)
        "Content-Base" => "", //段体的基位置
        "(Coremail)" => "", //段体的基位置
        "In-Reply-To" => "", //段体的基位置
        "References" => "" //段体的基位置
    ];
    function __construct()
    {
        $this->xMailer = "By Etsoftware-Mailer 1.0";
    }
    /**
     * encoding string
     * @param  [type] $str [description]
     * @return [type]      [description]
     */
    public function encode($str){
        $encoding = strtolower($this->contentTransferEncoding);
        if( $encoding == "base64" ){
            return base64_encode($str);
        }
        if( $encoding == "quoted-printable" ){
            return quoted_printable_encode($str);
        }
        if( $encoding == "7bit" ){//是缺省的编码方式
            // return quoted_printable_decode($str);
        }
        if( $encoding == "8bit" ){
            // return quoted_printable_decode($str);
        }
        if( $encoding == "Binary" ){
            // return quoted_printable_decode($str);
        }
        return $str;
    }
    /**
     * decode string
     * @param  [type] $str [description]
     * @return [type]      [description]
     */
    public function decode($str){
        $encoding = strtolower($this->contentTransferEncoding);
        // =?charset?B?xxxxxxxx?=
        // =?charset?Q?xxxxxxxx?=
        // =?gb18030?B?cmlta2U=?=
        if( preg_match("/^\s*=\?([^\?]+)\?([^\?]+)\?([^\?]+)\?=\s*$/", $str, $m) ){
            $encoding = strtolower($m[2]);
            if($encoding == 'b'){ $encoding = 'base64'; }
            if($encoding == 'q'){ $encoding = 'quoted-printable'; }
            $str = $m[3];
        }
        if( $encoding == "base64" ){
            return base64_decode($str);
        }
        if( $encoding == "quoted-printable" ){
            return quoted_printable_decode($str);
        }
        if( $encoding == "7bit" ){//是缺省的编码方式
            // return quoted_printable_decode($str);
        }
        if( $encoding == "8bit" ){
            // return quoted_printable_decode($str);
        }
        if( $encoding == "Binary" ){
            // return quoted_printable_decode($str);
        }
        return $str;
    }
    /**
     * analysis key for mime
     * @param  [type] $key [description]
     * @return [type]      [description]
     */
    private function analysisKey($key){
        if( preg_match_all("/([A-Z][a-z]+)|([a-z]+)|([A-Z]+)/m", $key, $mc, PREG_SET_ORDER) )
        {
            $key ="";
            foreach ($mc as $v) {
                $nkey = ucfirst($v[0]);
                $key .= $key?"-":'';
                $key .= ($nkey=='Id')?'ID':$nkey;
            }
        }
        return $key;
    }
    /**
     * set sender mail
     * @param [type] $showName show label in the mailbox list.
     * @param [type] $mail     email
     */
    public function setFrom($showName, $mail){
        $mime->from = "$showName<$mail>";
        return $this;
    }
    public function load($str){
        $ret = preg_match_all("/(.*)\r\n/im", $str, $mc, PREG_SET_ORDER);
        if(!$ret){$this->load($str."\r\n");}
        $header = ""; $body = "";
        $isbody = false;
        foreach ($mc as $v) {
            if($v[1] == ""){ $isbody=true; continue;}
            if($isbody == true){ 
                $body .= $v[0];
            }else{
                $header .= $v[0];
            }
        }
        $this->analysisHeader($header);
        $this->content = $body;
        // $this->content = $this->analysisBody($body);
    }
    /**
     * UTF to GB2312
     * @param  [type] $str [description]
     * @return [type]      [description]
     */
    public function utf2gb($str){
        return iconv("UTF-8", "gbk//TRANSLIT", $str);
    }
    /**
     * GB2312 to UTF
     * @param  [type] $str [description]
     * @return [type]      [description]
     */
    public function gb2utf($str, $gbk="GB2312"){
        return iconv($gbk, 'UTF-8', $str);
    }
    private function transChart($str, $mime, $charset='utf'){
        $charset = strtolower($charset);
        $cntCharset = null;
        if( preg_match("/.*\scharset=(.*)/im", $mime->contentType, $m) ){
            $cntCharset = strtolower($m[1]);
        }
        if(!$cntCharset){return $str;}
        if(strstr($charset, 'utf') && strstr($cntCharset, 'gb')){
            return $this->gb2utf($str);
        }
        if(strstr($charset, 'gb') && strstr($cntCharset, 'utf')){
            return $this->utf2gb($str);
        }
        return $str;
    }
    /**
     * get html with content
     * @param  string $charset [description]
     * @return [type]          [description]
     */
    public function getHTML($charset="utf"){
        return $this->analysisBody($this->content, $charset);
    }
    /**
     * analysis body content
     * @param  [type] $str [description]
     * @return [type]      [description]
     */
    public function analysisBody($str, $charset="utf"){
        $parts = []; $body = "";
        if($this->boundary){
            $endBoundary = "--".$this->boundary."--";
            $content = explode($endBoundary, $str);
            foreach ($content as $v) {
                $part = explode("--".$this->boundary, $v);
                $parts = array_merge($parts, $part);
            }
        }else{
            $parts = [$str];
        }
        foreach ($parts as $v) {
            $cnt = $v;
            if(preg_match_all("/^\s*(.+?):(.+)\s*\r\n/im", $v, $mc, PREG_SET_ORDER)){
                $mime = new self;
                foreach ($mc as $h) {
                    $cnt = str_replace($h[0], "", $cnt);
                    $mime->analysisHeader($h[0]);
                }
                $cnt= $mime->decode($cnt);
                $body .= $this->transChart($cnt, $mime, $charset);
            }else{
                $cnt= $this->decode($v);
                $body .= $this->transChart($cnt, $this, $charset);
            }
        }
        return $body;
    }
    /**
     * analysis header configuration
     * @param  [type] $str [description]
     * @return [type]      [description]
     */
    private function analysisHeader($str){
        preg_match_all("/^[\s\t]*([\w\-\_]+)\s*(:|=)\s*(.+)/im", $str, $mc, PREG_SET_ORDER);
        foreach ($mc as $v) {
            $key = $this->analysisKey($v[1]);
            $val = preg_replace("/\"?(.+?)\"?[\s\r\n]*$/im", "$1", $v[3]);
            $val = mb_decode_mimeheader($val);
            $this->config[$key] = $val;
        }
    }
    private function xToMail(){
        return $this->getMailByRFC822($this->to);
    }
    private function xFromMail(){
        return $this->getMailByRFC822($this->from);
    }
    private function xToLabel(){
        return $this->getLabelByRFC822($this->to);
    }
    private function xFromLabel(){
        return $this->getLabelByRFC822($this->from);
    }
    private function getMailByRFC822($rfc822){
        return preg_replace("/.*\<(.*)\>.*/", "$1", $rfc822);
    }
    private function getLabelByRFC822($rfc822){
        return preg_replace("/\s*(.*)\s*\<(.*)\>.*/", "$1", $rfc822);
    }
    /**
     * get value with mime's attribues
     * @param  [type] $key [description]
     * @return [type]      [description]
     */
    public function __get($key){
        $key = $this->analysisKey($key);
        if($key == 'X-From-Mail'){ return $this->xFromMail(); }
        if($key == 'X-To-Mail'){ return $this->xToMail(); }
        if($key == 'X-From-Label'){ return $this->xFromLabel(); }
        if($key == 'X-To-Label'){ return $this->xToLabel(); }
        $val = ($this->config[$key])??'';
        // $val = mb_decode_mimeheader($val);
        return $val;
    }
    /**
     * set value with mime's attribues
     * @param [type] $key [description]
     * @param [type] $val [description]
     */
    public function __set($key, $val){
        $key = $this->analysisKey($key);
        $val = mb_encode_mimeheader($val);
        if(isset($this->config[$key])){
            $this->config[$key] = $val;
        // }else if( preg_match("/^X\-.*/m", $key) ){
        }else {
            $this->config[$key] = $val;
        }
    }
    /**
     * get mime string
     * @return [type] [description]
     */
    public function toString(){
        $ret = '';
        $this->date = date("r");
        $this->messageId = preg_replace("/[^\d]+/im", "", microtime());
        foreach ($this->config as $k => $v) {
            $v && ($ret .= "$k:$v\r\n");
        }
        return $ret;
    }
}