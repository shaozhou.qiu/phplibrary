<?php
namespace Etsoftware\Lib;

//use Etsoftware\Support\Str;

class Configuration 
{
    public function __construct(){}

    private function iniGet($key=null){
        if($key){
            $key = preg_replace("/\W/", "_", $key);
            $key = strtolower($key);
            if($key == 'all'){ return  $this->iniGet(null); }
            $val = ini_get($key);
        }else{
            $val = ini_get_all();
        }
        return $val;
    }
    private function iniSet($key, $value){
        $key = preg_replace("/\W/", "_", $key);
        $key = strtolower($key);
        return ini_set($key, $value);
    }
    public function mb2b($val){
        return $this->kb2b($this->mb2kb($val));
    }
    public function mb2kb($val){
        return $this->unitStep($val, 1024);
    }
    public function kb2b($val){
        return $this->unitStep($val, 1024);
    }
    private function unitStep($val, $step){
        if(preg_match("/\s*(\d+).*/", "$val", $m)){
            $val = $m[1]*1;
        }
        return $val*$step;

    }
    public function __get($key){
        $key = $this->analysisKey($key);
        if( preg_match("/ini-(.*)/im", $key, $m) ){
            return $this->iniGet($m[1]);
        }
        return null;
    }
    public function __set($key, $value){
        $key = $this->analysisKey($key);
        if( preg_match("/ini-(.*)/im", $key, $m) ){
            return $this->iniSet($m[1], $value);
        }        
    }
    /**
     * analysis key for mime
     * @param  [type] $key [description]
     * @return [type]      [description]
     */
    private function analysisKey($key){
        if( preg_match_all("/([A-Z][a-z]+)|([a-z]+)|([A-Z]+)/m", $key, $mc, PREG_SET_ORDER) )
        {
            $key ="";
            foreach ($mc as $v) {
                $nkey = ucfirst($v[0]);
                $key .= $key?"-":'';
                $key .= ($nkey=='Id')?'ID':$nkey;
            }
        }
        return $key;
    }    
    public function __destory(){}

}