<?php
namespace Etsoftware\Lib;

use Etsoftware\Lib\File;

class Crontab 
{
    static 
    private $date = [
        "minute" => "*"
        , "hour" => "*"
        , "day" => "*"
        , "month" => "*"
        , "week" => "*"
    ];
    private $callback = null;
	function __construct($data=null, $type=null)
    {
        $data = $this->setDate($data);
    }
    /**
     * 执行定时任务
     * Crontab::exec("* * * * * /linuxsocket", function($aaa, $bbb){
            dump("*******");
            dump($a); // aaa
            dump($b); // bbb
            die;
        }, 'aaa', 'bbb');
     * @param  [type] $data     [description]
     * @param  [type] $callback [description]
     * @return [type]           [description]
     */
    static public function exec($data, $callback=null){
        $crontab = new self($data);
        $parmater = func_get_args();
        unset($parmater[0]);
        return call_user_func_array(array(&$crontab, 'call'), $parmater);
    }
    /**
     * string to array
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    private function arrayToCdate($data){
        if(!is_array($data)){ return null; }
        $reVal = [];
        $n = 0;
        foreach ($this->date as $k => $v) {
            if(isset($data[$k])){$v = $data[$k];}
            else if(isset($data[$n])){$v = $data[$n];}
            $reVal[$k] =$v;
            $n++;
        }
        return $reVal;
    }
    private function toArray($data){
        if(!$data) return null;
        if(is_array($data)){ return $this->arrayToCdate($data); }
        if( strlen($data) < 9)return null;
        $data = preg_replace("/\s+/im", " ", $data);
        $data = explode(" ", $data);
        return $this->arrayToCdate($data);
    }
    private function verifyData($data, $value){
        if(preg_match("/^\s*\*\s*$/im", $data)){
            return true;
        }else if(preg_match("/^\s*\d+\s*$/im", $data)){
            $data=[$data*1];
        }else if(preg_match("/^\s*\*\/(\d+)\s*$/im", $data, $m)){
            $stp =$m[1]*1;
            $max = $value*1 + $stp;
            $data = [];
            for($n = $stp; $n < $max; $n+=$stp){
                array_push($data, $n);
            }
        // }else if(preg_match("/^[\s\d\/\*\,]*$/im", $data, $m)){
        }else{
            $ds = explode(",", $data);
            $data = [];
            foreach ($ds as $d) {
                $stps = explode("-", $d);
                $num = count($stps);
                if( $num == 1){
                    array_push($data, $d*1);
                    continue;
                }else if( $num == 2){
                    list($min, $max) = $stps;
                    $min*=1; $max*=1;
                    if($min > $max){
                        $min += $max;
                        $max = $min-$max;
                        $min -= $max;
                    }
                    for(;$min <= $max; $min++){
                        array_push($data, $min);
                    }
                }
            }
        }
        $pattern = "";
        foreach ($data as $v) { $pattern .= "|($v)"; }
        $pattern = "/^(".substr($pattern, 1).")$/im";
        if( preg_match($pattern, ($value*1)."")){
            return true;
        }
        return false;
    }
    private function verification($date){
        $ret = $this->verifyData($date['minute'], date("i"));
        if(!$ret) return false;
        $ret = $this->verifyData($date['hour'], date("H"));
        if(!$ret) return false;
        $ret = $this->verifyData($date['day'], date("d"));
        if(!$ret) return false;
        $ret = $this->verifyData($date['month'], date("m"));
        if(!$ret) return false;
        $ret = $this->verifyData($date['week'], date("w"));
        if(!$ret) return false;
        return true;
    }
    public function setDate($data){
        $data = $this->toArray($data);
        if(!$data)return $this;
        $this->date = $data;
        
    }
    public function getDate(){ return $this->date;}
    public function call($callback=null){
        if(!$callback){ $callback = $this->callback; };
        if(!$callback || !is_callable($callback)){ $callback = function(){ return true; }; };
        $this->callback = $callback;
        $ret = $this->verification($this->date);
        if($ret){
            $data = func_get_args();
            if(func_num_args()){ unset($data[0]); }
            $ret = call_user_func_array($callback, $data);
            // $callback->call($this, $data);
            // is_callable
            // call_user_func("fun", 参数1, 参数2);
            // call_user_func_array("fun", array(参数1, 参数2));
        }
        return $ret;
    }
}