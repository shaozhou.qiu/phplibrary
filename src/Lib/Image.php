<?php
namespace Etsoftware\Lib;

use Etsoftware\Lib\File;

class Image
{
	protected $image = null;
	private $imgType = array(""
		,IMAGETYPE_GIF=> "gif"
		,IMAGETYPE_JPEG=> "jpeg"
		,IMAGETYPE_PNG=> "png"
		,IMAGETYPE_SWF=> "swf"
		,IMAGETYPE_PSD=> "psd"
		,IMAGETYPE_BMP=> "bmp"
		,IMAGETYPE_TIFF_II=> "tiff_ii"
		,IMAGETYPE_TIFF_MM=> "tiff_mm"
		,IMAGETYPE_JPC=> "jpc"
		,IMAGETYPE_JP2=> "jp2"
		,IMAGETYPE_JPX=> "jpx"
		,IMAGETYPE_JB2=> "jb2"
		,IMAGETYPE_SWC=> "swc"
		,IMAGETYPE_IFF=> "iff"
		,IMAGETYPE_WBMP=> "wbmp"
		,IMAGETYPE_XBM=> "xbm");
	private $imgTypei = 0;
	private $png = 1;
	private $bgcolor = 0xffffff;
	private $width = 0;
	private $height = 0;
	private $fontfile = null;
	public function __construct($width=200, $height=20, $color=0xffffff)
	{
		if( is_string($width) ){
			$file = $width;
			$this->image = $this->loadImage($file);
			if($this->image){
				$imgi = preg_replace("/.*\/([^\s]+)/im", "$1", getimagesize($file)['mime']);
				dump($imgi); 
				$this->imgTypei = array_search($imgi, $this->imgType);
				$width = imagesx($this->image);
				$height = imagesy($this->image);
			}else{
				$width = $height;
			}
		}
		$this->width = intval($width);
		$this->height = intval($height);
		$this->bgcolor = $color;
		if($this->image == null){
			$this->image = imagecreatetruecolor($width, $height);
			$bgcolor = $this->getColor($this->bgcolor);
			ImageFilledRectangle($this->image, 0, 0, $width, $height, $bgcolor);
			// Imagejpeg($this->image, $file_name); 
		}
	}
    static function getType($data){
    	if(preg_match("/^(\/\w*)+$/im", $data)){
    		return ;
    	}
    	if(self::isPng($data)) return "PNG";
    	if(self::isJpeg($data)) return "JPEG";

    	die;
    	return null;
    }
    static function isJpeg($data){
    	
    	return true;
    }
    static function isPng($data){
    	$d = [137, 80, 78, 71, 13, 10,  26, 10];
    	$l = count($d);
		$h = substr($data, 0, $l);
    	for($i=0; $i<$l; $i++){
    		if(ord(substr($h, $i, 1)) != $d[$i] ) return false;
    	}
    	return true;
    }
    static function thumb($src, $rate=0.8, $filename=null){
        $img = new self($src);
        $img->zoom($rate);
        if($filename){
            $img->toJpeg($filename);
        }
        return $img->image;
    }
	/**
	 * get gd information
	 * @return [type] [description]
	 */
	public function getInformation(){
		$reVal = [
			'width' => imagesx($this->image)
			,'height' => imagesy($this->image)
		];
		return $reVal;
	}
	public function zoomByWidth($width){
		$width = $width?$width:$this->width;
		return $this->zoom($width / $this->width);
	}
	public function zoomByHeight($height){
		$height = $height?$height:$this->height;
		return $this->zoom($height / $this->height);
	}
	/**
	 * Zoom image with rate
	 */
	public function zoom($rate=1){
		$width = intval($this->width * $rate);
		$height = intval($this->height * $rate);
		// dump($width); dump($height); die();
		$img = imagecreatetruecolor($width, $height);
		if( 
			imagecopyresampled($img, $this->image, 0, 0, 0, 0, $width, $height, $this->width, $this->height)
			 ){
			imagedestroy($this->image);
			$this->image = $img;
			$this->width = $width;
			$this->height = $height;
		}
		return $this;
	}
	/**
	 * Load image for file
	 * @param  [type] $file [description]
	 * @return [type]       [description]
	 */
	public function loadImage($file){
		if(!file_exists($file))return null;
		$img = null;
		$srcInfo = getimagesize($file);
		$imgformat = [""
			,"imagecreatefromgif"
			,"imagecreatefromjpeg"
			,"imagecreatefrompng"
			,"SWF"
			,"PSD"
			,"imagecreatefrombmp"
			,"TIFF"
			,"TIFF"
			,"JPC"
			,"JP2"
			,"JPX"
			,"JB2"
			,"SWC"
			,"IFF"
			,"imagecreatefromwbmp"
			,"imagecreatefromxbm"
		];	
		$srcInfo = getimagesize($file);
		$imfrmCreate = $imgformat[$srcInfo[2]];
		if(is_callable($imfrmCreate)) $img = $imfrmCreate($file);
		return $img;
	}
	// 创建image对象
	static public function imagecreatefrom($url){
		$type = "jpeg";
		$img = @imagecreatefromjpeg($url);
		if(false == $img) {$type = "bmp"; $img = @imagecreatefrombmp($url);}
		if(false == $img) {$type = "gd2"; $img = @imagecreatefromgd2($url);}
		if(false == $img) {$type = "gd2part"; $img = @imagecreatefromgd2part($url);}
		if(false == $img) {$type = "gd"; $img = @imagecreatefromgd($url);}
		if(false == $img) {$type = "png"; $img = @imagecreatefrompng($url);}
		if(false == $img) {$type = "gif"; $img = @imagecreatefromgif($url);}
		// if(false == $img) {$type = "string"; $img = @imagecreatefromstring($url);}
		if(false == $img) {$type = "wbmp"; $img = @imagecreatefromwbmp($url);}
		if(false == $img) {$type = "webp"; $img = @imagecreatefromwebp($url);}
		if(false == $img) {$type = "xbm"; $img = @imagecreatefromxbm($url);}
		if(false == $img) {$type = "xpm"; $img = @imagecreatefromxpm($url);}
		if(false == $img) {$type = "xpm"; $img = @imagecreatefromxpm($url);}
		return !$img?null:['type'=>$type, 'img'=>$img];
	}	
	private function outPut($type, $fileName, $fun=null)
	{
		ob_end_clean();
		if (!$fileName){
			header("Content-Type: " . image_type_to_mime_type($type));
			$fileName = null;
			// header("Content-Type: " . image_type_to_mime_type(IMAGETYPE_JPEG));
		}
	    switch ($type){
	      case 0:
	        // Imagejpeg($this->image, $fileName);
	      case 1:
	        imagegif($this->image, $fileName);
	        break;
	      case 2:
	        Imagejpeg($this->image, $fileName);
	        break;
	      case 3:
			imagealphablending($this->image, false);
			imagesavealpha($this->image, true);	      
	        Imagepng($this->image, $fileName);
	        break;
	      case 6:
	        imagebmp($this->image, $fileName);
	        break;
	      case 15:
	        imagewbmp($this->image, $fileName);
	        break;
	      case 16:
	        imagexbm($this->image, $fileName);
	        break;
	      default:
	        Imagejpeg($this->image, $fileName);
	        break;
	    }
		if (!$fileName){ $this->destory();die; }
	    return $this->image;
	}
	public function getImage(){
		return $this->image;
	}
	/**
	 * save or response image 
	 * @param  [type] $fileName save path
	 * @param  [type] $imgType  images type with jpeg,gif,png....
	 * @return [type]           [description]
	 */
	public function toImages($fileName=null, $imgType=null){
		if($imgType){
			$this->imgTypei = array_search($imgType, $this->imgType);
		}
		return $this->outPut($this->imgTypei, $fileName);	
	}
	public function toJpeg($fileName=null){
		return $this->outPut(IMAGETYPE_JPEG, $fileName);
	}
	public function toPng($fileName=null){
		return $this->outPut(IMAGETYPE_PNG, $fileName);
	}
	public function toGif($fileName=null){
		return $this->outPut(IMAGETYPE_GIF, $fileName);
	}
	public function toBmp($fileName=null){
		return $this->outPut(IMAGETYPE_BMP, $fileName);
	}
	private function getColor($color=0x000000, $callback=null){
		if(!$color){
			$color = $this->bgcolor ^ 0xffffff;
		}
		$color = imagecolorallocate($this->image, $color>>16, ($color>>8)&0x00ff, $color&0xff);		
		if($callback){
			return $callback->call($this, $color);
		}else{
			return $color;
		}
	}
	private function getPoints($num, ...$args){
		$points = [];
		if($num==0){ $num = count($args)/2; }
		for($i=0; $i < $num; $i++){
			array_push($points, ['x'=>rand(0, $this->width/2), 'y'=>rand(0, $this->height/2)]);
		}
		$idx=0;
		$points = array_map(function($v)use(&$idx, $args){
			if(isset($args[$idx]))$v['x'] = $args[$idx];
			$idx++;
			if(isset($args[$idx])) $v['y'] = $args[$idx];
			$idx++;
			return $v;
		}, $points);
		return $points;
	}
	public function width(){ return $this->width; }
	public function height(){ return $this->height; }
	public function getFontFile(){ return $this->fontfile; }

    /**
     * draw Circle to the image
     * @param string $file
     * @return mixed
     */		
	public function drawImage($file, $dstX=0, $dstY=0, $srcX=0, $srcY=0, $srcWidth=null, $srcHeight=null){
		$srcImage = $this->loadImage($file);
		if(!$srcImage) return $this;
		if($srcWidth === null){
			$srcWidth = $srcInfo[0];
		}
		if($srcHeight === null){
			$srcHeight = $srcInfo[1];
		}
		imagecopy($this->image, $srcImage, $dstX, $dstY, $srcX, $srcY, $srcWidth, $srcHeight);
		imagedestroy($srcImage);
		return $this;
	}
    /**
     * fill arc to the image
     * @param string $str
     * @param int $x   Default is 0, -1 is randomly generated
     * @param int $y   Default is 0, -1 is randomly generated
     * @return mixed
     */		
	public function drawFill($x=0, $y=0, $color=0x000000){
		$points = $this->getPoints(0, $x, $y);
		return $this->getColor($color, function($color)use($points){
			// dump($points); die;
			imagefill($this->image, $points[0]['x'], $points[0]['y'], $color);
			// imagesavealpha($this->image, TRUE);
		});
		// imagefilledarc($this->image, $x, $y, $width, $height, $start, $end, $color, $style);
	}	
    /**
     * fill arc to the image
     * @param string $str
     * @param int $x   Default is 0, -1 is randomly generated
     * @param int $y   Default is 0, -1 is randomly generated
     * @return mixed
     */		
	public function drawFilledarc($x=0, $y=0, $width=1, $height=1, $start=0, $end=0, $color=0x000000, $style=IMG_ARC_PIE){
		$points = $this->getPoints(0, $x, $y);
		return $this->getColor($color, function($color)use($points, $width, $height, $start, $end, $style){
			// $style = IMG_ARC_CHORD|IMG_ARC_NOFILL|IMG_ARC_EDGED;
			imagefilledarc($this->image, $points[0]['x'], $points[0]['y'], $width, $height, $start, $end, $color, $style);
		});
		// imagefilledarc($this->image, $x, $y, $width, $height, $start, $end, $color, $style);
	}	
    /**
     * draw Pixel to the image
     * @param string $str
     * @param int $x   Default is 0, -1 is randomly generated
     * @param int $y   Default is 0, -1 is randomly generated
     * @return mixed
     */		
	public function drawPixel($x=0, $y=0, $color=0x000000){
		$points = $this->getPoints(0, $x, $y);
		return $this->getColor($color, function($color)use($points){
			imagesetpixel($this->image, $points[0]['x'], $points[0]['y'], $color);
		});
	}
    /**
     * draw Circle to the image
     * @param string $str
     * @param int $x   Default is 0, -1 is randomly generated
     * @param int $y   Default is 0, -1 is randomly generated
     * @return mixed
     */		
	public function drawPoint($x=0, $y=0, $r=1, $color=0x000000){
		return $this->drawFilledarc($x, $y, $r, $r, 0, 0, $color);
	}
    /**
     * draw Circle to the image
     * @param string $str
     * @param int $x   Default is 0, -1 is randomly generated
     * @param int $y   Default is 0, -1 is randomly generated
     * @return mixed
     */		
	public function drawCircle($x1=0, $y1=0, $r=null, $color=0x000000){
		return $this->drawEllipse($x1, $y1, $r??10, $r, $color);
	}
    /**
     * draw Ellipse to the image
     * @param string $str
     * @param int $x   Default is 0, -1 is randomly generated
     * @param int $y   Default is 0, -1 is randomly generated
     * @return mixed
     */		
	public function drawEllipse($x1=0, $y1=0, $x2=null, $y2=null, $color=0x000000){
		$points = $this->getPoints(0, $x1, $y1, $x2, $y2);
		return $this->getColor($color, function($color)use($points){
			imageellipse($this->image, $points[0]['x'], $points[0]['y'], $points[1]['x'], $points[1]['y'], $color);
			return $this;
		});
	}
    /**
     * draw Square to the image
     * @param string $str
     * @param int $x   Default is 0, -1 is randomly generated
     * @param int $y   Default is 0, -1 is randomly generated
     * @return mixed
     */		
	public function drawSquare($x1=0, $y1=0, $r=10, $color=0x000000){
		return $this->drawRectangle($x1, $y1, $x1+$r, $y1+$r, $color);
	}
    /**
     * draw Rectangle to the image
     * @param string $str
     * @param int $x   Default is 0, -1 is randomly generated
     * @param int $y   Default is 0, -1 is randomly generated
     * @return mixed
     */		
	public function drawRectangle($x1=0, $y1=0, $x2=null, $y2=null, $color=0x000000){
		$points = $this->getPoints(0, $x1, $y1, $x2, $y2);
		return $this->getColor($color, function($color)use($points){
			imagerectangle($this->image, $points[0]['x'], $points[0]['y'], $points[1]['x'], $points[1]['y'], $color);
			return $this;
		});
	}
    /**
     * draw line to the image
     * @param string $str
     * @param int $x   Default is 0, -1 is randomly generated
     * @param int $y   Default is 0, -1 is randomly generated
     * @return mixed
     */		
	public function drawLine($x1=0, $y1=0, $x2=null, $y2=null, $color=0x000000){
		$points = $this->getPoints(0, $x1, $y1, $x2, $y2);
		return $this->getColor($color, function($color)use($points){
			imageline($this->image, $points[0]['x'], $points[0]['y'], $points[1]['x'], $points[1]['y'], $color);
			return $this;
		});
	}
    /**
     * draw string to the image
     * @param string $str
     * @param int $x   Default is 0, -1 is randomly generated
     * @param int $y   Default is 0, -1 is randomly generated
     * @return mixed
     */		
	public function drawString($str, $fontsize=1, $x=0, $y=0, $color=0x000000){
		return $this->getColor($color, function($color)use($str, $fontsize, $x, $y){
			imagestring($this->image, $fontsize, $x, $y, $str, $color);
			return $this;
		});		
	}
    /**
     * draw text to the image
     * @param string $str
     * @param int $fontsize 字体大小
     * @param int $angle   旋转角度
     * @param int $x   
     * @param int $y   
     * @param int $color   字体的颜色
     * @param int $fontfile   字体
     * @param int $fontweight   粗体
     * @return mixed
     */		
	public function drawText($str, $fontsize=1, $angle=0, $x=0, $y=0, $color=0x000000, $fontfile=null, $fontweight=1){
		return $this->getColor($color, function($color)use($str, $fontsize, $angle, $x, $y, $fontfile, $fontweight){
			if($fontfile == null && null != $this->fontfile){ $fontfile = $this->fontfile; }
			if( $fontfile == null || is_numeric($fontfile)){
				try {
					$fonts = [];
					$path = null;
					$paths = [
						$_SERVER['DOCUMENT_ROOT']
						, $_SERVER['DOCUMENT_ROOT']."/font"
						, $_SERVER['DOCUMENT_ROOT']."/fonts"
						, File::getAbsolute(__FILE__."/../../../font")
					];
					
					foreach ($paths as $v) {
						if(!file_exists($v))continue;
						$fs = File::scandir($v, "/\.((otf)|(fon)|(ttf)|(ttc))/im");
						$fs = array_values(array_filter($fs));
						$fs = array_map(function($f)use($v){return $v.DIRECTORY_SEPARATOR."$f"; }, $fs);
						foreach ($fs as $f) {
							array_push($fonts, $f);
						}
					}
					if(count($fonts)<1){
						try {
							$usrfs =shell_exec("find /usr/share/fonts -name \"*.ttf\"");
							if($usrfs){ $fonts = explode("\n", $usrfs); }
						} catch (Exception $e) {}
					}
					if(count($fonts)){
						$idx = is_numeric($fontfile)?$fontfile:mt_rand(0,count($fonts)-1);
						if($idx>count($fonts))$idx = 0;
						$fontfile = $fonts[$idx];
					}
				}catch(Exception $e){
					return false;
				}
			}
			if($fontfile == null ){throw new \Exception("Err : Font file not found!"); return false;}
			$this->fontfile = $fontfile;
			while ( $fontweight-- ){
				if($y==0 && $x==0)$y = $fontsize;// 防止给出文字超出边界
				@imageTtfText($this->image, $fontsize, $angle, $x, $y, $color, $fontfile, $str);
			}
			return $this;
		});		
	}
    /**
     * draw char to the image
     * @param string $str
     * @param int $x   Default is 0, -1 is randomly generated
     * @param int $y   Default is 0, -1 is randomly generated
     * @return mixed
     */		
	public function drawChars($str, $fontsize=1, $x=0, $y=0, $color=0x000000){
		return $this->getColor($color, function($color)use($str, $fontsize, $x, $y){
			$len = strlen($str);
			for($i=0; $i<$len; $i++){
			    if($x === null || $x == -1){
			    	$xpos = rand(0, $this->width-imagefontwidth($fontsize));
			    }else{
			    	$xpos = $x + $i * imagefontwidth($fontsize);
			    }
			    if($y === null || $y == -1){
			    	$ypos = rand(0, $this->height-imagefontheight($fontsize));
			    }else{
			    	$ypos = $y;
			    }
				imagechar($this->image, $fontsize, $xpos, $ypos, $str, $color);
			    $str = substr($str, 1);
			}
			return $this;
		});
	}
	public function destory(){
		if($this->image){
			imagedestroy($this->image);
			$this->image = null;
		}
	}
	public function __destruct(){
		$this->destory();
	}

}