<?php
namespace Etsoftware\Lib;

class Xml
{

	function __construct()
    {
        
    }

    static function toString($arr, $rootNodeName="xml"){
        if (is_array($arr)) {
            return self::array2String($arr, $rootNodeName);
        }
        return "";
    }

    static function array2String($arr, $rootNodeName="xml", $defaultKey="item"){
        $str = "<$rootNodeName>";
        foreach ($arr as $k => $v) {
            if (is_array($v)) {
                $str .= self::array2String($v, $k);
            }else{
                if (is_numeric($k)) { $k = $defaultKey; }
                $str .= "\n<$k>";
                $str .= self::quote($v);
                $str .= "</$k>\n";
            }
        }
        $str .= "</$rootNodeName>";
        return $str;
    }

    static function quote($str){
        $delimiter = [
            "amp" => "&"
            ,"lt" => "<"
            , "gt" => ">"
            , "apos" => "'"
            , "quot" => "\""
        ];
        foreach ($delimiter as $k => $v) {
            $str = preg_replace("/".preg_quote($v)."/im", "&$k;", $str);
        }
        return $str;
    }

}