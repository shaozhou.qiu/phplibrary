<?php
namespace Etsoftware\Lib;

use Etsoftware\Lib\File;
use Etsoftware\Lib\Memory;

class Cache 
{
    private $rootPath ="";
    private $cacheFolder = null;
    private $app = null;
	function __construct($type='file')
    {
        $this->rootPath = $_SERVER['DOCUMENT_ROOT'];
        $cacheFolders = ['~runtime', 'cache', 'temp', 'uploadfiles'];
        foreach ($cacheFolders as $v) {
            $cacheFolder = $this->rootPath.DIRECTORY_SEPARATOR.$v;
            if(file_exists($cacheFolder)){break; }
        }
        if(!file_exists($cacheFolder)){
            if(!mkdir($cacheFolder)){$cacheFolder=null;}
        }
        if($cacheFolder ){
            $cacheFolder .= DIRECTORY_SEPARATOR."cache";
            if(!file_exists($cacheFolder)){
                if(!mkdir($cacheFolder)){$cacheFolder=null;}
            }
        }
        $this->cacheFolder = $cacheFolder;
        if($type == 'shmop'){
            $this->app = $this->shmopApp();
        }else{
            $this->app = $this->fileApp();
        }
    }

    /**
     * push data to cache
     * @param [type] $key   [description]
     * @param [type] $value [description]
     */
    public function push($key, $value){
        $key = $this->encryption($key);
        return $this->app->push($key, $value);
    }
    /**
     * set data to cache for empty.
     * @param [type] $key   [description]
     * @param [type] $value [description]
     */
    public function set($key, $value)
    {
        $key = $this->encryption($key);
        return $this->app->set($key, $value);

    }
    /**
     * Get Value by key
     * @param [type] $key [description]
     */
    public function get($key)
    {
        $key = $this->encryption($key);
        $ret = $this->app->get($key);
        return ($ret['errcode'] == 0)?$ret['data']:null;
    }
    /**
     * destry key
     * @param  [type] $key [description]
     * @return [type]      [description]
     */
    public function unset($key){
        $key = $this->encryption($key);
        $this->app->unset($key);
    }
    private function encryption($key){
        return hash("sha1", $key);
    }

    private function fileApp(){
        return new Class($this->cacheFolder){
            private $path = '';
            public function __construct($path){ $this->path = $path; }
            public function push($key, $value){
                $fn = $this->path. DIRECTORY_SEPARATOR . "$key.tmp";
                $file = new File($fn);
                $file->append($value);
            }
            public function set($key, $value){
                $fn = $this->path. DIRECTORY_SEPARATOR . "$key.tmp";
                $file = new File($fn);
                $file->write($value);
            }
            public function get($key){
                $fn = $this->path. DIRECTORY_SEPARATOR . "$key.tmp";
                $file = new File($fn);
                $ret = $file->read();
                return $ret;
            }
            public function unset($key){
                $fn = $this->path. DIRECTORY_SEPARATOR . "$key.tmp";
                $file = new File($fn);
                $file->delete();
            }
        };
    }
    private function shmopApp(){
        if( !Memory::enable() ){ return $this->fileApp(); }
        return new Class($this->cacheFolder){
            public function __construct(){}
            public function push($key, $value){
                $memory = new Memory();
                $data = json_decode($memory->read(), true);
                $data = $data?$data:[];
                $data[$key] = $value;
                $memory->write(json_encode($data, true));
            }
            public function set($key, $value){
                $memory = new Memory();
                $data = [$key=>$value];
                $memory->write(json_encode($data, true));
            }
            public function get($key){
                $memory = new Memory();
                $data = json_decode($memory->read(), true);
                $data = $data?$data:[];
                if(isset($data[$key])){
                    return ['errcode'=>0, 'data'=>$data[$key]];
                }
                return ['errcode'=>1007, 'data'=>'is unset'];
            }
            public function unset($key){
                $memory = new Memory();
                $data = json_decode($memory->read(), true);
                $data = $data?$data:[];
                unset($data[$key]);
                $memory->write(json_encode($data, true));
            }
        };
    } 
}