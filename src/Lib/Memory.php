<?php
namespace Etsoftware\Lib;

use Etsoftware\Lib\File;

class Memory 
{
    private $key = 0;
    private $shmid = FALSE;
    function __construct($key=0){
        if(!is_numeric($key)){
            if(is_string($key)){
                $v = 0;
                for($i=0;$i<strlen($key); $i++){
                    $cd = ord(substr($key, $i, 1));
                    $v += ($cd*$i + $cd/($i+1)+ $cd%($i+1) );
                }
                $key = round($v);
            }else{
                $key = ftok(__FILE__, 't');
            }
        }
        $this->key = ($key || $key<1)?$key:ftok(__FILE__, 't');
        if(!self::enable()){
            echo "No extension shmop found, please use yum install php-shmop to install.";
            exit();
        }
    }
    private function openShmop($key, $flags, $mode, $size, $callback){
        $shmid = @shmop_open($key, $flags, $mode, $size);
        if($shmid === FALSE || $shmid === null){ return null; }
        $ret = null;
        if(is_callable($callback)){
            $ret = call_user_func_array($callback, [$shmid]);
        }
        shmop_close($shmid);
        return $ret;

    }
    static public function enable(){
        return function_exists("shmop_open");
    }
    static public function list(){
        if(!self::enable())return [];
        // system("ipcs -m", $status);
        return [];
    }
    /**
     * 追加内容到内存
     * @param  [type] $value [description]
     * @return [type]        [description]
     */
    public function append($value){
        if(!$value)return false;
        return $this->write($this->read().$value);
    }
    /**
     * 读取内存内容
     * @return [type] [description]
     */
    public function read(){
        return $this->openShmop($this->key, 'a', 0, 0, function($e){
            $size = shmop_size($e);
            $data = @shmop_read($e, 0, $size);
            return $data;
        });
    }
    /**
     * 创建新内存空间，如何存在则删除
     * @param  [type] $value [description]
     * @return [type]        [description]
     */
    public function write($value){
        $size = strlen($value);
        if($size == 0)return FALSE;
        if($this->getSize()){ $this->delete(); }
        $this->shmid = @shmop_open($this->key, 'c', 0644, $size);
        if($this->shmid === FALSE || $this->shmid === null){ return null; }
        $len = @shmop_write($this->shmid, $value, 0);
        $this->close();
        return $len; 
    }
    /**
     * 删除内容空间
     * @return [type] [description]
     */
    public function unset(){$this->delete();}
    public function delete(){
        $this->shmid = @shmop_open($this->key, 'a', 0, 0);
        if($this->shmid !== FALSE){
            shmop_delete($this->shmid);
        }else{
            dump('delete memry fail.'); die;
        }
        $this->shmid = FALSE;
    }
    /**
     * 获取内容大小
     * @return [type] [description]
     */
    public function getSize(){
        $this->shmid = @shmop_open($this->key, 'a', 0, 0);
        if($this->shmid === FALSE){ return 0; }
        if($this->shmid !== null){
            $shm_size = shmop_size($this->shmid);
        }else{ $shm_size = 0; }
        $this->close();
        return $shm_size; 
    }
    public function close(){
        if($this->shmid !== FALSE && $this->shmid !== null){
            shmop_close($this->shmid);
            $this->shmid = FALSE;
        }
    }
    function __destory(){
        $this->close();
    }
    
}