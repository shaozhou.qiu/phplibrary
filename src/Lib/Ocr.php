<?php
namespace Etsoftware\Lib;

use Etsoftware\Lib\Image;
use Etsoftware\Lib\File;

class Ocr {
	private $img = null;
	private $type = null;
	private $filename = null;
	private $size = 0;
	private $font = null;
	private $hec = [];
	public function __construct($filename){
		$img = Image::imagecreatefrom($filename);
		if($img){
			$this->size = getimagesize($filename);
			$this->img = $img['img'];
			$this->type = $img['type'];
			$this->filename = $filename;
		}
	}


	public function setFont($font){
		if(!file_exists($font))return ;
		$this->font = $font;
	}
	public function getHecData($filename=null){
		if(!$filename)$filename=$this->filename;
		$hec = hash('sha1', "$filename");
		if(isset($this->hec[$hec]))return $this->hec[$hec];
		$img = Image::imagecreatefrom($filename);
		if(!$img)return ;
		list($w, $h) = getimagesize($filename);
		$data = $this->getHec($img['img'], $w, $h);
		$this->hec[$hec] = $data;
		return $data; 

	}
	public function getString(){
		$hecData = $this->getHecData();
		if(!$hecData)return 0;
		if($this->font==null){
			$usrfs =shell_exec("find /usr/share/fonts -name \"*.ttf\"");
			if($usrfs){ 
				$fonts = explode("\n", $usrfs);
				$this->font=$fonts[mt_rand(0,count($fonts)-1)];
			}
		}
		$sdcs = null;
		$dcs = $this->createHecDatas($this->getDefCharacterSet(), $this->font);
		$sdcs = array_map(function($e){return $e['sdata'][0]??null; }, $dcs);
		$text = $this->getfeatureCode($hecData['sdata'], $sdcs);
		return $text;
	}
	public function showHec($data){
		$c = "";
		if(is_array($data)){
			foreach ($data as $key => $val){
				foreach ($val as $k => $v){
					if($v == 0){
						$c .= "<font color='#FFFFFF'>".$v."</font>"; 
					}else{
						$c .= $v; 
					}
				} 
				$c .= "<br/>";
			} 
		}
		echo $c;
	}
	private function getfeatureCode($data, $dcs) {
		// $this->showHec($dcs['Y']);
		// $this->showHec($data[0]);
		// die;
		$ret = "";
		$dcs = array_map(function($e){return $this->a2s($e); }, $dcs);
		foreach ($data as $k => $v) {
			$sd = $this->a2s($v);
			$char = null;
			$similar = 90;
			while ($char==null && $similar>60) {
				$char = $this->getCharbyFC($sd, $dcs, $similar);
				$similar-=10;
			} 
			$ret .= $char?$char:" ";
		}
		return $ret;
	}
	// 根据特殊码获取字符
	private function getCharbyFC($data, $featureCode, $similar=90) {
				// dump($data);
		$result = null;
		foreach ($featureCode as $key => $val){
			similar_text($data, $val, $pre);
			// dump("$key = $pre");
			if($pre>$similar){//相似度90%以上
				$result = $key;
				break;
			}
		}
		// die;
		return $result;
	}
	private function a2s($data) {
		if(gettype($data)!="array")return $data;
		$ret = "";
		foreach ($data as $k => $v) {
			if(gettype($v)=="array"){
				$ret .= $this->a2s($v);
			}else{
				$ret .= $v;
			}
		}
		return $ret;
	}
	// 获取默认字符集
	private function getDefCharacterSet() {
		$txt = "";
		$data=[
			[33,47], [58,63], [91,96], [123,127] // 符号
			,[48,57] // 数字
			,[97,122] //小写字母
			,[65,90] //大写字母
		];
		foreach ($data as $d) {
			for($i=$d[0]; $i<=$d[1]; $i++) $txt .= chr($i);
		}
		return $txt;
	}
	// 根据字符获取hec数据
	private function createHecDatas($str, $font=null) {
		$fn = hash('sha1', "$str,$font").".orc";
		$fn = File::getAbsolute("downloadfiles/ocr/$fn");
		if(file_exists($fn)){
			$f = new File($fn);
			$ret = $f->read();
			if($ret['errcode']==0){
				$data = json_decode($ret['data'], true);
				return $data;
			}
		}
		$data = [];
		for($i=0; $i<strlen($str); $i++ ){
			$char = substr($str, $i, 1);
			$data[$char] = $this->createHecData($char, $font);
		}
		$f = new File($fn);
		$f->write(json_encode($data, true));
		return $data;
	}
	private function createHecData($txt, $font=null) {
		$w = $h = 100;
		$img = new Image($w, $h);
		$img->drawText($txt, 50, 0, $w/2, $h/2+1, null, $font);
		// $img->drawText("YTAD", 50, 0, 0, $h/2+1, null, $font);
		// $img->toJpeg(); 
		$data = $this->getHec($img->getImage(), $w, $h);
		$img->destory();
		return $data;
	}

	/**
	* 颜色分离后的数据横向整理...
	*
	* @return unknown
	*/
	private function getHorData($data, $width, $height) {
		$ret = [];
		$z = 0;
		for($i=0; $i<$height; ++$i) {
			if(in_array('1',$data[$i])){
				$z++;
				for($j=0; $j<$width; ++$j) {
					if($data[$i][$j] == '1'){
						$ret[$z][$j] = 1;
					}else{
						$ret[$z][$j] = 0;
					}
				}
			}
		}
		return $ret;
	}	
	/**
	* 分割二值数据
	*
	* @return unknown
	*/
	public function getSplitData($data){
		$col = 0;
		foreach ($data as $v) {
			$col = count($v); if($col>0)break;
		}
		$splt = []; $n=0; $j=0;
		$s=0; $e=0;
		for($c=0; $c<$col; $c++){
			if($s==0){
				foreach ($data as $v) { 
					if($v[$c]== 1){ $s = $c; break; }
				}
			}else if($e==0){
				$issp = true;
				foreach ($data as $v) { 
					if($v[$c]== 1){ $issp = false; break; }
				}
				if($issp){ $e = $c; }
			}else{
				array_push($splt, ["s"=>$s, "e"=>$e]);
				$s=0; $e=0;
			}
		}
		if(count($splt)<1) return [];
		$ndata = [];
		for($i=0; $i<count($splt); $i++){
			$char = $splt[$i];
			$s = $char["s"];
			$e = $char["e"];
			$d = [];
			foreach ($data as $v) {
				$rd = [];
				for($j=$s; $j<$e; $j++){
					array_push($rd, $v[$j]);
				}
				array_push($d, $rd);
			}
			array_push($ndata, $d);
		}
		return $ndata;
	}
	// 颜色分离转换
	private function getHec($img, $width, $height){
		$data=[];
		for($i=0; $i < $height; ++$i) {
			for($j=0; $j < $width; ++$j) {
				$rgb = @imagecolorat($img, $j, $i);
				$rgbarray = @imagecolorsforindex($img, $rgb);
				if(null == $rgbarray)continue;
				if($rgbarray['red'] < 125 
					|| $rgbarray['green']<125
					|| $rgbarray['blue'] < 125) {
						$data[$i][$j]=1;
				}else{
					$data[$i][$j]=0;
				}
			}
		}
		if(count($data)<1)return null;
		$hdata = $this->getHorData($data, $width, $height);
		return [
			"data" => $data
			,"hdata" => $hdata
			,"sdata" => $this->getSplitData($hdata)
		];
	}

}