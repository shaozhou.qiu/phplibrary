<?php
namespace Etsoftware\Lib;

class StringUtil
{
    
	function __construct($str)
    {
        
    }
    /**
     * 将字符串转换成16进制表达
     * @param  [type] $buff 字符
     * @param  [type] $len  长度
     * @return [type]       [description]
     */
    static public function char2Hexstr($buff, $len) {
        $reVal = ["length"=>0, "data"=>""];
        for ($i=0; $i < $len; $i++) {
            $c = substr($buff, $i, 1); 
            $reVal["length"]++;
            $reVal["data"]=sprintf("%s 0x%02X", $reVal["data"], ord($c));
        }
        if(strlen($reVal["data"])) $reVal["data"] = substr($reVal["data"], 1);
        return $reVal;
    }
    /**
     * 将16进制表达式转换成字符
     * @param  [type] $hexstr [description]
     * @return [type]         [description]
     */
    static public function hexstr2Char($hexstr) {
        $reVal = ["length"=>0, "data"=>$hexstr];
        if(preg_match_all("/\\\\(0x[0-9a-f]{1,2})/im", $hexstr, $mc, PREG_SET_ORDER)){
            foreach($mc as $m){
                $d=hexdec($m[1]);
                $reVal["length"]++;
                $reVal["data"] = preg_replace("/".preg_quote($m[0])."/im", chr($d), $reVal["data"]);
            }
        }
        return $reVal;
    }
    /**
     * 排除字符串
     * @param  [type] $content [description]
     * @param  [type] $reg     [description]
     * @param  [type] $cb      [description]
     * @return [type]          [description]
     */
    static public function exclude($content, $reg, $cb) {
        if(preg_match_all($reg, $content, $mc, PREG_SET_ORDER)){
            foreach($mc as $idx=>$m){
                $v = preg_replace("/".preg_quote($m[0])."/im", "{{ET_EXCLUDE_MC_$idx}}", $content);
                if($v == null)continue;
                $content = $v;
            }
            $content = $cb($content);
            if(preg_match_all("/".preg_quote("{{ET_EXCLUDE_MC_")."(\\d+)".preg_quote("}}")."/im", $content, $fmc, PREG_SET_ORDER)){
                foreach($fmc as $m){
                    $mcv = $mc[$m[1]]??[""];
                    $v = preg_replace("/".preg_quote($m[0])."/im", preg_quote($mcv[0]), $content);
                    if($v == null)continue;
                    $content = $v;
                }
            }
        }else{
            $content = $cb($content);
        }
        return $content;
    }
    static public function is_utf8($str) {
        return preg_match('%^(?:
        [\x09\x0A\x0D\x20-\x7E] # ASCII
        | [\xC2-\xDF][\x80-\xBF] # non-overlong 2-byte
        | \xE0[\xA0-\xBF][\x80-\xBF] # excluding overlongs
        | [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2} # straight 3-byte
        | \xED[\x80-\x9F][\x80-\xBF] # excluding surrogates
        | \xF0[\x90-\xBF][\x80-\xBF]{2} # planes 1-3
        | [\xF1-\xF3][\x80-\xBF]{3} # planes 4-15
        | \xF4[\x80-\x8F][\x80-\xBF]{2} # plane 16
        )*$%xs', $str);
    }    
    /**
     * 判断是否为base64加密
     * @param  [type]  $str [description]
     * @return boolean      [description]
     */
    static public function is_base64($str){
        if(@preg_match('/^[0-9]*$/',$str) || @preg_match('/^[a-zA-Z]*$/',$str)){
            return false;
        }else{
            $b = base64_decode($str);
            if(self::is_utf8($b) && $b != '') return true;
        }
        return false;        
    }
    static public function encodeUnicode($str){
        $strArr = preg_split('/(?<!^)(?!$)/u', $str);//拆分字符串为数组(含中文字符)
        $resUnicode = '';
        foreach ($strArr as $str)
        {
            $bin_str = '';
            $arr = is_array($str) ? $str : str_split($str);//获取字符内部数组表示,此时$arr应类似array(228, 189, 160)
            foreach ($arr as $value) {
                $bin_str .= decbin(ord($value));//转成数字再转成二进制字符串,$bin_str应类似111001001011110110100000,如果是汉字"你"
            }
            $bin_str = preg_replace('/^.{4}(.{4}).{2}(.{6}).{2}(.{6})$/', '$1$2$3', $bin_str);//正则截取, $bin_str应类似0100111101100000,如果是汉字"你"
            $unicode = dechex(bindec($bin_str));//返回unicode十六进制
            $_sup = '';
            for ($i = 0; $i < 4 - strlen($unicode); $i++)
            {
                $_sup .= '0';//补位高字节 0
            }
            $str =  '\\u' . $_sup . $unicode; //加上 \u  返回
            $resUnicode .= $str;
        }
        return $resUnicode;
    }
    static public function decodeUnicode($unStr){
        // 转换编码，将Unicode编码转换成可以浏览的utf-8编码
        $ret = $unStr;
        preg_match_all("/\\\\u([0-9a-f]{2})([0-9a-f]{2})/im", $unStr, $mc, PREG_SET_ORDER);
        if (!empty($mc)) {
            foreach ($mc as $m) {
                $c1 = base_convert($m[1], 16, 10);
                $c2 = base_convert($m[2], 16, 10);
                $val = chr($c1).chr($c2);
                $val = iconv('UCS-2', 'UTF-8', $val);
                $ret = preg_replace("/".preg_quote($m[0])."/im", $val, $ret);            
                
            }
        }
        return $ret;        
    }
    static public function unicodeToString($unStr){
        $unStr = preg_replace("/(\"|\n)/im", "\\\\$1", $unStr);
        $json = '{"str":"' . $unStr . '"}';
        $arr = json_decode($json, true);
        if (empty($arr)) return '';
        return $arr['str'];        
    }
    static public function base64Encode($data){
        $reVal = base64_encode($data);
        return $reVal;
    }
    static public function base64Decode($data){
        $data = str_replace(' ','+',$data);
        $reVal = "";
        for ($i=0; $i < ceil(strlen($data)/256); $i++)
        $reVal = $reVal . base64_decode(substr($data,$i*256,256));
        return $reVal;
    }
}