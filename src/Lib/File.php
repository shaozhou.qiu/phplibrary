<?php
namespace Etsoftware\Lib;

//use Etsoftware\Support\Str;

class File
{
    private $filename = null;
    private $handle = null;
    private $data = [];
    private $errcode = [
        '0'=>'Ok'
        , '10001'=>'file does not exist'
        , '10002'=>'Current mode does not allow write operations'
        , '10003'=>'Current file system not allow write operations'
        , '10004'=>'Failed to create file.'
        , '10005'=>'No writable data, you can use push data.'
        , '10006'=>'Failed to delete file'
        , '10007'=>'The parameter is incorrect'
        , '10008'=>'Failed to create directory'
    ];
    /**
     * create file handle
     * @param  [type] $filename  document_root/xxxx.tmp default
     *                            xxx.tmp    as document_root/xxx.tmp
     *                            ../xxx.tmp    as document_root/../xxx.tmp
     * @return [type]        [description]
     */
	function __construct($filename=null)
    {
        $this->filename = self::getAbsolute( $filename );
        // $this->filename = self::getAbsolute( "ss.txt" );
        // dump($this->filename); die;
        // $this->mode = strtolower($mode);
    }
    /**
     * 生成树形目录
     * @param  [type] $ruler [description]
     * @return [type]        [description]
     */
    static function buildTreeDirectory($ruler=null){
        $root = $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR;
        if($ruler==null){
            $root .= "{date:Y".DIRECTORY_SEPARATOR."m".DIRECTORY_SEPARATOR."d}";
        }else{
            $ruler = preg_replace("/^\\".DIRECTORY_SEPARATOR."(.*)/im", "$1", $ruler);
            $root .= $ruler;
        }
        $ret = preg_match_all("/\{([^\}\{]*)\}/im", $root, $mc, PREG_SET_ORDER);
        if($ret){
            foreach ($mc as $m) {
                $v = "";
                if(preg_match("/([^:]+):(.*)/", $m[1], $mm)){
                    if($mm[1]=="date"){
                        $v = date($mm[2]);
                    }else{
                    }
                }
                $root = str_replace($m[0], $v, $root);
            }
        }
        $root = preg_replace("/(\\\\){1,}/im", DIRECTORY_SEPARATOR, $root);
        $root = preg_replace("/(\\".DIRECTORY_SEPARATOR."){2,}/im", "$1", $root);
        if(!preg_match("/.*\\".DIRECTORY_SEPARATOR."$/im", $root)){
            $root .= DIRECTORY_SEPARATOR;
        }
        return $root;
    }
    /**
     * write data to file with empty.
     * @param  [type] $value [description]
     * @return [type]        [description]
     */
    public function getAttributes(){
        return self::Attributes($this->filename);
    }
    static function quote($content){
        $fn = $content;
        $specialCharacters = [];
        if(preg_match_all("/[\<\>\/\\\|\:\"\*\?]/im", $fn, $mc)){
           foreach ($mc[0] as $v) {
               $specialCharacters[$v] = "&#".ord($v).";";
           }
        }
        foreach ($specialCharacters as $k => $v) {
            $fn = preg_replace("/".preg_quote($k, '/')."/im", $v, $fn);
        }
        return $fn;
    }
    /**
     * scan file with dir
     * @param  [type] $path path
     * @param  [type] $callback loop function
     * @return [type]        [description]
     */
    static function scandir($path, $callback=null){
        if(!file_exists($path)){return null;}
        $files = scandir($path);
        if($callback == null){
            $callback = function($e){return $e;};
        }else if( is_string($callback) ){
            $re = $callback;
            if(!preg_match("/^\/.*\/(im)?$/im", $re)){
                $re = "/".preg_quote($re, '/')."/";
            }
            $callback = function($e)use($re){
                if($e =='.' || $e =='..') return null;
                if(!preg_match($re, $e)){return null;}
                return $e;
            };
        }
        foreach ($files as $k=>$v){
            if($callback){
                $files[$k] = $callback->call(new self, $v, $k);
            }
        }
        return $files; 
    }
    static function Attributes($filename=null){
        if(!$filename)return null;
        // $filename="/var/www/39doo/wwwroot/public/downloadfiles/1.jpgs";
        $path = preg_replace("/^(.*)\\".DIRECTORY_SEPARATOR.".*$/im", "$1", $filename);
        $suffix = preg_replace("/^.*\.(.*)$/im", "$1", $filename);
        $name = preg_replace("/^(.*)\\".DIRECTORY_SEPARATOR."(.*)$/im", "$2", $filename);
        $ctime = $atime = $mtime = $size = 0;
        $type = "";
        if(file_exists($filename)){
            $ctime = filectime($filename);
            $atime = fileatime($filename);
            $mtime = filemtime($filename);
            $type = filetype($filename);
            $size = filesize($filename) ;           
        }
        return array(
            "filename" => $filename
            , "path" => $path
            , "name" => $name
            , "suffix" => $suffix
            , "readable" => is_readable($filename)
            , "writable" => is_writable($filename)
            , "executable" => is_executable($filename)

            , "ctime" => $ctime
            , "atime" => $atime
            , "mtime" => $mtime
            , "type" => $type
            , "size" => $size
        );
    }    

    /**
     * make dir
     * @param  [type] $path 
     * @return [type]        [description]
     */   
    static function mkdir($path){
        $dirs = explode(DIRECTORY_SEPARATOR, $path);
        $dir = "";
        foreach ($dirs as $k => $v) {
            $dir .= $v.DIRECTORY_SEPARATOR;
            if( !file_exists($dir) ){
                try {
                    mkdir($dir);
                } catch (\Exception $e) {
                    return ['errcode'=>10008, 'data'=>"Err:\"$path\"\t".$e->getMessage()];
                }
            }
        }
        return ['errcode'=>0, 'data'=>"ok"];
    }
    /**
     * remove qualifier with path
     * @param  [type] $filename  
     * @return [type]        [description]
     */    
    static function removeQualifier($filename){
        if(!$filename) return $filename;
        $filename = preg_replace("/(([\\".DIRECTORY_SEPARATOR."]{2,})|(\\".DIRECTORY_SEPARATOR."\.\\".DIRECTORY_SEPARATOR."))/im", DIRECTORY_SEPARATOR, $filename);
        $re = "/\\".DIRECTORY_SEPARATOR."?[^\\".DIRECTORY_SEPARATOR."]+\\".DIRECTORY_SEPARATOR."\.\.\\".DIRECTORY_SEPARATOR."/im";  //  
        $fn = $filename;
        while ( preg_match($re, $fn) ) {
            $fn = preg_replace($re, DIRECTORY_SEPARATOR, $fn);
        }
        return $fn;
    }
    /**
     * get absolute filename
     * @param  [type] $filename  
     * @return [type]        [description]
     */    
    static function getAbsolute($filename, $basepath=null){
        if(!$filename) return $filename;
        $filename = preg_replace("/[\\\\\/]/im", DIRECTORY_SEPARATOR, $filename);
        $documentRoot = $basepath?$basepath:$_SERVER['DOCUMENT_ROOT'];
        if( preg_match("/^\w+\\".DIRECTORY_SEPARATOR."/im", $filename) ){ // xx/xxxxxx/xx.txt
            return self::getAbsolute(".".DIRECTORY_SEPARATOR.$filename);
        }else if( preg_match("/^[^\\".DIRECTORY_SEPARATOR."]*$/im", $filename) ){ // xx.txt
            return self::getAbsolute(".".DIRECTORY_SEPARATOR.$filename);
        }else if( preg_match("/^\\".DIRECTORY_SEPARATOR."/im", $filename) ){ // /var/xxxxxx
            return self::removeQualifier($filename);
        }else if(preg_match("/^\w+\:\\".DIRECTORY_SEPARATOR.".*/im", $filename)){ // c:\xxxxxxx
            return self::removeQualifier($filename);
        }else if(preg_match("/^\.\\".DIRECTORY_SEPARATOR."(.*)/im", $filename, $m)){ // ./ 
            return self::removeQualifier($documentRoot.DIRECTORY_SEPARATOR.$m[1]);
        }else if(preg_match_all("/^\.\.\\".DIRECTORY_SEPARATOR."(.*)/im", $filename)){ // ../ 
            $filename = $documentRoot.DIRECTORY_SEPARATOR.$filename;
            $ret = preg_match_all("/\.\.\\".DIRECTORY_SEPARATOR."/im", $filename, $mc);
            $re = "/[^(\.\.\\".DIRECTORY_SEPARATOR.")]+\\".DIRECTORY_SEPARATOR."\.\.\\".DIRECTORY_SEPARATOR."/im";
            while ($ret--) {
                $filename = preg_replace($re, "", $filename);
            }
        }
        return self::removeQualifier($filename);
    }
    static function file_exists($file){
        if(!$file) return $file;
        if(file_exists($file)){ return $file; }
        $nFile = self::getAbsolute($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.$file);
        if(file_exists($nFile)){
            return $nFile;
        }
        $nFile = self::getAbsolute(__DIR__.DIRECTORY_SEPARATOR.$file);
        if(file_exists($nFile)){
            return $nFile;
        }
        return null;
    }
    public function exists(){
        return file_exists($this->filename);
    }
    public function getFileName(){
        return $this->filename;
    }
    /**
     * write data to file with empty.
     * @param  [type] $value [description]
     * @return [type]        [description]
     */
    public function write($value){
        if(!$this->exists()){
            $ret = $this->createFile();
            if($ret['errcode'] != 0){ return $ret; }
        }
        $this->push($value);
        return $this->save();
    }
    /**
     * write data to file with empty.
     * @param  [type] $value [description]
     * @return [type]        [description]
     */
    public function append($value){
        if(!$this->exists()){
            $ret = $this->createFile();
            if($ret['errcode'] != 0){ return $ret; }
        }
        $hdl = $this->getAppendHandle();
        if($hdl == null){ return ['errcode'=>10001, 'data'=>$this->errcode['10001']]; }
        if(!is_writable($this->filename) ){$this->close(); return ['errcode'=>10003, 'data'=>$this->errcode['10003']];}
        fwrite($hdl, $value);
        $this->close();
        return ['errcode'=>0, 'data'=>$this->errcode['0']];
    }
    /**
     * read content with file
     * @param  integer $length content length
     * @param  integer $offset point's offset by start
     * @return [type]          [description]
     */
    public function read($length=0, $offset=0){
        if(!$this->exists()){
            return ['errcode'=>10001, 'data'=>$this->errcode['10001']];
        }
        $hdl = $this->getReadHandle();
        if($hdl == null){ return ['errcode'=>10001, 'data'=>$this->errcode['10001']]; }
        $fsize = filesize($this->filename);
        $length = ($length==0)?$fsize:$length;
        $data = "";
        $ret = fseek($hdl, $offset);
        if($ret == 0){
            if($fsize != 0){
                $data = fread($hdl, $length);
            }
        }
        $this->close();
        return ['errcode'=>0, 'data'=>$data];
    }
    /**
     * delete file
     * @return [type] [description]
     */
    public function delete(){
        if($this->exists()){
            if(unlink($this->filename)){
                // $this->filename = null;
                return ['errcode'=>0, 'data'=>$this->errcode['0']];
            }else{
                return ['errcode'=>10006, 'data'=>$this->errcode['10006']];
            }
        }
        return ['errcode'=>10006, 'data'=>$this->errcode['10006']];
    }


    public function push($value){
        array_push($this->data, $value);
        return $this;
    }
    /**
     * save cache to file
     * @return [type] [description]
     */
    public function save($data=null){
        if($data){ $this->data=[]; $this->push($data); }
        if(!$this->exists()){
            $ret = $this->createFile();
            if($ret['errcode'] != 0){ return $ret; }
        }
        if(count($this->data) == 0){ return ['errcode'=>10005, 'data'=>$this->errcode['10005']]; }
        $hdl = $this->getWriteHandle();
        if($hdl == null){ return ['errcode'=>10001, 'data'=>$this->errcode['10001']]; }
        // if(!strstr($this->mode, 'w') ){$this->close($hdl); return ['errcode'=>10002, 'data'=>$this->errcode['10002']];}
        if(!is_writable($this->filename) ){$this->close(); return ['errcode'=>10003, 'data'=>$this->errcode['10003']];}
        foreach ($this->data as $k=>$v) {
            fwrite($hdl, $v);
            unset($this->data[$k]);
        }
        $this->close();
        return ['errcode'=>0, 'data'=>$this->errcode['0']];
    }
    private function createFile(){
        if(!$this->filename){ return ['errcode'=>10007, 'data'=>$this->errcode['10007']]; }
        $attr = $this->getAttributes();
        $ret = self::mkdir($attr['path']);
        if($ret['errcode']!=0){ return $ret;}
        $f = fopen($this->filename, "w+");
        if($f){
            fclose($f);
            return ['errcode'=>0, 'data'=>$this->errcode['0']];
        }
        return ['errcode'=>10004, 'data'=>$this->errcode['10004']];
    }
    private function getReadHandle(){
        if(!$this->filename){ return null;}
        return fopen($this->filename, "rb");
    }
    private function getAppendHandle(){
        if(!$this->filename){ return null;}
        return fopen($this->filename, "ab");
    }
    private function getWriteHandle(){
        if(!$this->filename){ return null;}
        return fopen($this->filename, "wb");
    }
    /**
     * close handle and destroy
     * @param  [type] $handle [description]
     * @return [type]         [description]
     */
    private function close()
    {
        if($this->handle){
            fclose($this->handle);
            $this->handle = null;
        }
    }
    function __destory()
    {
        $this->close();
    }


}