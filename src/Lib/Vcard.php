<?php
namespace Etsoftware\Lib;

use Etsoftware\Lib\Xml;

class Vcard
{
    private $charset = "utf-8";
    // 新增加的类型
    const TYPE_FN = 0;
    const TYPE_N = 1;
    const TYPE_NICKNAME = 2;
    const TYPE_PHOTO = 3;
    const TYPE_BDAY = 4;
    const TYPE_ADR = 5;
    const TYPE_LABEL = 6;
    const TYPE_TEL = 7;
    const TYPE_EMAIL = 8;
    const TYPE_MAILER = 9;
    const TYPE_TZ = 10;
    const TYPE_GEO = 11;
    const TYPE_TITLE = 12;
    const TYPE_ROLE = 13;
    const TYPE_LOGO = 14;
    const TYPE_AGENT = 15;
    const TYPE_ORG = 16;
    const TYPE_CATEGORIES = 17;
    const TYPE_NOTE = 18;
    const TYPE_PRODID = 19;
    const TYPE_REV = 20;
    const TYPE_SORT = 21;
    const TYPE_STRING = 22;
    const TYPE_SOUND = 23;
    const TYPE_URL = 24;
    const TYPE_UID = 25;
    const TYPE_VERSION = 26;
    const TYPE_CLASS = 27;
    const TYPE_KEY = 28;

    //预定义的类型
    const PREDEFINED_SOURCE = 0;
    const PREDEFINED_NAME = 1;
    const PREDEFINED_PROFILE = 2;
    const PREDEFINED_BEGIN = 3;
    const PREDEFINED_END = 4;

    // 新增加的值类型
    const DATA_BINARY = 0;
    const DATA_PHONE_NUMBER = 1;
    const DATA_UTC_OFFSET = 2;
    const DATA_AND = 3;
    const DATA_VCARD = 4;
    const DATA_VALUE = 5;

    private $data=[];
    
	function __construct($data = null)
    {
        if($data){
            $ret = $this->decode($data);
            if($ret){ $this->data = $ret; }
        }else{
            $this->add("VERSION", null, "2.1");
        }
    }
    public function toXml(){
        return Xml::toString($this->toArray(), "VCARD");
    }
    public function toArray(){
        $data = [];
        foreach ($this->data as $k => $vd) {
            $key = $vd->type;
            $arg = ""; foreach ($vd->args as $v) {$arg .= ";$v;"; }
            if(preg_match_all("/;([^\=\;\:]+);/im", $arg, $mc, PREG_SET_ORDER)){
                if(!isset($data[$key])){ $data[$key] = []; }
                $b=&$data[$key];
                foreach ($mc as $k => $v) {
                    if(!isset($b[$v[1]])){
                        $b[$v[1]] = [];
                    }
                    $b = &$b[$v[1]];
                }
                // $b = $vd->getValues();
                $b = array_merge($b, $vd->getValues());
            }else{
                $d = [$key => $vd->getValues()];
                $data = array_merge($data, $d);
            }
        }
        return $data;
    }
    public function toString(){ return $this->encode(); }
    /**
     * 添加数据
     * @param  [type] $type  类型
     * @param  [type] $args  参数
     * @param  [type] $value 值
     * @return [type]        [description]
     */
    public function add($type, $args, $value){
        $data = new Class($type, $args, $value){
            public $type = '';
            public $args = [];
            public $values = [];
            public function __construct($type, $args, $value){
                $this->type = $type;
                $this->args = $args?is_array($args)?$args:[$args]:[];
                $this->values = $value?is_array($value)?$value:[$value]:[];
                $arg = "";
                foreach ($this->args as $v) {$arg .= ";$v"; }
                if(preg_match_all("/;?([^=]+)=([^;]*)/", $arg, $mc, PREG_SET_ORDER)){
                    foreach ($mc as $v) {
                        $key = strtolower($v[1]);
                        $this->$key = $v[2];
                    }
                }
            }
            public function getValues(){
                $data = $this->values;
                if(property_exists($this, 'encoding')){
                    if(strnatcasecmp($this->encoding ,"QUOTED-PRINTABLE") == 0){
                        foreach ($data as $k => $v) {
                            $data[$k] = quoted_printable_decode($v);
                        }
                    }
                }
                return $data;
            }
        };
        array_push($this->data, $data);
        // array_push($this->data, ['type' => $type, 'args' => $args, 'value' => $value ]);
        return $this;
    }
    private function encode(){
        $vcard = "BEGIN:VCARD\n";
        foreach ($this->data as $v) {
            $vcard .= $this->dataEncode($v->type, $v->args, $v->values)."\n";
        }
        $vcard .= "END:VCARD";
        return $vcard;
    }
    private function decode($content){
        $vcard = explode("\n", $content);
        $data = [];
        foreach ($vcard as $v) {
            if(preg_match("/^\s*(((BEGIN)|(END))\s*:\s*VCARD)?\s*$/im", $v))continue;
            $d = $this->dataDecode($v);
            $this->add($d['type'], $d['args'], $d['value']);
        }
        return $data;
    }
    /**
     * 数据格式
     * @param  [type] $type  类型
     * @param  [type] $args  参数
     * @param  [type] $value 值
     * @return [type]        [description]
     */
    private function dataEncode($type, $args, $value){
        $val = strtoupper($type);
        if(is_array($args)){
            foreach ($args as $v) {$val .= ";$v"; }
        }else if($args){
            $val .= ";$args";
        }
        $val .= ":";
        if(is_array($value)){
            foreach ($value as $v) {$val .= "$v;"; }
            $val = substr($val, 0, strlen($val)-1);
        }else if($value){
            $val .= "$value";
        }
        return $val;
    }
    private function dataDecode($data){
        $ret = preg_match("/([^;]+)([^:]*):(.+)/im", $data, $m);
        if(!$ret)return null;
        $type = $m[1];
        $args = [];
        $val = [];
        if(preg_match_all("/;([^;]*)/im", $m[2], $mc, PREG_SET_ORDER)){
            foreach ($mc as $k => $v) {
                array_push($args, $v[1]);
            }
        }
        if(preg_match_all("/([^;]*);?/im", $m[3], $mc, PREG_SET_ORDER)){
            foreach ($mc as $k => $v) {
                if($v[0] == "")continue;
                array_push($val, $v[1]);
            }
        }
        return ['type' => strtoupper($type), 'args' => $args, 'value' => $val ];
    }



}