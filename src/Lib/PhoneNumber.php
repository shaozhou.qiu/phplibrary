<?php
namespace Etsoftware\Lib;
/**
 * 电话号码识别
 */
// Identity card
class PhoneNumber
{
    private $data=array(
        array(13, 0, 2, '联通')
        , array(13, 3, 3, '电信')
        , array(13, 4, 9, '移动')
        , array(14, 0, 0, '联通/物联网')
        , array(14, 1, 1, '电信/物联网')
        , array(14, 2, 3, '物联网')
        , array(14, 4, 4, '移动/物联网')
        , array(14, 5, 6, '联通/物联网')
        , array(14, 7, 8, '移动/物联网')
        , array(14, 9, 9, '电信/物联网')
        , array(15, 0, 2, '移动')
        , array(15, 3, 3, '电信')
        , array(15, 4, 4, '-')
        , array(15, 5, 6, '联通')
        , array(15, 7, 9, '移动')
        , array(16, 0, 1, '-')
        , array(16, 2, 2, '电信/虚拟')
        , array(16, 3, 4, '-')
        , array(16, 5, 5, '移动/虚拟')
        , array(16, 6, 6, '联通')
        , array(16, 7, 7, '联通/虚拟')
        , array(16, 8, 9, '-')
        , array(17, 0, 0, '虚拟')
        , array(17, 1, 1, '联通/虚拟')
        , array(17, 2, 2, '移动')
        , array(17, 3, 3, '电信')
        , array(17, 4, 4, '电信/卫星')
        , array(17, 5, 6, '联通')
        , array(17, 7, 7, '电信')
        , array(17, 8, 8, '移动')
        , array(17, 9, 9, '电信')
        , array(18, 0, 1, '电信')
        , array(18, 2, 4, '移动')
        , array(18, 5, 6, '联通')
        , array(18, 7, 8, '移动')
        , array(18, 9, 9, '电信')
        , array(19, 0, 1, '电信')
        , array(19, 2, 2, '广电')
        , array(19, 3, 3, '电信')
        , array(19, 4, 4, '-')
        , array(19, 5, 5, '移动')
        , array(19, 6, 6, '联通')
        , array(19, 7, 8, '移动')
        , array(19, 9, 9, '电信')

        , array(172, 4, 4, '移动/物联')
        , array(178, 9, 9, '移动/物联')
        , array(184, 9, 9, '移动/物联')
        , array(134, 9, 9, '电信/卫星')
        , array(174, 9, 9, '交通/卫星')

        , array(1740, 0, 4, '电信/卫星')
        , array(1740, 6, 12, '工信/卫星')

        , array(1064, 0, 0, '广电/物联网')
        , array(1064, 1, 1, '联通/物联网')
        , array(1064, 6, 6, '联通/物联网')
        , array(1064, 7, 8, '移动/物联网')
        , array(1064, 9, 9, '电信/物联网')

    );
    function __construct()
    {
    }
    private function getCarrier($num){
        $code = substr($num, 0, 5);
        for($i=5; $i>2; $i--){
            $code = substr($code, 0, $i);
            $prefix = substr($code, 0, $i-1);
            foreach($this->data as $k=>$v){
                if($prefix != $v[0])continue;
                for($n=$v[1];$n <= $v[2]; $n++){
                    if($code == "$prefix$n"){
                        return $v[3];
                    }
                }
            }
        }
        return "";
    }
    public function get($num){
        $num = preg_replace("/\s+/im", "", $num);
        if(preg_match("/.*(\d{11})$/im", $num, $m))
            $id = $m[1];
        else
            $id = $num;        
        return array(
            'phonenumber'=>$num
            , 'phonenumber_provider'=>$this->getCarrier($id)
        );
    }
}