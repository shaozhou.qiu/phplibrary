<?php
namespace Etsoftware\Lib;

use Etsoftware\Lib\Image;

class DataUriScheme {
	const TYPE_URL = 1;
	const TYPE_BASE64 = 2;
	const TYPE_JSON = 4;
	const TYPE_SESSION = 8;
	const TYPE_HTML = 16;
	const TYPE_UUENCODE = 32;
	private $type = 0;
	
	public function __construct(){
		
	}
	public function get($data){
		return "data:".$this->getProtocol($data).";".$this->getTypeName($this->type).",".$this->encode($this->type, $data);
	}
	// 声明数据协议及类型名称
	private function getProtocol($data){
		
		return Image::getType($data);
	}
	private function getTypeName($t){
		switch ($t) {
			case DataUriScheme::TYPE_URL:
				return "url";
				break;
			case DataUriScheme::TYPE_BASE64:
				return "base64";
				break;
			case DataUriScheme::TYPE_JSON:
				return "json";
				break;
			case DataUriScheme::TYPE_SESSION:
				return "session";
				break;
			case DataUriScheme::TYPE_HTML:
				return "html";
				break;
			case DataUriScheme::TYPE_UUENCODE:
				return "uuencode";
				break;
			default:
				return "base64";
				break;
		}

	}
	private function encode($t, $data){
		switch ($t) {
			case DataUriScheme::TYPE_URL:
				return urlencode($data);
				break;
			case DataUriScheme::TYPE_BASE64:
				return base64_encode($data);
				break;
			case DataUriScheme::TYPE_JSON:
				return json_encode($data);
				break;
			case DataUriScheme::TYPE_SESSION:
				return session_encode($data);
				break;
			case DataUriScheme::TYPE_HTML:
				return htmlspecialchars($data);
				break;
			case DataUriScheme::TYPE_UUENCODE:
				return convert_uuencode($data);
				break;
			default:
				break;
		}
		return base64_encode($data);
	}
}