<?php
namespace Etsoftware\Lib;


interface OauthInterface 
{
	/**
     * get access token
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function getToken($data=null);
    /**
     * refresh access token
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function refreshToken($token);


}