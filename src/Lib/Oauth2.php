<?php
namespace Etsoftware\Lib;
use Etsoftware\Lib\File;
use Etsoftware\Lib\Memory;


class Oauth2 
{
    private $obj = null;
    protected $expires_in = 7200;
    public function __construct($key="", $type="file"){
        $this->obj = $this->getObject($type, $key);
    }
	/**
     * get access token
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function getToken($data=null){
        $appid = hash("sha1", json_encode($data, true));
        //$appid, $secret
        $tokens = $this->obj->get();
        if(!$tokens){ $tokens = []; }
        $token = null;
        foreach ($tokens as $key => $v) {
            if($key != $appid)continue;
            $token = $v;
            break;
        }
        if($token == null){
            $tokens[ $appid ] =  $this->createdToken($appid);
            $this->obj->set(json_encode($tokens, true));
        }else{
            if(!$this->validateToken($token['access_token'])){
                unset($tokens[$appid]);
                $this->obj->set(json_encode($tokens, true));
                return $this->getToken($data);
            }
        }
        unset($token['create_at']);
        return $token;
    }
    /**
     * validate access Token
     * @param  [type] $token [description]
     * @return [type]        [description]
     */
    public function validateToken($token){
        if(!$token) return false;
        $data = $this->obj->get();
        if(!$data)return false;
        foreach ($data as $key => $v) {
            if($v['access_token'] != $token)continue;
            if(time()-$v['create_at'] > $this->expires_in){
                unset($data[$key]);
                $this->obj->set(json_encode($data, true));
                return false;
            }
            return true;
        }
        return false;
    }
    /**
     * refresh access token
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function refreshToken($token){
        if(!$token) return false;
        $data = $this->obj->get();
        $isMatch = false;
        foreach ($data as $key => $v) {
            if($v['refresh_token'] != $token)continue;
            if( !$this->validateToken($v['access_token']) ){
                return false;
            }
            $data[$key]['create_at'] = time();
            $isMatch = true;
            break;
        }
        if(!$isMatch){ return false; }
        $this->obj->set(json_encode($data, true));
        return true;
    }

    public function __destruct(){

    }
    /**
     * created new access token
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    protected function createdToken($data=null){
        $token = hash("sha1", $data.time());
        return [
            'expires_in' => $this->expires_in
            ,'access_token' => $token
            ,'refresh_token' => hash('sha1', $token.$this->expires_in)
            ,'create_at' => time()
        ];
    }    
    /**
     * get save object
     * @return [type] [description]
     */
    private function getObject($type, $key){
        $key = sprintf('%05x',intval(sprintf('%u', crc32($key)))%512 );
        if($type == "shmop"){
            // $shmid = @shmop_open(0x0000076e, 'a', 0, 0);
            // shmop_delete($shmid); die;
            return new Class($key?$key:__CLASS__){
                private $key=0;
                public function __construct($key){
                    if(preg_match_all("/\w/", $key, $mc)){
                        foreach ($mc[0] as $n=>$m) {
                            $this->key += ord($m);
                            $this->key <<= $n;
                        }
                    }
                }
                public function get(){
                    $obj = new Memory($this->key);
                    $ret = $obj->read();
                    if($ret){
                        return json_decode($ret, true);
                    }
                    return null;
                }
                public function set($val){
                    $obj = new Memory($this->key);
                    $obj->write($val);
                }
            };
        }else {
            return new Class($key?$key:__CLASS__){
                private $filename="./auth2";
                public function __construct($key){
                    $fn = File::quote($key);
                    $this->filename .= "/$fn.ot2";
                }
                public function get(){
                    $obj = new File($this->filename);
                    $ret = $obj->read();
                    if($ret['errcode'] == 0){
                        return json_decode($ret['data'], true);
                    }
                    return null;
                }
                public function set($val){
                    $obj = new File($this->filename);
                    $obj->write($val);
                }
            };
        }

    }
}