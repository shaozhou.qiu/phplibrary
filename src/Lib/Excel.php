<?php
namespace Etsoftware\Lib;

use Etsoftware\Lib\File;

class Excel 
{
    /**
     * [
     *     // sheet 1
     *     [
     *         ['title'=>'',......]
     *         ['title'=>'',......]
     *     ]
     *     // sheet 2
     *     [
     *         ['title'=>'',......]
     *         ['title'=>'',......]
     *     ]
     * ]
     * @var array
     */
    private $data=[];
	function __construct($filename=null)
    {
        
    }
    /**
     * 创建表
     * @param  [type] $title [description]
     * @return [type]        [description]
     */
    public function newSheet($data=[], $title=null){
        if(!$title){ $title = "sheet ".count($this->data); }
        $this->data[$title] = $data;
    }
    private function array2csv($data, $cellsplit="\t", $rowsplit="\r"){
        $reval = '';
        foreach ($data as $idx => $row) {
            foreach ($row as $k => $v) {
                $v = preg_replace("/\"/im", "\"\"", $v);
                $reval .= "\"$v\"$cellsplit"; 
            }
            $reval .= "$rowsplit";
        }
        // dump($reval);die;
        // dump($data);die;
        return $reval;
    }
    /**
     * data to csv format
     * @return [type] [description]
     */
    public function toCSV(){
        $reval = '';
        foreach ($this->data as $k => $sheet) {
            $reval = $this->array2csv($sheet);
            break;
        }
        return $reval;
    }
    public function saveCSV($title=null){
        $content = $this->toCSV();
        if($title){
            $f = new File($title);
            $f->write($content);
        }else{
            $fn =$this->getTempFileName($content);
            header("Content-type:application/vnd.ms-excel");  
            header("Content-Disposition:filename=".$fn.".csv");
            exit($content);            
        }
        return $this;
    }
    private function getTempFileName($str){
        $fn = '';
        if($str){
            $fn = preg_replace("/^([^\r\n]+)[\w\W]*$/im", "$1", $str);
            $fn = preg_replace("/[^a-zA-Z\d\x{4e00}-\x{9fa5}]/uim", "", $fn);
        }
        if(!$fn){
            $fn='';
            for ($i=0; $i < 6; $i++) { 
                $fn .= chr(rand(48, 57));
                $fn .= chr(rand(65, 90));
                $fn .= chr(rand(97, 122));
            }
        }
        return $fn;
    }
    /**
     * translate number to a-z
     * @param  [type] $n [description]
     * @return [type]    [description]
     */
    static function int2Az($n){
        $A = 65; $Z = 90;
        $stp = $Z-$A;
        if( $n > $stp ){
            $h = intval($n/$stp);
            $l = intval($n%$stp) - $h;
            $reval = self::int2Az($h-1);
            $reval .= self::int2Az($l);
            return $reval;
        }
        return chr(65+$n);
    }
    /**
     * translate number to a-z
     * @param  [type] $n [description]
     * @return [type]    [description]
     */
    static function az2Int($n){
        $n = strtoupper($n);
        if( strlen($n)>1 ){
            preg_match_all("/[a-z]/im", $n, $mc, PREG_SET_ORDER);
            $reval = 0;
            foreach ($mc as $k => $v) {
                if($k>0){
                    $reval += 1;
                    $reval *= 26;
                }
                $reval += self::az2Int($v[0]);
            }
            return $reval;
        }
        return ord($n) - ord('A');
    }

}