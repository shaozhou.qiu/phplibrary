<?php
namespace Etsoftware\RemoteDeskTop;
use Etsoftware\RemoteDeskTop\RdpInterface;
use Etsoftware\RemoteDeskTop\Rdp;

class Mstsc extends Rdp implements RdpInterface
{
	public function setFullScreen($v){
		$this->deleteParams('f');
		if($v) $this->addParams('f');
		return $this;
	}
	public function setWidth($n){if($n>0)$this->chgParams("w", $n); }
	public function setHeight($n){if($n>0)$this->chgParams("h", $n); }
	public function download(){
		$cmd = $this->encryptCmd($this->toCommand());
		$this->output($cmd, $this->fileName.".bat");
	}
	public function toCommand(){
		$str = "@Echo off\n";
		$str .= "title ".($this->fileName?$this->fileName:$this->server)."\n";
		$str .= "mode con cols=50 lines=2\n";

		$str .= "set ROOT=%HOMEDRIVE%%HOMEPATH%\n";
		$str .= "set DOCPATH=%ROOT%\Documents\n";
		$str .= "set DEFRDP=%DOCPATH%\Default.rdp\n";
		$str .= "del /a:H %DEFRDP%\n";

		$str .= "Set SERVER=".$this->server."\n";
		$str .= "Set HOST=%SERVER%".($this->port!=3389?":".$this->port:'')."\n";
		$str .= "Set USERNAME=".$this->user."\n";
		$str .= "Set PASSWORD=".$this->password."\n";
		$str .= "Cmdkey /delete:TERMSRV/%SERVER%\n";
		$str .= "Cmdkey /add:TERMSRV/%SERVER% /user:%USERNAME% /pass:%PASSWORD%\n";

		$str .= "Start /wait mstsc /v:%HOST% ".$this->getParams()."\n";
		
		$str .= "Cmdkey /delete:TERMSRV/%SERVER%\n";
		return $str;
	}
}