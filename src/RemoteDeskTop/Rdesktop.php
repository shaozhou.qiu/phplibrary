<?php
namespace Etsoftware\RemoteDeskTop;
use Etsoftware\RemoteDeskTop\RdpInterface;
use Etsoftware\RemoteDeskTop\Rdp;

class Rdesktop extends Rdp implements RdpInterface
{
	function __construct(){
		$this->addParams("r", "disk:x=\${HOME}");
		$this->addParams("r", "disk:y=/media");
		$this->addParams("r", "disk:z=/mnt");
	}	
	public function setFullScreen($v){
		$this->deleteParams('f');
		if($v) $this->addParams('f');
		return $this;
	}
	private function setG($w, $h){
		foreach ($this->params as $k => $v) {
			if($v['key'] = "g"){
				if(preg_match("/(\d+)[\*\x](\d+)/im", $v, $m, PREG_SET_ORDER)){
					if($w==0) $w = $m[1];
					if($h==0) $h = $m[2];
				}
			}
		}
		
		
	}
	public function setWidth($n){$this->setG($n, 0);}
	public function setHeight($n){$this->setG(0, $n);}
	public function download(){
		$cmd = $this->encryptCmd($this->toCommand());
		$this->output($cmd, $this->fileName.".sh");
	}
	public function toCommand(){
		$parmas = " -u " . $this->user;
		$parmas .= " -p " . $this->password;
		$parmas .= $this->getParams(" ", "-");
		return "rdesktop ".$this->server.":".$this->port.$parmas;
	}
}