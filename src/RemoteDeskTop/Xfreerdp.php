<?php
namespace Etsoftware\RemoteDeskTop;
use Etsoftware\RemoteDeskTop\RdpInterface;
use Etsoftware\RemoteDeskTop\Rdp;

class Xfreerdp extends Rdp implements RdpInterface
{
	public CONST SEC_NLA = "-sec-nla";
	function __construct(){
		$this->addParams("drive", "home,\${HOME}");
		$this->addParams("drive", "media,/media");
		$this->addParams("drive", "mnt,/mnt");
		$this->addParams("cert", "tofu");
		$this->setSec(Xfreerdp::SEC_NLA);
	}
	public function setFullScreen($v){
		$this->deleteParams('f');
		if($v) $this->addParams('f');
		return $this;
	}
	public function setWidth($n){if($n>0)$this->chgParams("w", $n); }
	public function setHeight($n){if($n>0)$this->chgParams("h", $n); }
	public function setSec($v){
		if($v){
			$this->chgParams("sec", $v);
		}else{
			$this->deleteParams("sec");
		}
	}
	public function download(){
		$cmd = $this->encryptCmd($this->toCommand());
		$this->output($cmd, $this->fileName.".sh");
	}
	public function toCommand(){
		$parmas = " /u:" . $this->user;
		$parmas .= " /p:" . $this->password;
		$parmas .= $this->getParams(":");
		return "xfreerdp /v:".$this->server.":".$this->port.$parmas;
	}
}