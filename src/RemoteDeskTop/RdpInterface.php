<?php
namespace Etsoftware\RemoteDeskTop;


interface RdpInterface{
    // const HOST = "39doo.com";
    // const PORT = 3389;
    public function setFileName($fileName);
	public function setFullScreen($v);
	public function setWidth($n);
	public function setHeight($n);
	public function setHost($host);
	public function setPort($port);
	public function setUser($user);
	public function setPassword($password);
	public function download();
	public function toCommand();

}