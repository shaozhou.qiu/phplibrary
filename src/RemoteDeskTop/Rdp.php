<?php
namespace Etsoftware\RemoteDeskTop;

class Rdp{
	protected $fileName = null;
	protected $server = "127.0.0.1";
	protected $port = 3389;
	protected $user = "administrator";
	protected $password = "";
	protected $params = [];

	public function setHost($host){
		$this->server = $host;
		if(!$this->fileName)$this->fileName=$host;
		return $this;
	}
	public function setPort($port){
		$this->port = $port;
		return $this;
	}
	public function setUser($user){
		$this->user = $user;
		return $this;
	}
	public function setPassword($password){
		$this->password = $password;
		return $this;
	}
	public function setFileName($fileName){
		$this->fileName = $fileName;
	}
	protected function getParams($spcSympol=null, $sync=null){
		$spcSympol = ($spcSympol==null)?":":$spcSympol;
		$sync = ($sync==null)?"/":$sync;
		$str = "";
		foreach ($this->params as $k => $v) {
			$str .= " $sync". $v['key'];
			if($v['value']) $str .= $spcSympol. $v['value'];
		}
		return $str;
	}
	protected function chgParams($k, $v){
		$this->deleteParams($k);
		$this->addParams($k, $v);
	}
	protected function addParams($k, $v=null){
		array_push($this->params, ['key'=>$k, "value"=>$v]);
	}
	protected function deleteParams($key){
		foreach ($this->params as $k => $v) {
			if($v['key'] == $key){
				unset($this->params[$k]);
				return true;
			}
		}
		return false;
	}
	protected function encryptCmd($data){
		return chr(0xff).chr(0xfe)."\n\n$data";
	}
	protected function output($data, $fn=null){
		$fn = (null==$fn)?$this->fileName:$fn;
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control:must-revalidate, post-check=0, pre-check=0");
		header("Content-Type:application/force-download");
		header("Content-Type:application/vnd.ms-execl");
		header("Content-Type:application/octet-stream");
		header("Content-Type:application/download");;
		header('Content-Disposition:attachment;filename="'.$fn.'"');
		header("Content-Transfer-Encoding:binary");
		echo($data);die;
	}

}