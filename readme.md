EtSoftware php library

/*
*
*   扩展 1.0.0.1 
*   Release date: 2019-8-14
*   Author      : 半条虫(466814195)
*   Keywords    : Etsoftware 半条虫(466814195) rimke 39doo 39度
*   Description : 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*   Blog        : http://rimke.blog.163.com/        
*   Website     : http://www.39doo.com/
*   Mail        : rimke@163.com
*   Copyright   : Power By Etsoftware
*
*/

此组件需运行在lnmp环境中。

仅供学习不得用于商业用途

安装，
    通过git仓库安装

    第一步，修改根目录下的 composer.json
        -----------------------------------------------------------------------
        "repositories": [
            {
                "type": "git",
                "url": "https://gitlab.com/shaozhou.qiu/phplibrary.git",
                "reference":"master"
            }
        ],    
        "require": {
            "etsoftware/phplibrary": "*"
        },
        "http-basic":{   
            "https://gitlab.com/shaozhou.qiu/phplibrary.git":{   
                "username":"",   
                "password":""   
            }
        }    
        ----------------------------------------------------------------------- 
    第二步，更新依赖
        composer update
        composer dump-autoload 

使用：

<?php
    #ini_set('display_errors', 1);
    require __DIR__.'/../vendor/autoload.php';
    use Etsoftware\Email\Smtp;
    $smtp = new Smtp(111);
?>

Etsoftware\Email\Imap;

    new Imap($host, $port, $user, $password, $email='', $servertype='pop', $ssl=false)



文件操作（res/file.md）

    Etsoftware\Lib\File
    $file = new File($filename);

RSA 加密与解密 (res/rsa.md)

Socket 网络操作(res/socket.md)

Proxy 代理插件(res/socket.md)

Template 模板 (res/template.md)  

Crontab 组件 (res/crontab.md)  

    $tpl = "{{$name}}  {{$sex}}";
    $data = ['name'=>'半条虫(466814195)'  'sex'=>0];
    $tpl = (new Template)->render($tpl, $data);
    dump($tpl);

Mstsc 远程桌面组件 (res/mstsc.md)  

    远程rdp文件读写工具

Ocr 图片识别组件 (res/ocr.md)
    识别图片中的字符