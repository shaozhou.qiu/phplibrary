EtSoftware php library

/*
*
*   扩展 1.0.0.1 
*   Release date: 2021-3-28
*   Author      : 半条虫(466814195)
*   Keywords    : Etsoftware 半条虫(466814195) rimke 39doo 39度
*   Description : 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*   Blog        : http://rimke.blog.163.com/        
*   Website     : http://www.39doo.com/
*   Mail        : rimke@163.com
*   Copyright   : Power By Etsoftware
*
*/


Etsoftware\Lib\Ocr
    $img = "http://www.39doo.com/xxxx.jpg";
    $orc = new Ocr($img);
    //设置字体
    $orc->setFont("/var/font/msyh.ttf");
    //获取图片中的字符
    $data = $orc->getString();
    //获取特征码
    $data = $orc->getHecData();
