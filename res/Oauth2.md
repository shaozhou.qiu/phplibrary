EtSoftware php library

/*
*
*   扩展 1.0.0.1 
*   Release date: 2019-8-14
*   Author      : 半条虫(466814195)
*   Keywords    : Etsoftware 半条虫(466814195) rimke 39doo 39度
*   Description : 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*   Blog        : http://rimke.blog.163.com/        
*   Website     : http://www.39doo.com/
*   Mail        : rimke@163.com
*   Copyright   : Power By Etsoftware
*
*/


Etsoftware\Lib\Oauth2
    $oauth = new Oauth2($key="", $type="file");
    // $key 标识
    // $type 存储类型， file; shmop

    $oauth->getToken($appid, $secret, $data=null);
    // appid 授权应用id
    // secret 授权密钥
    // data 附加信息
    // 获取token, 如果不存在则生成新的token

    $oauth->validateToken($token);
    // 验证token是否有效，返回为boolean类型

    $oauth->refreshToken($token);
    // 刷新token