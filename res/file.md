EtSoftware php library

/*
*
*   扩展 1.0.0.1 
*   Release date: 2019-8-14
*   Author      : 半条虫(466814195)
*   Keywords    : Etsoftware 半条虫(466814195) rimke 39doo 39度
*   Description : 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*   Blog        : http://rimke.blog.163.com/        
*   Website     : http://www.39doo.com/
*   Mail        : rimke@163.com
*   Copyright   : Power By Etsoftware
*
*/


Etsoftware\Lib\File
    $file = new File($filename);

    $filename : 文件完成目录。支持
                ../abc/readme.txt // 相对目录，当前目录上一级
                abc/readme.txt //相对目录，当前目录
                ./abc/readme.txt //相对目录，当前目录
                /var/log/readme.txt  // 绝对目录

    $f = new File("/var/log/readme.txt");
    $f = new File("readme.txt");
    $f = new File("./readme.txt");
    $f = new File("../../readme.txt");            

    $f->exists()        
    // 判断文件是否存在

    $f->getFileName()   
    // 获取文件完整路径 

    $f->write($value)         
    // 向文件写入数据，如果文件不存在则创建。

    $f->append($value)        
    // 向文件追加数据，如果文件不存在则创建。

    $f->read($length=0, $offset=0)          
    // 读取文件数据
    // $length 读取长度
    // $offset 开始位置偏移量

    $f->delete()        
    // 删除文件

    $f->push($msg)          
    // 将内容缓存到内存中，调用save()后，数据将会被写入磁盘。

    $f->save($data=null)          
    // 将内内存中数据写入磁盘。
    // 如果指定$data, 只会将$data保存到文件中

    File::mkdir($path) 
    //创建目录

    File::getAbsolute($path) 
    //获取真实地址

    File::Attributes($path) 
    //获取文件相关属性

    File::quote($content) 
    //转义非法字符


