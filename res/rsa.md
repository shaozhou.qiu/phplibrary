EtSoftware php library

/*
*
*   扩展 1.0.0.1 
*   Release date: 2019-8-14
*   Author      : 半条虫(466814195)
*   Keywords    : Etsoftware 半条虫(466814195) rimke 39doo 39度
*   Description : 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*   Blog        : http://rimke.blog.163.com/        
*   Website     : http://www.39doo.com/
*   Mail        : rimke@163.com
*   Copyright   : Power By Etsoftware
*
*/



RSA 加密与解密
    use Etsoftware\Encrypt\Rsa;

    $rsa = new Rsa;
    $keys = $rsa->getKey();

    // 客户端与服务端通信
        // 加密
        $str = $rsa->encryptByPrivate("client -> server", $keys['privkey']);
        dump($str);
        // 解密
        dump($rsa->decryptByPublic($str, $keys['pubkey']));



    // 服务端与客户端通信
        // 加密
        $str = $rsa->encryptByPublic("server -> client", $keys['pubkey']);
        dump($str);
        // 解密
        dump($rsa->decryptByPrivate($str, $keys['privkey']));


    前端可采
        来源：https://github.com/travist/jsencrypt
        <script src="bin/jsencrypt.min.js"> </script>
        <script >
             var rsa = new JSEncrypt()
            , publikKey = $('#hiptrsa').val();
            rsa.setPublicKey(publikKey);
            rsa.encrypt( val );
        </script>

