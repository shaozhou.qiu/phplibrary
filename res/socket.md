EtSoftware php library

/*
*
*   扩展 1.0.0.1 
*   Release date: 2019-8-14
*   Author      : 半条虫(466814195)
*   Keywords    : Etsoftware 半条虫(466814195) rimke 39doo 39度
*   Description : 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*   Blog        : http://rimke.blog.163.com/        
*   Website     : http://www.39doo.com/
*   Mail        : rimke@163.com
*   Copyright   : Power By Etsoftware
*
*/



Socket 网络操作(socket.md)

    use Etsoftware\Socket\Http;
    $http = new Http($useragent);
    $http->download($url, $filename, [$timeout=300, $speed=0]);
    // 下载文件
    $http->get($url, $data);
    // get方式获取
    $http->post($url, $data);
    // post方式获取
    $http->http($url, $params, $ispost, $https, $header, $speed);
    // http方式获取


    use Etsoftware\Socket\Qqwry;
    纯真IP信息查询
        $qqwry = new Qqwry();
        $info = $qqwry->getlocation($ipv4);
        if(!$info){
            $info = $qqwry->getlocationByOnline($ipv4);
        }       
    注：如果db目录上没有qqwry.dat文件，插件会通过网络获取ip信息，建议手动添加以提高效率

Proxy 代理插件

    use Etsoftware\Proxy\Ssr;
    $ssr = new Ssr();
    $ssr->load($content); // 加载ssr数据，可以是ssr://、订阅地址、或本地磁盘地址
    $ssr->getData(); // 获取数据
    $ssr->getMessages($content, $callback); // 解析消息为json

