EtSoftware php library

/*
*
*   Memory 扩展 1.0.0.1 
*   Release date: 2019-8-14
*   Author      : 半条虫(466814195)
*   Keywords    : Etsoftware 半条虫(466814195) rimke 39doo 39度
*   Description : 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*   Blog        : http://rimke.blog.163.com/        
*   Website     : http://www.39doo.com/
*   Mail        : rimke@163.com
*   Copyright   : Power By Etsoftware
*
*/

[Readme.md]   (readme.md)  

Cache 模块

    // $cache = new Cache(); // 缓存到文件中
    $cache = new Cache('shmop'); // 缓存到内存中
    // $cache->push('username', 'rimke');
    // $cache->push('sex', '男');
    // $cache->unset('sex'); // 删除set字段
    dump($cache->get('username')); // 获取username字段内容
    dump($cache->get('sex'));