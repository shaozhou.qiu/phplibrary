EtSoftware php library

/*
*
*   扩展 1.0.0.1 
*   Release date: 2019-8-14
*   Author      : 半条虫(466814195)
*   Keywords    : Etsoftware 半条虫(466814195) rimke 39doo 39度
*   Description : 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*   Blog        : http://rimke.blog.163.com/        
*   Website     : http://www.39doo.com/
*   Mail        : rimke@163.com
*   Copyright   : Power By Etsoftware
*
*/

[Readme.md]   (readme.md)  

Template 模板
    $tpl = new Template;

    function:
        setQualifier : function(['{', '}']) 设置修饰符
        getQualifier : function() 获取修饰符
        setLQualifier : function('{{') 设置左修饰符
        getLQualifier : function() 获取左修饰符
        setRQualifier : function('}}') 设置右修饰符
        getRQualifier : function() 获取右修饰符
        setData : function([key=>val]) 设置模板数据
        getData : function() 获取模板数据
        setEsc : function("@") 获取转义字符
        getEsc : function() 获取转义字符

实例eg:
    
    $tpl = "{{$name}}  {{$sex}}";
    $data = ['name'=>'半条虫(466814195)'  'sex'=>0];
    $tpl = (new Template)->render($tpl, $data);
    dump($tpl);