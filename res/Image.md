EtSoftware php library

/*
*
*   扩展 1.0.0.1 
*   Release date: 2019-8-14
*   Author      : 半条虫(466814195)
*   Keywords    : Etsoftware 半条虫(466814195) rimke 39doo 39度
*   Description : 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*   Blog        : http://rimke.blog.163.com/        
*   Website     : http://www.39doo.com/
*   Mail        : rimke@163.com
*   Copyright   : Power By Etsoftware
*
*/



Image 图片处理函数
    use Etsoftware\Lib\Image;

    // $img = new Image($width=200, $height=20, $color=0xffffff); 创建一个新的图片，指定宽，高，及背景色
    $img = new Image($filename); // 指定图片

    Image::thumb($src, $rate=0.8, $filename=null);
    // 缩略图，
    // $src 图片源地址
    // $rate 缩放比率
    // $filename 生成位置

    $img->getInformation() // 获取图片信息
    $img->zoomByWidth($width) // 根据宽度等比缩放
    $img->zoomByHeight($height) // 根据高度等比缩放
    $img->zoom($rate) // 根据比率等比缩放
    $img->loadImage($filename) // 读取图片
    $img->getImage() // 获取生成结果图片
    $img->toImages($fileName=null, $imgType=null) // 根据类型生成图片或保存图片
    $img->toJpeg($fileName=null) // 生成Jpeg图片或保存图片
    $img->toPng($fileName=null) // 生成Png图片或保存图片
    $img->toGif($fileName=null) // 生成Gif图片或保存图片
    $img->drawImage($file, $dstX=0, $dstY=0, $srcX=0, $srcY=0, $srcWidth=null, $srcHeight=null) // 将图片绘画到图层中
    $img->drawFill($x=0, $y=0, $color=0x000000) // 指定点填充颜色
