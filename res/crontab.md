EtSoftware php library

/*
*
*   扩展 1.0.0.1 
*   Release date: 2019-8-14
*   Author      : 半条虫(466814195)
*   Keywords    : Etsoftware 半条虫(466814195) rimke 39doo 39度
*   Description : 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*   Blog        : http://rimke.blog.163.com/        
*   Website     : http://www.39doo.com/
*   Mail        : rimke@163.com
*   Copyright   : Power By Etsoftware
*
*/


Etsoftware\Lib\Crontab

     Crontab::exec("* * * * *", function(){}, ...);
     Crontab::exec("1 * * * *", function(){
         dump(func_get_args());
         dump("finish!");
     },1,2,3);

等效于

    $crontab = new Crontab("* * * * *");
    // 设置日期
    // $crontab->setDate("* * * * *");
    // $crontab->setDate(["minute" => "*", "hour" => "*", "day" => "*", "month" => "*", "week" => "*"]);
    // $crontab->setDate(["*", "*", "*", "*", "*"]);
    // 获取日期
    // $crontab->getDate();

    // 调用函数
    $crontab->call(function(){
        dump(func_get_args());
        dump("finish!");
    },1,2,3);