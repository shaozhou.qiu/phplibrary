EtSoftware php library

/*
*
*   Mstsc 扩展 1.0.0.1 
*   Release date: 2019-8-14
*   Author      : 半条虫(466814195)
*   Keywords    : Etsoftware 半条虫(466814195) rimke 39doo 39度
*   Description : 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*   Blog        : http://rimke.blog.163.com/        
*   Website     : http://www.39doo.com/
*   Mail        : rimke@163.com
*   Copyright   : Power By Etsoftware
*
*/

[Readme.md]   (readme.md)  

Mstsc 模块
    $rdp = new Mstsc();

    

实例eg:
    
    use Etsoftware\RemoteDeskTop\Mstsc;

    $rdp = new Mstsc();
    $rdp->full_address = 'frp6.39doo.com:43308';
    $rdp->username = 'administrator';
    // 压缩
    $rdp->compression = 1;
    // 连接类型 
    $rdp->connection_type = 1;
    $rdp->setPassword("742520");
    // $rdp->password_51 = Mstsc::password("742520");

    dump($rdp->toString());
    die;
    // $rdp->save("php://output");