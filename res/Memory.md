EtSoftware php library

/*
*
*   Memory 扩展 1.0.0.1 
*   Release date: 2019-8-14
*   Author      : 半条虫(466814195)
*   Keywords    : Etsoftware 半条虫(466814195) rimke 39doo 39度
*   Description : 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*   Blog        : http://rimke.blog.163.com/        
*   Website     : http://www.39doo.com/
*   Mail        : rimke@163.com
*   Copyright   : Power By Etsoftware
*
*/

[Readme.md]   (readme.md)  

Memory 模块

    $memory = new Memory($key=0);
    // $key 指定系统共享内存id 如 0x74110f79
        [root@web]# ipcs -m
        ------ Shared Memory Segments --------
        key        shmid      owner      perms      bytes      nattch     status      
        0x00000000 65536      nginx      644        5          0                       
        0x00000000 98305      nginx      644        5          0                       
        0x00000000 131074     nginx      644        5          0                       
        0x00000000 163843     nginx      644        5          0                       
        0x00000000 196612     nginx      644        5          0                       
        0x00000000 229381     nginx      644        5          0                       
        0x00000000 262150     nginx      644        5          0                       
        0x00000000 294919     nginx      644        5          0                       
        0x00000000 327688     nginx      644        5          0                       
        0x00000000 360457     nginx      644        5          0                       
        0x74110f79 16646154   nginx      644        58         0            

    Memory::list();   
    // 获取共享内容列表

    Memory::enable();   
    // 判断shmop是否可用

    $memory->unset();   
    // 删除内容空间

    $memory->delete();  
    // 删除内容空间

    $memory->getSize(); 
    // 获取内存空间大小

    $memory->read();    
    // 读取内容

    $memory->write("hello");    
    //// 写入内容

    $memory->append("append");  
    // 以追加方式写入
