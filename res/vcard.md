EtSoftware php library

/*
*
*   扩展 1.0.0.1 
*   Release date: 2019-8-14
*   Author      : 半条虫(466814195)
*   Keywords    : Etsoftware 半条虫(466814195) rimke 39doo 39度
*   Description : 本脚本功能由EtSoftWare团队研发，仅供学习不得用于商业用途。
*   Blog        : http://rimke.blog.163.com/        
*   Website     : http://www.39doo.com/
*   Mail        : rimke@163.com
*   Copyright   : Power By Etsoftware
*
*/

[Readme.md]   (readme.md)  

Vcard 模块
    $tpl = new Vcard;

    function:
        toXml : function() 输出xml字符串
        toArray : function() 输出Array数组
        toString : function() 输出vcard字符串
        add : function ($type, $args, $value) 添加字段信息

实例eg:
    
    $data = "BEGIN:VCARD\nVERSION:2.1\n".
    "n;CHARSET=UTF-8;ENCODING=QUOTED-PRINTABLE:=E5=8D=8A;=E6=9D=A1=E8=99=AB;;;\n".
    "FN;CHARSET=UTF-8;ENCODING=QUOTED-PRINTABLE:=E5=8D=8A=E6=9D=A1=E8=99=AB\n".
    "TEL;CELL:13760****62\n";
    "END:VCARD";

    $vcard = new Vcard($data);

    
    $vcard->add("TEL", ["CELL"], "159141***53");
    // dump($vcard->toString());
    // dump($vcard->toArray());
    header("Content-type:text/xml");
    echo($vcard->toXml());