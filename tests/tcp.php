<?php
require __DIR__.'/../vendor/autoload.php';

use Etsoftware\Socket\Udp;
use Etsoftware\Socket\Tcp;

// UDP
$type = $_GET['type']??'server';
if( $type =='server' ){
    $udp = new Tcp('127.0.0.1', 7425);
    $ret = $udp->connect(function($len, $data, $addr, $port)use($udp){
        echo "-----------------------------\n";
        echo "time : ". time() ."\n";
        echo "length : $len\n";
        echo "data : $data\n";
        echo "address : $addr\n";
        echo "port : $port\n";
        // $udp->send('ok', 0, $addr, $port);
    });
    dump($ret);
    die();
}


$udp = new Tcp('127.0.0.1', 7426);
$ip = $_POST['ip']??'127.0.0.1';
$port = $_POST['port']??'7425';
$msg = $_POST['msg']??null;
$ret = null;
if($msg){
    $udp->send($msg, 0, $ip, $port, function($len, $data, $addr, $port)use(&$ret){
        $ret = "-----------------------------\n";
        $ret .= "time : ". time() ."\n";
        $ret .= "length : $len\n";
        $ret .= "data : $data\n";
        $ret .= "address : $addr\n";
        $ret .= "port : $port\n";        
    });
    echo($ret) ;
    die();
}else{
    $msg = 'Hello:'.time();
}



?>
<script type="text/javascript" src="/public/js/jquery-3.4.1.js"></script>
<script type="text/javascript" src="/public/js/EtSoftWare.7.0.js"></script>
<script type="text/javascript" src="/public/js/vue/dist/vue.js"></script> 

<form action="" method="POST" id=hfrm >
    <p>
        IP: <input type="text" name="ip" value="<?php echo $ip;?>">
    </p>
    <p>
        port: <input type="text" name="port" value="<?php echo $port;?>">
    </p>
    <p>
        message: <input type="text" name="msg" :value="msg">
    </p>
    <p>
        <input type="button" @click="post" value="send">
    </p>
    <p>
        <span v-html="data"></span>
    </p>
</form>
<script type="text/javascript" >
    new Vue({
        'el' : '#hfrm'
        , 'data' : {data:'', msg:'hello'}
        , 'methods' : {
            'post' : function(){
                var _me = this;
                var data = $('#hfrm').serialize();
                $.ajax({
                    url : location.href
                    , type : 'post'
                    , data : data
                    , success : function(e){
                        _me.data = e;
                    }
                });
                this.msg = 'time:'+ (new Date).getTime()
                
            }
        }
    });
</script> 